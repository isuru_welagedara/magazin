const { app, BrowserWindow } = require('electron')
const path = require('path')
// Specify flash path, supposing it is placed in the same directory with main.js.
let pluginName

switch (process.platform) {

  case 'win32':
    pluginName = 'pepflashplayer.dll'
    break
  case 'darwin':
    pluginName = 'PepperFlashPlayer.plugin'
    break
  case 'linux':
    pluginName = 'libpepflashplayer.so'
    break
}
app.commandLine.appendSwitch('ppapi-flash-path', __dirname + '/PepperFlash/pepflashplayer.dll');
app.on('ready', () => {
  let win = new BrowserWindow({ titleBarStyle: 'hidden', frame: true, width: 1280, height: 720, webPreferences: { plugins: true } })
  win.loadURL(`file://${__dirname}/home.html`)
  //win.loadURL('http://asmworks.net/game/Untitled-1.html');
  // Something else
  // window.open("http://asmworks.net/game/Untitled-1.html");
});
