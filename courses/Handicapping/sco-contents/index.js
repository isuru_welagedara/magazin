(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.an_TextInput = function(options) {
	this._element = new $.an.TextInput(options);
	this._el = this._element.create();
	var $this = this;
	this.addEventListener('added', function() {
		$this._lastAddedFrame = $this.parent.currentFrame;
		$this._element.attach($('#dom_overlay_container'));
	});
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,22);

p._tick = _tick;
p._handleDrawEnd = _handleDrawEnd;
p._updateVisibility = _updateVisibility;



(lib.Path_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(2).p("AiJkOQB4ArBIBoQBMBrAACCQAABSgfBL");
	this.shape.setTransform(14.1,28.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_4, new cjs.Rectangle(-0.6,0,28.9,56.3), null);


(lib.Path_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3).p("AiHhrID+gLIgxD6");
	this.shape.setTransform(13.7,13.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_3, new cjs.Rectangle(-1.4,0.1,28.8,28.1), null);


(lib.Path_2_0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3).p("AiLgcIDvhYIAeD8");
	this.shape.setTransform(14.5,13.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_2_0, new cjs.Rectangle(-1,-0.2,30,29.2), null);


(lib.Path_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3).p("AGHrOIsNWd");
	this.shape.setTransform(40.4,72.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_2, new cjs.Rectangle(-0.2,-0.8,81.2,146.8), null);


(lib.Path_1_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(2).p("ACoEYQiNgdhchwQhehyAAiSQAAhRAghN");
	this.shape.setTransform(17,28.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_1_2, new cjs.Rectangle(0,-0.6,34.8,58), null);


(lib.Path_1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3).p("AiLgcIDvhYIAeD8");
	this.shape.setTransform(14.5,13.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_1_1, new cjs.Rectangle(-1,-0.2,30,29.2), null);


(lib.Path_1_0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3).p("AGHrOIsNWd");
	this.shape.setTransform(40.5,72.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_1_0, new cjs.Rectangle(-0.1,-0.8,81.2,146.8), null);


(lib.Path_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EhNbAJ5QhbAAhBhAQhAhBAAhbIAAwVMChvAAAIAAQVQAABbhABBQhBBAhbAAg");
	this.shape.setTransform(517.6,63.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_1, new cjs.Rectangle(0,0,1035.3,126.6), null);


(lib.Path_0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3).p("AqUJlIUpzJ");
	this.shape.setTransform(67.1,62.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_0, new cjs.Rectangle(-0.5,-0.4,135.3,125.6), null);


(lib.Path = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#26C6DA").p("EBkAA8AMjH/AAAMAAAh3/MDH/AAAg");
	this.shape.setTransform(640.5,384.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#26C6DA").s().p("Ehj/A8AMAAAh3/MDH/AAAMAAAB3/g");
	this.shape_1.setTransform(640.5,384.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path, new cjs.Rectangle(-0.5,-0.5,1282,770), null);


(lib.Group_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Egp1A44QgsAAgggfQgfggAAgtMAAAhuXQAAgtAfggQAggfAsAAMBHSAAAMAOEBPxMgGQAh+g");
	this.shape.setTransform(278.5,364);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Group_1, new cjs.Rectangle(0,0,557,728), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AARA4IgagqIgKAKIAAAgIgOAAIAAhvIAOAAIAAA/IAgggIASAAIgfAdIAiAzg");
	this.shape.setTransform(159.1,18.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgdAkQgIgHAAgKQAAgGADgFQADgFAEgDQAFgCAFgBIANgCQAPgCAIgDIAAgEQAAgIgEgEQgFgEgKAAQgJAAgFADQgEAEgDAIIgNgCQACgIAEgGQAEgFAIgDQAIgDAKAAQAKAAAHADQAGACADAEQADAEACAFIAAANIAAASQAAATABAFQABAFADAFIgPAAIgDgKQgHAGgHADQgGADgJAAQgNAAgHgHgAgCAFIgMADQgEABgCADQgCADAAAEQAAAFAEAEQAFAEAIAAQAHAAAGgEQAGgDADgGQACgFAAgJIAAgFQgHADgOACg");
	this.shape_1.setTransform(150.3,19.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AARA4IgagqIgKAKIAAAgIgOAAIAAhvIAOAAIAAA/IAgggIASAAIgfAdIAiAzg");
	this.shape_2.setTransform(142.5,18.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AATA4IAAg0QAAgJgEgFQgFgEgIAAQgFAAgFADQgGADgCAFQgDAFAAAKIAAAsIgNAAIAAhvIANAAIAAAoQAKgLAOAAQAJAAAHADQAGAEADAGQADAGAAALIAAA0g");
	this.shape_3.setTransform(133.7,18.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAqAqIAAgzQAAgIgCgEQgBgDgEgDQgDgCgFAAQgJAAgFAGQgGAGAAAMIAAAvIgNAAIAAg0QAAgKgDgEQgEgFgHAAQgGAAgFADQgFADgCAGQgDAGAAALIAAAqIgNAAIAAhRIAMAAIAAAMQAEgGAGgEQAHgEAIAAQAJAAAGAEQAEAEADAHQAKgPAPAAQAMAAAHAHQAGAHAAAOIAAA3g");
	this.shape_4.setTransform(122.8,19.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgGA4IAAhQIANAAIAABQgAgGgoIAAgPIANAAIAAAPg");
	this.shape_5.setTransform(114.5,18.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgaAgQgKgLAAgUQAAgUAKgLQALgMAPAAQARAAAKALQAKAMAAATIAAADIg8AAQABAOAHAHQAHAHAJAAQAIAAAGgEQAFgEADgJIAOACQgEANgIAHQgJAHgPAAQgRAAgKgLgAgPgYQgGAGgBALIAtAAQgBgKgEgFQgHgIgLAAQgIAAgHAGg");
	this.shape_6.setTransform(103.9,19.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgVAqIAAhRIAMAAIAAAMQAFgIAEgDQADgDAFAAQAHAAAHAFIgEANQgFgDgGAAQgEAAgDACQgDADgCAFQgCAHAAAJIAAAqg");
	this.shape_7.setTransform(97.5,19.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgkAYIAAgOIBJAAIAAAOgAgkgKIAAgNIBJAAIAAANg");
	this.shape_8.setTransform(89.7,18.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgIAuQAFgCACgEQABgDAAgIIgHAAIAAgQIAPAAIAAAQQAAAJgDAGQgDAFgFADgAgHgjIAAgQIAPAAIAAAQg");
	this.shape_9.setTransform(82.9,21);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgWAkQgJgHgCgNIAOgCQABAIAFAFQAFAEAJAAQAJAAAFgEQAFgEAAgFQAAgEgEgDQgDgCgMgDIgUgGQgGgCgDgFQgDgFAAgGQAAgGADgEQACgFAFgDIAJgEQAFgCAGAAQAJAAAHADQAIADADAFQAEAEABAIIgOACQgBgGgEgEQgEgDgIAAQgJAAgEADQgEADAAAEQAAABAAAAQAAABAAABQAAAAABABQAAAAAAABQACACAEACIALADIAVAHQAFABAEAFQADAFAAAHQAAAHgEAGQgEAGgIADQgHAEgKAAQgPAAgIgHg");
	this.shape_10.setTransform(76.7,19.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AglAGIAAgMIBKgfIAAANIg6AYIA6AZIAAANg");
	this.shape_11.setTransform(68.2,18.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgWAkQgJgHgCgNIAOgCQABAIAFAFQAFAEAJAAQAJAAAFgEQAFgEAAgFQAAgEgEgDQgDgCgMgDIgUgGQgGgCgDgFQgDgFAAgGQAAgGADgEQACgFAFgDIAJgEQAFgCAGAAQAJAAAHADQAIADADAFQAEAEABAIIgOACQgBgGgEgEQgEgDgIAAQgJAAgEADQgEADAAAEQAAABAAAAQAAABAAABQAAAAABABQAAAAAAABQACACAEACIALADIAVAHQAFABAEAFQADAFAAAHQAAAHgEAGQgEAGgIADQgHAEgKAAQgPAAgIgHg");
	this.shape_12.setTransform(59.6,19.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAqAqIAAgzQAAgIgCgEQgBgDgEgDQgDgCgFAAQgJAAgFAGQgGAGAAAMIAAAvIgNAAIAAg0QAAgKgDgEQgEgFgHAAQgGAAgFADQgFADgCAGQgDAGAAALIAAAqIgNAAIAAhRIAMAAIAAAMQAEgGAGgEQAHgEAIAAQAJAAAGAEQAEAEADAHQAKgPAPAAQAMAAAHAHQAGAHAAAOIAAA3g");
	this.shape_13.setTransform(49.2,19.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#F44336").s().p("As2C8QhNABg4g4Qg3g4AAhNQAAhNA3g3QA4g4BNAAIZtAAQBOAAA3A4QA3A3AABNQAABNg3A4Qg3A4hOgBg");
	this.shape_14.setTransform(101.1,18.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F44336").s().p("AiBhPIEDAAIiCCfg");
	this.shape_15.setTransform(101.1,45.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,202.3,53.4), null);


(lib.an_CSS = function(options) {
	this._element = new $.an.CSS(options);
	this._el = this._element.create();
	var $this = this;
	this.addEventListener('added', function() {
		$this._lastAddedFrame = $this.parent.currentFrame;
		$this._element.attach($('#dom_overlay_container'));
	});
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,22);

p._tick = _tick;
p._handleDrawEnd = _handleDrawEnd;
p._updateVisibility = _updateVisibility;



(lib.correct = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhrgEIAdgeIA4A3IBlhmIAeAeIiDCFg");
	this.shape.setTransform(0.4,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#419946").s().p("AAXBpIAAABIAAgBIAAAAIgCgCIAAABIAAgBIgCgBIAAgBIAAABIgCgBIgBgBIAAgBIgBAAIAAAAIgBgBIAAgBIgBgBIgCgCIgCAAIAAgBIgBAAIAAAAIgBgBIgBgBIgBAAIgBgCIgBgBIgBgBIAAAAIgCgBIAAAAIAAgBIgBgBIgBgBIgBgBIgBgBIgBAAIAAgBIgBAAIAAgBIgCgBIgCgCIgBAAIAAgBIgBgBIAAAAIgBAAIAAgBIgBAAIgDgCIgBgBIAAgBIgBgBIgBAAIAAAAIgBgBIAAgBIgCgBIgCgBIAAgBIgBAAIgBgBIAAAAIgBgBIgBgBIgBgCIgBAAIhWhWIAegfIACACIABAAIABADIABAAIABABIABABIACABIABAAIACACIAAABIABABIABAAIACADIAAAAIACABIAAAAIABABIABAAIAAABIAAAAIACACIAAgBIABABIAAAAIACACIAAgBIAAACIABAAIABABIAAAAIADACIAAAAIAAAAIAAAAIACACIAAgBIAAABIABABIABABIAAgBIADADIAAAAIAAABIABAAIAAAAIABABIAAAAIABABIAAAAIABABIAAgBIACADIAAgBIABABIABAAIAAABIABABIAAgBIABACIABAAIAAABIAAAAIACACIAAAAIABAAIABABIAAABIABgBIABABIABABIAAABIAAgBIACADIAAAAIABABIABAAIAAABIABAAIAAABIBghjIABAAIAAABIABAAIABABIAAAAIABABIABAAIACADIABAAIABABIAAABIABAAIAAAAIABABIABAAIACACIABABIABABIABAAIAAABIABAAIAAABIAAABIABAAIACACIABAAIABABIABAAIAAABIABAAIAAABIABAAIAAAAIACADIABAAIAAABIABABIADABIAAAAIACACIABABIAAABIABAAQAMAbAAAbIAAADQAAArgaAjQgbAigpAMg");
	this.shape_1.setTransform(1.3,2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#52B855").s().p("AgDCCQg0AAgmgmQglgmAAg1IAAgBQAAg2AlgmQAmglA0AAIAHAAQA0AAAmAlQAlAmAAA2IAAABQAAA1glAmQgmAmg0AAg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgDCLQg4AAgogoQgngpAAg5IAAgBQAAg6AngoQAogoA4AAIAHAAQA4AAAoAoQAnAoAAA6IAAABQAAA5gnApQgoAog4AAg");

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAAAeIg3A5IgdgeIA3g5Ig3g4IAdgeIA3A4IA4g4IAdAeIg3A4IA3A5IgdAeg");
	this.shape_4.setTransform(0,0.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#C4342B").s().p("AgaBtIgBgBIAAAAIgBAAIgBgBIAAgBIAAABIgCgDIAAABIgBgBIAAAAIgCgCIAAABIgBgBIgBgBIAAgBIAAABIgDgDIAAABIAAgBIgBAAIgBgCIAAABIAAgBIgBgBIgBgBIAAABIgDgDIAAABIAAgBIgBAAIgBgCIAAABIgBgBIAAgBIgBgBIAAABIgCgDIAAABIgBgBIgBAAIgBgCIAAABIgBgBIgBgBIAAgBIAAABIgCgDIAAABIgBgBIAAAAIgCgCIAAABIgBgBIgBgBIAAgBIAAABIgDgDIAAABIAAgBIAAAAIgBgBIAAgBIgBABIAAgBIgCgBIAAgBIAAABIgDgDIAAABIAAgBIgBgBIgBgBIAAABIAAgBIgBgBIgegeIA4g4Ig4g5IAegeIABABIABACIABAAIAAAAIABABIAAAAIABABIABAAIAAABIACABIACACIAAAAIABABIAAAAIABABIABAAIAAABIABABIADACIABAAIAAABIABAAIAAABIABAAIAAABIABABIACACIACAAIAAABIABAAIAAABIABAAIABABIAAABIACACIABAAIABABIAAAAIABABIABAAIABABIABABIABACIABAAIABABIAAAAIABABIABAAIAAABIABABIADACIABAAIAAABIAAAAIABABIABAAIAAABIABABIACACIACAAIAAABIABAAIAAABIABAAIAAABIABABIACACIABAAIABABIAyg0IAAABIABAAIABABIABABIACACIABAAIABACIABAAIABABIABABIACACIABAAIACACIABAAIAAABIABABIACACIAAAAIACACIACAAIAAABIABABIACACIAAAAIACACIABAAIABABIABABIACACIABAAIABACIABAAIABABIABABIACACIABAAIACACIABAAIAAABIABABIACACIAAAAIADACIABAAIAAABIABABIACACIAAAAIABAAIAAABIABAAIAAABIACAAIAAABIABABIACACIABAAIAAABIABAAIAAABIABAAIABABIABABIACACIABAAIABABIAAAAIAAABIABAAIABABIABABIACAXIAAACQAAA1glAmQgmAmgzAAg");
	this.shape_5.setTransform(2.3,2.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#EE4036").s().p("AgDCCQg0ABgmgmQglgnAAg1IAAgCQAAg0AlgnQAmglA0gBIAHAAQA0ABAmAlQAlAnAAA0IAAACQAAA1glAnQgmAmg0gBg");
	this.shape_6.setTransform(0,0.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgDCLQg4AAgogoQgngoAAg6IAAgBQAAg5AngpQAogoA4AAIAHAAQA4AAAoAoQAnApAAA5IAAABQAAA6gnAoQgoAog4AAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-13.9,-13.9,27.9,27.9);


(lib.confirm = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#004D40").s().p("AAaBWIgpg/IgOANIAAAyIgWAAIAAirIAWAAIAABhIAwgxIAcAAIgvAsIA0BPg");
	this.shape.setTransform(39.7,2.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#004D40").s().p("AgtA3QgLgKAAgQQgBgJAFgHQAEgIAHgEQAGgEAJgCIASgDQAZgEAMgEIAAgFQAAgNgFgFQgJgIgPAAQgPAAgGAGQgIAFgDANIgVgDQADgNAGgIQAHgIAMgFQANgEAOAAQARAAAJAEQALADAEAGQAEAGADAJIABATIAAAbQAAAeABAHQABAIAEAHIgWAAQgDgGgBgJQgLAKgLAEQgLAEgMAAQgVAAgKgKgAgEAIQgNACgGACQgFACgDAFQgDAEAAAGQAAAIAHAGQAGAGAMAAQALAAAKgGQAKgFAEgJQAEgIgBgNIAAgIQgLAEgWAEg");
	this.shape_1.setTransform(26.2,5.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#004D40").s().p("AAZBWIgng/IgQANIAAAyIgUAAIAAirIAUAAIAABhIAygxIAbAAIgvAsIAzBPg");
	this.shape_2.setTransform(14.3,2.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#004D40").s().p("AghBAIAAh8IATAAIAAATQAIgNAGgEQAFgFAHAAQALAAALAHIgHAUQgIgFgIAAQgHAAgEAFQgGAEgCAHQgEALAAAOIAABAg");
	this.shape_3.setTransform(4.5,5.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#004D40").s().p("AgJBWIAAirIATAAIAACrg");
	this.shape_4.setTransform(-3.2,2.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#004D40").s().p("AgoAwQgQgRAAgeQAAgeAQgSQAQgRAZAAQAZAAAPARQAQARAAAeIAAAFIhcAAQACAVAKALQAKALAPAAQAMAAAIgGQAIgGAFgOIAWADQgGATgOALQgNAKgWAAQgaAAgQgRgAgWglQgKAKgBAQIBEAAQgBgQgHgHQgKgNgQAAQgOAAgJAKg");
	this.shape_5.setTransform(-19.2,5.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#004D40").s().p("AghBAIAAh8IATAAIAAATQAIgNAGgEQAFgFAHAAQALAAALAHIgHAUQgIgFgIAAQgHAAgEAFQgGAEgCAHQgEALAAAOIAABAg");
	this.shape_6.setTransform(-28.9,5.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#004D40").s().p("AAhBXIAAg8QgGAGgIAFQgJAFgKAAQgVAAgQgSQgQgSAAgdQAAgTAHgPQAGgPANgIQAMgHAPAAQAWAAANATIAAgQIATAAIAACqgAgWg6QgKALAAAaQAAAXALAMQAKAMANgBQAOAAAJgLQAKgMAAgVQAAgagKgMQgLgNgOAAQgMAAgKAMg");
	this.shape_7.setTransform(-40.9,7.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#004D40").s().p("AgXBtIAEgSIAKACQAGAAADgEQADgFAAgRIAAiCIAVAAIAACDQAAAXgGAJQgIALgQAAQgJAAgIgCgAADhWIAAgYIAVAAIAAAYg");
	this.shape_8.setTransform(-51.2,5.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#004D40").s().p("AgtBVIgDgUQAHACAGAAQAGAAAFgCQAEgCADgFQACgDAEgNIACgFIgvh7IAXAAIAaBIIAIAbQADgNAFgOIAbhIIAVAAIgvB9QgIAVgEAHQgFALgHAFQgHAEgLAAQgFAAgIgCg");
	this.shape_9.setTransform(-58.4,7.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#004D40").s().p("AgMBGQAHgDADgFQACgGABgLIgLAAIAAgYIAXAAIAAAYQAAANgFAJQgFAIgJAEgAgKg2IAAgYIAXAAIAAAYg");
	this.shape_10.setTransform(-67.9,6.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFCA28").s().p("As6ENQgOAAgKgJQgLgIgCgNIhUluQgEgRALgOQALgOARgBIcdhfQATgBAMAOQAMAOgDASIhWHMQgCAOgLAJQgLAJgNAAg");

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AthE1QgOAAgLgIQgKgJgDgOIhWm9QgDgRALgOQALgNARgBIdwhgQASAAAMANQAMANgDASIhYIdQgCAOgLAJQgKAJgOAAg");

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF6F00").s().p("As6ENQgOAAgKgJQgLgIgCgNIhUluQgEgRALgOQALgOARgBIcdhfQATgBAMAOQAMAOgDASIhWHMQgCAOgLAJQgLAJgNAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_12},{t:this.shape_13},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-99,-30.9,198.1,61.8);


(lib.answer = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#004D40").s().p("AghBAIAAh8IATAAIAAATQAIgNAGgEQAFgFAHAAQALAAALAHIgHAUQgIgFgIAAQgHAAgEAFQgGAEgCAHQgEALAAAOIAABAg");
	this.shape.setTransform(99,5.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#004D40").s().p("Ag4AkIAAgUIBxAAIAAAUgAg4gPIAAgUIBxAAIAAAUg");
	this.shape_1.setTransform(87.1,3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#004D40").s().p("AgMBGQAHgDADgFQACgGABgLIgLAAIAAgYIAXAAIAAAYQAAANgFAJQgFAIgJAEgAgKg2IAAgYIAXAAIAAAYg");
	this.shape_2.setTransform(76.7,6.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#004D40").s().p("AgjA3QgMgKgDgUIAUgDQACAMAIAHQAIAHANAAQAPAAAHgGQAHgGAAgIQAAgHgGgEQgEgDgRgEQgXgGgJgEQgJgEgFgHQgEgIAAgJQAAgIAEgIQAEgHAGgEQAFgEAJgCQAIgDAJAAQAOAAAMAEQALAEAFAIQAFAHACAMIgVADQgBgKgHgFQgGgGgMAAQgPAAgFAFQgHAFAAAGQABAEACAEQACADAGACIARAGQAXAGAJAEQAJACAEAIQAGAHgBALQABAKgHAKQgFAJgMAFQgMAFgPAAQgWAAgOgKg");
	this.shape_3.setTransform(67.3,5.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#004D40").s().p("Ag4AKIAAgTIBxgwIAAAVIhaAkIBaAlIAAAVg");
	this.shape_4.setTransform(54.4,3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#004D40").s().p("AgjA3QgMgKgDgUIAUgDQACAMAIAHQAIAHAOAAQAOAAAHgGQAHgGAAgIQAAgHgGgEQgFgDgQgEQgXgGgJgEQgJgEgEgHQgFgIAAgJQAAgIAEgIQAEgHAGgEQAFgEAIgCQAJgDAJAAQAOAAALAEQAMAEAFAIQAFAHACAMIgVADQgBgKgHgFQgGgGgMAAQgPAAgGAFQgFAFgBAGQAAAEADAEQADADAFACIASAGQAWAGAJAEQAIACAGAIQAEAHAAALQABAKgHAKQgFAJgMAFQgMAFgOAAQgYAAgNgKg");
	this.shape_5.setTransform(41.3,5.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#004D40").s().p("AA/BAIAAhOQAAgMgCgGQgCgFgFgEQgGgDgHAAQgNAAgJAJQgIAIAAATIAABIIgUAAIAAhQQAAgOgGgHQgFgHgMAAQgIAAgIAEQgIAFgDAJQgDAJAAARIAABAIgVAAIAAh8IASAAIAAASQAGgJAKgGQAKgGAMAAQAOAAAJAGQAIAGADAKQAPgWAYAAQASAAAKALQAKAKAAAVIAABVg");
	this.shape_6.setTransform(25.4,5.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#004D40").s().p("AgjA3QgMgKgEgUIAVgDQACAMAIAHQAIAHANAAQAPAAAHgGQAHgGAAgIQAAgHgGgEQgFgDgRgEQgWgGgJgEQgJgEgFgHQgEgIAAgJQAAgIAEgIQADgHAHgEQAFgEAJgCQAIgDAKAAQAOAAALAEQAKAEAGAIQAFAHACAMIgUADQgCgKgGgFQgIgGgLAAQgPAAgFAFQgHAFABAGQgBAEADAEQACADAGACIARAGQAXAGAJAEQAJACAEAIQAFAHABALQgBAKgGAKQgGAJgLAFQgMAFgPAAQgXAAgNgKg");
	this.shape_7.setTransform(2.7,5.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#004D40").s().p("AgpAwQgQgQAAggQAAgiATgQQAQgOAWAAQAaAAAQARQAQARAAAdQAAAXgHAOQgHAOgOAHQgOAIgQAAQgZAAgQgRgAgZgjQgLAMAAAXQAAAYALAMQAKAMAPAAQAQAAAKgMQALgMAAgYQAAgXgLgMQgKgMgQAAQgPAAgKAMg");
	this.shape_8.setTransform(-10,5.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#004D40").s().p("AghBAIAAh8IATAAIAAATQAIgNAGgEQAFgFAHAAQALAAALAHIgHAUQgIgFgIAAQgHAAgEAFQgGAEgCAHQgEALAAAOIAABAg");
	this.shape_9.setTransform(-19.7,5.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#004D40").s().p("AgoAwQgQgRAAgeQAAgeAQgSQAQgRAZAAQAZAAAPARQAQARAAAeIAAAFIhcAAQACAVAKALQAKALAPAAQAMAAAIgGQAIgGAFgOIAWADQgGATgOALQgNAKgWAAQgaAAgQgRgAgWglQgKAKgBAQIBEAAQgBgQgHgHQgKgNgQAAQgOAAgJAKg");
	this.shape_10.setTransform(-31.3,5.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#004D40").s().p("AgXBtIAEgSIAKACQAHAAACgEQACgFAAgRIAAiCIAWAAIAACDQAAAXgGAJQgIALgRAAQgIAAgIgCgAAChWIAAgYIAWAAIAAAYg");
	this.shape_11.setTransform(-42,5.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#004D40").s().p("AgiA3QgNgKgDgUIAUgDQACAMAIAHQAIAHAOAAQAOAAAHgGQAHgGAAgIQAAgHgGgEQgFgDgQgEQgXgGgJgEQgJgEgEgHQgFgIAAgJQAAgIAEgIQADgHAHgEQAFgEAIgCQAJgDAJAAQAOAAALAEQALAEAGAIQAFAHACAMIgUADQgCgKgGgFQgIgGgLAAQgOAAgHAFQgFAFAAAGQAAAEACAEQADADAFACIASAGQAWAGAJAEQAIACAGAIQAEAHAAALQAAAKgFAKQgHAJgLAFQgMAFgOAAQgXAAgNgKg");
	this.shape_12.setTransform(-49.4,5.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#004D40").s().p("AAaBWIgpg/IgPANIAAAyIgVAAIAAirIAVAAIAABhIAygxIAbAAIgvAsIAzBPg");
	this.shape_13.setTransform(-60.5,2.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFCA28").s().p("As6ENQgOAAgKgJQgLgIgCgNIhUluQgEgRALgOQALgOARgBIcdhfQATgBAMAOQAMAOgDASIhWHMQgCAOgLAJQgLAJgNAAg");

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AthE1QgOAAgLgIQgKgJgDgOIhWm9QgDgRALgOQALgNARgBIdwhgQASAAAMANQAMANgDASIhYIdQgCAOgLAJQgKAJgOAAg");

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF6F00").s().p("As6ENQgOAAgKgJQgLgIgCgNIhUluQgEgRALgOQALgOARgBIcdhfQATgBAMAOQAMAOgDASIhWHMQgCAOgLAJQgLAJgNAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_15},{t:this.shape_16},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-99,-30.9,203.3,61.8);


(lib.enterans = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_30 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(30).call(this.frame_30).wait(1));

	// Layer 1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(0,7.7,1,1,0,0,0,101.1,26.7);
	this.instance.alpha = 0;

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AARA4IgagqIgKAKIAAAgIgOAAIAAhvIAOAAIAAA/IAgggIASAAIgfAdIAiAzg");
	this.shape.setTransform(58.1,-8.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgdAkQgIgHAAgKQAAgGADgFQADgFAEgDQAFgCAFgBIANgCQAPgCAIgDIAAgEQAAgIgEgEQgFgEgKAAQgJAAgFADQgEAEgDAIIgNgCQACgIAEgGQAEgFAIgDQAIgDAKAAQAKAAAHADQAGACADAEQADAEACAFIAAANIAAASQAAATABAFQABAFADAFIgPAAIgDgKQgHAGgHADQgGADgJAAQgNAAgHgHgAgCAFIgMADQgEABgCADQgCADAAAEQAAAFAEAEQAFAEAIAAQAHAAAGgEQAGgDADgGQACgFAAgJIAAgFQgHADgOACg");
	this.shape_1.setTransform(49.3,-6.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AARA4IgagqIgKAKIAAAgIgOAAIAAhvIAOAAIAAA/IAgggIASAAIgfAdIAiAzg");
	this.shape_2.setTransform(41.5,-8.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AATA4IAAg0QAAgJgEgFQgFgEgIAAQgFAAgFADQgGADgCAFQgDAFAAAKIAAAsIgNAAIAAhvIANAAIAAAoQAKgLAOAAQAJAAAHADQAGAEADAGQADAGAAALIAAA0g");
	this.shape_3.setTransform(32.7,-8.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAqAqIAAgzQAAgIgCgEQgBgDgEgDQgDgCgFAAQgJAAgFAGQgGAGAAAMIAAAvIgNAAIAAg0QAAgKgDgEQgEgFgHAAQgGAAgFADQgFADgCAGQgDAGAAALIAAAqIgNAAIAAhRIAMAAIAAAMQAEgGAGgEQAHgEAIAAQAJAAAGAEQAEAEADAHQAKgPAPAAQAMAAAHAHQAGAHAAAOIAAA3g");
	this.shape_4.setTransform(21.8,-6.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgGA4IAAhQIANAAIAABQgAgGgoIAAgPIANAAIAAAPg");
	this.shape_5.setTransform(13.5,-8.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgaAgQgLgLABgUQgBgUALgLQALgMAQAAQAQAAAKALQALAMAAATIAAADIg9AAQACAOAGAHQAHAHAKAAQAHAAAFgEQAGgEADgJIAOACQgDANgKAHQgIAHgPAAQgQAAgLgLgAgOgYQgHAGgBALIAtAAQgBgKgEgFQgHgIgKAAQgJAAgGAGg");
	this.shape_6.setTransform(2.9,-6.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgVAqIAAhRIAMAAIAAAMQAFgIAEgDQADgDAFAAQAHAAAHAFIgEANQgFgDgGAAQgEAAgDACQgDADgCAFQgCAHAAAJIAAAqg");
	this.shape_7.setTransform(-3.5,-6.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgkAYIAAgOIBJAAIAAAOgAgkgKIAAgMIBJAAIAAAMg");
	this.shape_8.setTransform(-11.3,-8.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgHAuQAEgCACgDQABgFABgHIgIAAIAAgPIAQAAIAAAPQgBAJgDAFQgDAGgGADgAgHgkIAAgPIAQAAIAAAPg");
	this.shape_9.setTransform(-18.1,-5.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgWAkQgJgHgCgNIAOgCQABAIAFAFQAFAEAJAAQAJAAAFgEQAFgEAAgFQAAgEgEgDQgDgCgMgDIgUgGQgGgCgDgFQgDgFAAgGQAAgGADgEQACgFAFgDIAJgEQAFgCAGAAQAJAAAHADQAIADADAFQAEAEABAIIgOACQgBgGgEgEQgEgDgIAAQgJAAgEADQgEADAAAEQAAABAAAAQAAABAAABQAAAAABABQAAAAAAABQACACAEACIALADIAVAHQAFABAEAFQADAFAAAHQAAAHgEAGQgEAGgIADQgHAEgKAAQgPAAgIgHg");
	this.shape_10.setTransform(-24.3,-6.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgkAGIAAgLIBJggIAAAOIg6AXIA6AYIAAAOg");
	this.shape_11.setTransform(-32.8,-8.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgWAkQgJgHgCgNIAOgCQABAIAFAFQAFAEAJAAQAJAAAFgEQAFgEAAgFQAAgEgEgDQgDgCgMgDIgUgGQgGgCgDgFQgDgFAAgGQAAgGADgEQACgFAFgDIAJgEQAFgCAGAAQAJAAAHADQAIADADAFQAEAEABAIIgOACQgBgGgEgEQgEgDgIAAQgJAAgEADQgEADAAAEQAAABAAAAQAAABAAABQAAAAABABQAAAAAAABQACACAEACIALADIAVAHQAFABAEAFQADAFAAAHQAAAHgEAGQgEAGgIADQgHAEgKAAQgPAAgIgHg");
	this.shape_12.setTransform(-41.4,-6.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAqAqIAAgzQAAgIgCgEQgBgDgEgDQgDgCgFAAQgJAAgFAGQgGAGAAAMIAAAvIgNAAIAAg0QAAgKgDgEQgEgFgHAAQgGAAgFADQgFADgCAGQgDAGAAALIAAAqIgNAAIAAhRIAMAAIAAAMQAEgGAGgEQAHgEAIAAQAJAAAGAEQAEAEADAHQAKgPAPAAQAMAAAHAHQAGAHAAAOIAAA3g");
	this.shape_13.setTransform(-51.8,-6.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#F44336").s().p("Ai4hxIFxAAIi5Djg");
	this.shape_14.setTransform(0,15.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F44336").s().p("As2C8QhNABg4g4Qg3g4AAhNQAAhNA3g3QA4g4BNAAIZtAAQBOAAA3A4QA3A3AABNQAABNg3A4Qg3A4hOgBg");
	this.shape_15.setTransform(0,-7.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.instance}]},21).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({y:6.9,alpha:0.25},0).wait(1).to({y:6.1,alpha:0.5},0).wait(1).to({y:5.3,alpha:0.75},0).wait(1).to({y:4.4,alpha:1},0).to({_off:true},1).wait(21).to({_off:false},0).wait(1).to({y:5.3,alpha:0.75},0).wait(1).to({y:6.1,alpha:0.5},0).wait(1).to({y:6.9,alpha:0.25},0).wait(1).to({y:7.7,alpha:0},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-101.1,-19,202.3,53.4);


// stage content:
(lib.index = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		createjs.Touch.enable(stage);
		var checkcount = 7; 
		var loopcount = checkcount +1;
		this.enterans.visible = false;
		this.enterans.stop();
		this.answer.mouseEnabled = false;
		this.confirm.mouseEnabled = false;
		createjs.Touch.enable(stage);
			setTimeout(function (){
				$("#text1").prop("disabled", true);
				$("#text2").prop("disabled", true);
				$("#text3").prop("disabled", true);
				$("#text4").prop("disabled", true);
				$("#text5").prop("disabled", true);
				$("#text6").prop("disabled", true);
				$("#text7").prop("disabled", true);
		},0.00001);
		
		
		
		var aud1 = createjs.Sound.play("audio");
		
		aud1.on("complete", audComplete.bind(this));
		
		
		
		function audComplete(event) {
				$("#text1").prop("disabled", false);
				$("#text2").prop("disabled", false);
				$("#text3").prop("disabled", false);
				$("#text4").prop("disabled", false);
				$("#text5").prop("disabled", false);
				$("#text6").prop("disabled", false);
				$("#text7").prop("disabled", false);
				this.confirm.mouseEnabled = true;
		}
		
		
		var clip = this; 
		
		
		for(var i=1 ; i<loopcount; i++)
		{
		
		
			var obj = clip["correct"+i];
			obj.visible = false;
			
		}
		
		
		
		var txt =['6','2','3','4','7','5','1']; //add the answers here |Sanka|
		
		
		
		this.confirm.addEventListener("click", fl_ClickToPosition.bind(this));
		
		
		function fl_ClickToPosition()
		{ 
		
		  var cc = 0;
			for(var at=1; at<loopcount ; at++  )//numbers of text fields *Sanka*
			{
				if ($("#text"+at).val() =='')
				{
					cc = cc -1;
				}
				  else
				  {
					  cc = cc + 1;
				  }
				  
			}
			
			
			if (cc == checkcount )//numbers of text fields *Sanka*
			{
				$("#text1").prop("disabled", true);
				$("#text2").prop("disabled", true);
				$("#text3").prop("disabled", true);
				$("#text4").prop("disabled", true);
				$("#text5").prop("disabled", true);
				$("#text6").prop("disabled", true);
				$("#text7").prop("disabled", true);
				
				this.confirm.mouseEnabled = false;
				this.answer.mouseEnabled = true;
				//checking all the answers are correct or wrong *Sanka* 
				var c = 0;
				for(var a = 1; a<loopcount; a++ )
				{
		
					var obj = clip["correct"+a];
					obj.visible = true;
		
					if($("#text"+a).val() == txt[c]) 
					{
						obj.gotoAndStop(2);
					}else
					{
						obj.gotoAndStop(3);
		
					}
		
					c = c+1;
					
					
				}
			
			}else
			{
				this.enterans.visible = true;
				this.enterans.play();
				
			}
		}
		
		
		this.answer.addEventListener("click", answerbtn.bind(this));
		
		
		function answerbtn()
		{
			this.answer.mouseEnabled = false;
			//this.answer.visible=false;
			
			for(var a = 1; a<loopcount; a++ )
				{
					$("#text"+a).val(txt[a-1]);
				}
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// correct
	this.correct7 = new lib.correct();
	this.correct7.parent = this;
	this.correct7.setTransform(1188,561.7);

	this.correct6 = new lib.correct();
	this.correct6.parent = this;
	this.correct6.setTransform(1188,499.1);

	this.correct5 = new lib.correct();
	this.correct5.parent = this;
	this.correct5.setTransform(1188,437.1);

	this.correct4 = new lib.correct();
	this.correct4.parent = this;
	this.correct4.setTransform(1188,374.3);

	this.correct3 = new lib.correct();
	this.correct3.parent = this;
	this.correct3.setTransform(1188,312.5);

	this.correct2 = new lib.correct();
	this.correct2.parent = this;
	this.correct2.setTransform(1188.2,250.7);

	this.correct1 = new lib.correct();
	this.correct1.parent = this;
	this.correct1.setTransform(1188.2,188.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.correct1},{t:this.correct2},{t:this.correct3},{t:this.correct4},{t:this.correct5},{t:this.correct6},{t:this.correct7}]}).wait(1));

	// btn
	this.enterans = new lib.enterans();
	this.enterans.parent = this;
	this.enterans.setTransform(793.4,607.1);

	this.answer = new lib.answer();
	this.answer.parent = this;
	this.answer.setTransform(1005.5,667.2);
	new cjs.ButtonHelper(this.answer, 0, 1, 2);

	this.confirm = new lib.confirm();
	this.confirm.parent = this;
	this.confirm.setTransform(795.5,667.2);
	new cjs.ButtonHelper(this.confirm, 0, 1, 2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.confirm},{t:this.answer},{t:this.enterans}]}).wait(1));

	// text
	this.text7 = new lib.an_TextInput({'id': 'text7', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.text7.setTransform(1090.5,562.2,0.9,1.382,0,0,0,50,11);

	this.text6 = new lib.an_TextInput({'id': 'text6', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.text6.setTransform(1090.5,500.2,0.9,1.382,0,0,0,50,11);

	this.text5 = new lib.an_TextInput({'id': 'text5', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.text5.setTransform(1090.5,438.2,0.9,1.382,0,0,0,50,11);

	this.text4 = new lib.an_TextInput({'id': 'text4', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.text4.setTransform(1090.5,375.2,0.9,1.382,0,0,0,50,11);

	this.text3 = new lib.an_TextInput({'id': 'text3', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.text3.setTransform(1090.5,313.2,0.9,1.382,0,0,0,50,11);

	this.text2 = new lib.an_TextInput({'id': 'text2', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.text2.setTransform(1090.5,250.3,0.9,1.382,0,0,0,50,11);

	this.text1 = new lib.an_TextInput({'id': 'text1', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.text1.setTransform(1090.5,189.3,0.9,1.382,0,0,0,50,11);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text1},{t:this.text2},{t:this.text3},{t:this.text4},{t:this.text5},{t:this.text6},{t:this.text7}]}).wait(1));

	// bg
	this.instance = new lib.an_CSS({'id': '', 'href':'assets/style.css'});

	this.instance.setTransform(-119.3,442.4,1,1,0,0,0,50,11);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#0097A7").ss(3).p("AG4C5ItvAAQg2AAgmgmQgmgmAAg2IAAhtQAAg1AmgnQAmgmA2AAINvAAQA2AAAmAmQAmAnAAA1IAABtQAAA2gmAmQgmAmg2AAg");
	this.shape.setTransform(1091,562.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#05686D").s().p("Am3C5Qg2AAgmgmQgmgmAAg2IAAhtQAAg1AmgnQAmgmA2AAINvAAQA2AAAmAmQAmAnAAA1IAABtQAAA2gmAmQgmAmg2AAg");
	this.shape_1.setTransform(1091,562.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgPAOIAAgbIAfAAIAAAbg");
	this.shape_2.setTransform(678.9,570.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgYBOIABgQQAEgiAMgcQALgdAVgYIhPAAIAAgYIBtAAIAAAYQgZAdgJAZQgJAZgEAkIgCAQg");
	this.shape_3.setTransform(669.3,563.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AAdBWIAAhPQAAgPgGgHQgHgHgNAAQgIAAgIAFQgJAFgDAIQgEAIABAOIAABEIgWAAIAAirIAWAAIAAA+QAOgRAWAAQAOgBAJAGQALAGAEAJQAEAKABARIAABPg");
	this.shape_4.setTransform(787.5,563.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AApBWIg8hWIgcAaIAAA8IgXAAIAAirIAXAAIAABVIBUhVIAfAAIhHBGIBKBlg");
	this.shape_5.setTransform(773.7,563.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AghBAIAAh8IATAAIAAATQAIgNAGgEQAFgFAHAAQALAAALAHIgHAUQgIgFgIAAQgHAAgEAFQgGAEgCAHQgEALAAAOIAABAg");
	this.shape_6.setTransform(761.7,565.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgiA3QgNgKgDgUIAUgDQACAMAIAHQAIAHAOAAQAOAAAHgGQAHgGAAgIQAAgHgGgEQgFgDgQgEQgXgGgJgEQgJgEgEgHQgFgIAAgJQAAgIAEgIQADgHAHgEQAFgEAIgCQAJgDAJAAQAOAAALAEQALAEAGAIQAFAHACAMIgUADQgCgKgGgFQgIgGgLAAQgOAAgHAFQgFAFAAAGQAAAEACAEQADADAFACIASAGQAWAGAJAEQAIACAGAIQAEAHAAALQAAAKgFAKQgHAJgLAFQgMAFgOAAQgXAAgNgKg");
	this.shape_7.setTransform(750.7,565.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgKBWIAAirIAVAAIAACrg");
	this.shape_8.setTransform(742.1,563.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AAZBWIgng/IgPANIAAAyIgVAAIAAirIAVAAIAABhIAxgyIAbAAIgvAtIAzBPg");
	this.shape_9.setTransform(727.6,563.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgMBGQAHgDADgFQACgGABgLIgLAAIAAgYIAXAAIAAAYQAAANgFAJQgFAIgJAEgAgKg2IAAgYIAXAAIAAAYg");
	this.shape_10.setTransform(717.4,567.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AA/BAIAAhOQAAgMgCgGQgCgFgFgEQgGgDgHAAQgNAAgJAJQgIAIAAATIAABIIgUAAIAAhQQAAgOgGgHQgFgHgMAAQgIAAgIAEQgIAFgDAJQgDAJAAARIAABAIgVAAIAAh8IASAAIAAASQAGgJAKgGQAKgGAMAAQAOAAAJAGQAIAGADAKQAPgWAYAAQASAAAKALQAKAKAAAVIAABVg");
	this.shape_11.setTransform(704.2,565.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("EgmqAD/Qg2AAgmgmQgmgnAAg1IAAj5QAAg2AmgmQAmgmA2AAMBNVAAAQA1AAAnAmQAmAmAAA2IAAD5QAAA1gmAnQgnAmg1AAg");
	this.shape_12.setTransform(900.5,562.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#0097A7").ss(3).p("AG4C5ItvAAQg2AAgmgmQgmgmAAg2IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA2gmAmQgmAmg2AAg");
	this.shape_13.setTransform(1091,500);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#05686D").s().p("Am3C5Qg2AAgmgmQgmgmAAg2IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA2gmAmQgmAmg2AAg");
	this.shape_14.setTransform(1091,500);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgPAOIAAgbIAfAAIAAAbg");
	this.shape_15.setTransform(678.9,507.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgmA/QgPgQAAgeIAAgdQAAggASgSQARgRAbgBQAJABAHABQAHACAIADIgGAXIgMgEIgNgBQgNAAgJAKQgJALABATIAAABQAGgGAIgDQAHgDAKAAQAVAAAMAPQALAOAAAXQAAAYgOAPQgPAPgXAAQgYAAgQgRgAgMAAQgGACgEAFIAAALQAAASAHAKQAHAKAKAAQAKAAAFgJQAGgIAAgNQAAgNgGgIQgGgHgJAAQgIAAgGACg");
	this.shape_16.setTransform(669.6,501.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AAeBWIAAhPQgBgOgGgIQgHgHgMAAQgJAAgIAFQgJAFgDAIQgEAIAAAOIAABEIgUAAIAAirIAUAAIAAA+QAPgRAVAAQAOAAAKAFQALAFAFAKQADAKAAARIAABPg");
	this.shape_17.setTransform(819.5,500.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AApBWIg8hWIgcAaIAAA8IgXAAIAAirIAXAAIAABVIBUhVIAfAAIhHBGIBKBlg");
	this.shape_18.setTransform(805.7,500.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgtA3QgMgKAAgQQAAgJAFgHQAEgIAHgEQAHgEAIgCIATgDQAYgEAMgEIAAgFQABgNgHgFQgHgIgQAAQgOAAgIAGQgGAFgEANIgUgDQACgNAHgIQAGgIAMgFQAMgEAPAAQAQAAAKAEQALADAEAGQAEAGACAJIABATIAAAbQAAAeACAHQABAIAEAHIgVAAQgEgGgBgJQgLAKgLAEQgKAEgNAAQgVAAgKgKgAgEAIQgNACgGACQgFACgDAFQgDAEAAAGQAAAIAHAGQAGAGANAAQALAAAJgGQAJgFAFgJQADgIAAgNIAAgIQgLAEgWAEg");
	this.shape_19.setTransform(790.1,503.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgbBPQgMgIgHgPQgHgPAAgSQAAgTAHgOQAFgPANgJQAMgIAQAAQAKAAAJAFQAIAFAGAHIAAg+IAVAAIAACsIgTAAIAAgQQgNATgWAAQgOAAgNgJgAgVgNQgKAMAAAYQAAAXAKAMQAKAMANAAQANAAALgLQAJgLAAgYQAAgZgKgLQgKgNgOAAQgNAAgJAMg");
	this.shape_20.setTransform(776.3,501);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgKBWIAAirIAVAAIAACrg");
	this.shape_21.setTransform(767.4,500.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgQBXIAAhrIgTAAIAAgQIATAAIAAgNQAAgNACgGQADgIAHgFQAHgFAOAAQAJAAALACIgEASIgMgBQgJAAgFAEQgEAEAAALIAAAMIAZAAIAAAQIgZAAIAABrg");
	this.shape_22.setTransform(762,500.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgMBGQAHgDADgFQACgGABgLIgLAAIAAgYIAXAAIAAAYQAAANgFAJQgFAIgJAEgAgKg2IAAgYIAXAAIAAAYg");
	this.shape_23.setTransform(748.1,504.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgLAMIAAgXIAXAAIAAAXg");
	this.shape_24.setTransform(741.6,508.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AhGBbICAi0IATAAIiAC0gABDBXIAAgSIgtAAIAAgOIAwg3IAOAAIAAA5IAMAAIAAAMIgMAAIAAASgAAoA5IAbAAIAAgfgAhUgFQgJgHgCgMIARgBQACAGAEADQAFAEAHgBQAIABAFgFQAFgFAAgGQAAgGgEgEQgEgDgKAAIgHAAIADgMQAJAAAFgEQAEgDAAgGQAAgEgEgDQgDgDgHAAQgHAAgDACQgEAEgDAGIgQgDQAEgMAIgFQAHgFAOAAQARAAAIAGQAHAHAAAJQAAAGgEAFQgEAGgIACQALADAFAFQAEAGAAAIQAAALgJAIQgJAHgRAAQgRAAgIgFg");
	this.shape_25.setTransform(728.2,501.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AgjA3QgMgKgEgUIAVgDQACAMAIAHQAIAHANAAQAPAAAHgGQAHgGAAgIQAAgHgGgEQgEgDgSgEQgWgGgJgEQgJgEgFgHQgEgIAAgJQAAgIAEgIQADgHAHgEQAFgEAJgCQAIgDAKAAQANAAAMAEQALAEAFAIQAFAHACAMIgVADQgBgKgHgFQgHgGgLAAQgPAAgFAFQgHAFAAAGQABAEACAEQACADAGACIARAGQAXAGAJAEQAJACAEAIQAGAHAAALQAAAKgHAKQgFAJgMAFQgMAFgPAAQgWAAgOgKg");
	this.shape_26.setTransform(712.1,503.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AAZBWIgng/IgPAOIAAAxIgVAAIAAirIAVAAIAABhIAwgyIAcAAIgvAtIAzBPg");
	this.shape_27.setTransform(700.9,500.9);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("EgmqAD/Qg2AAgmgmQgmgmAAg2IAAj5QAAg2AmgmQAmgmA2AAMBNVAAAQA1AAAnAmQAmAmAAA2IAAD5QAAA2gmAmQgnAmg1AAg");
	this.shape_28.setTransform(900.5,500.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#0097A7").ss(3).p("AG4C5ItvAAQg2AAgmgmQgmgmAAg2IAAhtQAAg1AmgnQAmgmA2AAINvAAQA2AAAmAmQAmAnAAA1IAABtQAAA2gmAmQgmAmg2AAg");
	this.shape_29.setTransform(1091,437.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#05686D").s().p("Am3C5Qg2AAgmgmQgmgnAAg1IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA1gmAnQgmAmg2AAg");
	this.shape_30.setTransform(1091,437.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgPAOIAAgbIAfAAIAAAbg");
	this.shape_31.setTransform(678.9,445.4);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AglBEQgPgNABgUIAAgBIAegBQAAAKAGAHQAGAFAJAAQAKAAAGgHQAFgJAAgMQAAgNgGgJQgFgHgKAAQgJAAgFACQgFAEgCAFIgcgCIAJhVIBXAAIAAAYIg9AAIgEAlQAEgDAGgCQAGgCAHAAQAWgBANAOQAMANAAAZQAAAWgNAQQgOAPgZgBQgVABgQgMg");
	this.shape_32.setTransform(669.6,439.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AAdBWIAAhPQABgPgHgHQgHgHgMAAQgJAAgIAFQgJAFgDAIQgEAIABAOIAABEIgVAAIAAirIAVAAIAAA+QAOgRAVAAQAOgBALAGQAKAGAEAJQAFAKAAARIAABPg");
	this.shape_33.setTransform(774.2,438.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AAdBAIAAhLQABgNgDgGQgCgHgHgDQgGgEgJAAQgMAAgJAIQgKAJAAAYIAABDIgWAAIAAh8IATAAIAAASQAOgVAZAAQALAAAJAFQAKAEAFAGQAEAHACAJIABAUIAABMg");
	this.shape_34.setTransform(760.8,440.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AgKBWIAAh7IAVAAIAAB7gAALg9IAAgZIAWAAIAAAZgAggg9IAAgZIAWAAIAAAZg");
	this.shape_35.setTransform(750.9,438.5);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AgMAUQAHgDADgFQACgHABgJIgLAAIAAgYIAXAAIAAAYQAAAMgFAIQgFAJgJAEg");
	this.shape_36.setTransform(744.1,447.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("AgjA3QgMgKgDgUIAUgDQACAMAIAHQAIAHAOAAQAOAAAHgGQAHgGAAgIQAAgHgGgEQgFgDgQgEQgXgGgJgEQgJgEgEgHQgFgIAAgJQAAgIAEgIQAEgHAGgEQAFgEAIgCQAJgDAJAAQAOAAALAEQAMAEAFAIQAFAHACAMIgVADQgBgKgHgFQgGgGgMAAQgPAAgGAFQgFAFgBAGQAAAEADAEQADADAFACIASAGQAWAGAJAEQAIACAGAIQAEAHAAALQABAKgHAKQgFAJgMAFQgMAFgOAAQgYAAgNgKg");
	this.shape_37.setTransform(734.8,440.9);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("AAtBWIhaiGIAACGIgWAAIAAirIAYAAIBZCGIAAiGIAVAAIAACrg");
	this.shape_38.setTransform(720.1,438.6);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#000000").s().p("AAZA+IgUhJIgFgVIgYBeIgWAAIgnh7IAXAAIATBHIAIAaIAGgZIAUhIIAUAAIATBHIAHAYIAHgYIAVhHIAUAAIgnB7g");
	this.shape_39.setTransform(702.8,440.9);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("EgmqAD/Qg2AAgmgmQgmgnAAg1IAAj5QAAg2AmgmQAmgmA2AAMBNVAAAQA1AAAnAmQAmAmAAA2IAAD5QAAA1gmAnQgnAmg1AAg");
	this.shape_40.setTransform(900.5,438);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#0097A7").ss(3).p("AG4C5ItvAAQg2AAgmgmQgmgmAAg2IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA2gmAmQgmAmg2AAg");
	this.shape_41.setTransform(1091,375.4);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#05686D").s().p("Am3C5Qg2AAgmgmQgmgmAAg2IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA2gmAmQgmAmg2AAg");
	this.shape_42.setTransform(1091,375.4);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#000000").s().p("AgPAOIAAgbIAfAAIAAAbg");
	this.shape_43.setTransform(678.9,383.1);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#000000").s().p("AAIBOIAAgiIg/AAIgBgSIBAhnIAfAAIAABhIASAAIAAAYIgSAAIAAAigAAFgfIggAzIAjAAIAAg3IgBAAg");
	this.shape_44.setTransform(669.5,376.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#000000").s().p("AAeBWIAAhPQAAgOgHgIQgHgHgMAAQgJAAgIAFQgIAEgEAJQgDAIgBAOIAABEIgUAAIAAirIAUAAIAAA+QAPgRAVAAQAOAAALAFQAKAFAFAKQADAKAAARIAABPg");
	this.shape_45.setTransform(792.8,376.2);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#000000").s().p("AApBWIg8hWIgcAaIAAA8IgXAAIAAirIAXAAIAABVIBUhVIAfAAIhHBGIBKBlg");
	this.shape_46.setTransform(779,376.2);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#000000").s().p("AghBAIAAh8IATAAIAAATQAIgNAGgEQAFgFAHAAQALAAALAHIgHAUQgIgFgIAAQgHAAgEAFQgGAEgCAHQgEALAAAOIAABAg");
	this.shape_47.setTransform(767.1,378.4);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#000000").s().p("AgjA3QgMgKgEgUIAVgDQACAMAIAHQAIAHANAAQAPAAAHgGQAHgGAAgIQAAgHgGgEQgEgDgSgEQgWgGgJgEQgJgEgFgHQgEgIAAgJQAAgIAEgIQADgHAHgEQAFgEAJgCQAIgDAKAAQAOAAALAEQAKAEAGAIQAFAHACAMIgUADQgCgKgHgFQgGgGgMAAQgPAAgFAFQgHAFAAAGQABAEACAEQACADAGACIARAGQAXAGAJAEQAJACAEAIQAGAHAAALQAAAKgHAKQgGAJgLAFQgMAFgPAAQgXAAgNgKg");
	this.shape_48.setTransform(756.1,378.6);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#000000").s().p("AgJBWIAAirIATAAIAACrg");
	this.shape_49.setTransform(747.4,376.2);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#000000").s().p("AAaBWIgphAIgPAPIAAAxIgVAAIAAirIAVAAIAABhIAxgyIAcAAIgvAtIA0BPg");
	this.shape_50.setTransform(732.9,376.2);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#000000").s().p("AgMBGQAHgDADgFQACgGABgLIgLAAIAAgYIAXAAIAAAYQAAANgFAJQgFAIgJAEgAgKg2IAAgYIAXAAIAAAYg");
	this.shape_51.setTransform(722.8,380.3);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#000000").s().p("AhGBbICAi0IATAAIiAC0gABDBXIAAgSIgtAAIAAgOIAwg3IAOAAIAAA5IAMAAIAAAMIgMAAIAAASgAAoA5IAbAAIAAgfgAhUgFQgJgHgCgMIARgBQACAGAEADQAFAEAHgBQAIABAFgFQAFgFAAgGQAAgGgEgEQgEgDgKAAIgHAAIADgMQAJAAAFgEQAEgDAAgGQAAgEgEgDQgDgDgHAAQgHAAgDACQgEAEgDAGIgQgDQAEgMAIgFQAHgFAOAAQARAAAIAGQAHAHAAAJQAAAGgEAFQgEAGgIACQALADAFAFQAEAGAAAIQAAALgJAIQgJAHgRAAQgRAAgIgFg");
	this.shape_52.setTransform(709.5,376.4);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#000000").s().p("AgXBtIAEgSIAKACQAGAAADgEQADgFAAgRIAAiCIAVAAIAACDQAAAXgGAJQgIALgQAAQgJAAgIgCgAADhWIAAgYIAVAAIAAAYg");
	this.shape_53.setTransform(695.5,378.7);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("EgmqAD/Qg2AAgmgmQgmgmAAg2IAAj5QAAg2AmgmQAmgmA2AAMBNVAAAQA1AAAnAmQAmAmAAA2IAAD5QAAA2gmAmQgnAmg1AAg");
	this.shape_54.setTransform(900.5,375.6);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#0097A7").ss(3).p("AG4C5ItvAAQg2AAgmgmQgmgnAAg1IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA1gmAnQgmAmg2AAg");
	this.shape_55.setTransform(1091,313.1);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#05686D").s().p("Am3C5Qg2AAgmgmQgmgnAAg1IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA1gmAnQgmAmg2AAg");
	this.shape_56.setTransform(1091,313.1);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#000000").s().p("AgPAOIAAgbIAfAAIAAAbg");
	this.shape_57.setTransform(678.9,320.8);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#000000").s().p("AgmBFQgPgMAAgVIAAgBIAfAAQAAAJAGAHQAHAFAJAAQALAAAGgGQAGgGAAgLQAAgMgGgGQgGgFgLAAIgSAAIAAgWIASAAQAKAAAFgHQAGgFAAgLQAAgJgGgGQgFgFgKgBQgIAAgGAGQgGAFAAAIIgfAAIAAgBQAAgSAOgLQAPgNAWAAQAXAAAPAMQAOALAAAWQAAAKgGAJQgHAJgLAGQANADAHAKQAGAKAAAMQAAAVgPANQgQAMgXAAQgWAAgQgLg");
	this.shape_58.setTransform(669.3,314.4);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#000000").s().p("AAdBWIAAhPQAAgPgGgHQgHgHgNAAQgIAAgIAFQgIAFgEAIQgEAIAAAOIAABEIgVAAIAAirIAVAAIAAA+QAPgRAWAAQANgBAKAGQALAGAFAJQADAKAAARIAABPg");
	this.shape_59.setTransform(806.2,313.9);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#000000").s().p("AApBWIg8hWIgcAaIAAA8IgXAAIAAirIAXAAIAABVIBUhVIAfAAIhHBGIBKBlg");
	this.shape_60.setTransform(792.4,313.9);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#000000").s().p("AgtA3QgLgKAAgQQgBgJAFgHQAEgIAHgEQAGgEAJgCIASgDQAZgEAMgEIAAgFQAAgNgFgFQgJgIgPAAQgPAAgGAGQgIAFgDANIgVgDQADgNAGgIQAHgIAMgFQANgEAOAAQAQAAAKAEQALADAEAGQAEAGADAJIABATIAAAbQAAAeABAHQABAIAEAHIgWAAQgDgGgBgJQgLAKgMAEQgKAEgMAAQgVAAgKgKgAgEAIQgNACgGACQgFACgDAFQgDAEAAAGQAAAIAHAGQAGAGAMAAQALAAAKgGQAKgFAEgJQAEgIgBgNIAAgIQgLAEgWAEg");
	this.shape_61.setTransform(776.8,316.3);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#000000").s().p("AgbBPQgMgIgHgPQgHgOAAgUQAAgSAGgOQAGgPANgJQAMgIAQAAQAKAAAJAFQAJAFAFAHIAAg+IAVAAIAACrIgUAAIAAgPQgMASgWABQgOAAgNgJgAgVgNQgKAMAAAXQAAAYAKAMQAKAMANAAQANAAAKgMQAKgKAAgYQAAgZgKgMQgKgLgOgBQgNAAgJAMg");
	this.shape_62.setTransform(763,314.1);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#000000").s().p("AgJBWIAAirIATAAIAACrg");
	this.shape_63.setTransform(754.1,313.9);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#000000").s().p("AgQBXIAAhrIgTAAIAAgQIATAAIAAgNQAAgNABgGQAEgIAHgFQAHgFAOAAQAJAAALACIgDASIgNgBQgKAAgDAEQgFAEAAALIAAAMIAZAAIAAAQIgZAAIAABrg");
	this.shape_64.setTransform(748.7,313.8);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#000000").s().p("AAaBWIgpg/IgPANIAAAyIgVAAIAAirIAVAAIAABhIAxgyIAcAAIgvAtIA0BPg");
	this.shape_65.setTransform(732.9,313.9);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#000000").s().p("AgMBGQAHgDADgFQACgGABgLIgLAAIAAgYIAXAAIAAAYQAAANgFAJQgFAIgJAEgAgKg2IAAgYIAXAAIAAAYg");
	this.shape_66.setTransform(722.8,318);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#000000").s().p("AhGBbICAi1IATAAIiAC1gABDBXIAAgSIgtAAIAAgOIAwg3IAOAAIAAA5IAMAAIAAAMIgMAAIAAASgAAoA5IAbAAIAAgfgAhUgFQgJgHgCgMIARgBQACAGAEADQAFADAHAAQAIAAAFgEQAFgFAAgGQAAgGgEgEQgEgDgKAAIgHABIADgOQAJAAAFgDQAEgEAAgFQAAgEgEgDQgDgDgHAAQgHAAgDACQgEADgDAHIgQgCQAEgNAIgFQAHgFAOAAQARAAAIAGQAHAHAAAJQAAAHgEAEQgEAFgIADQALADAFAFQAEAGAAAIQAAALgJAIQgJAHgRAAQgRAAgIgFg");
	this.shape_67.setTransform(709.5,314.1);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#000000").s().p("AgXBtIAEgSIAKACQAGAAADgEQADgFAAgRIAAiCIAVAAIAACDQAAAXgGAJQgIALgQAAQgJAAgIgCgAADhWIAAgYIAVAAIAAAYg");
	this.shape_68.setTransform(695.5,316.4);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("EgmqAD/Qg2AAgmgmQgmgnAAg1IAAj5QAAg2AmgmQAmgmA2AAMBNVAAAQA1AAAnAmQAmAmAAA2IAAD5QAAA1gmAnQgnAmg1AAg");
	this.shape_69.setTransform(900.5,313.3);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#0097A7").ss(3).p("AG4C5ItvAAQg2AAgmgmQgmgmAAg2IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA2gmAmQgmAmg2AAg");
	this.shape_70.setTransform(1091,250.7);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#05686D").s().p("Am3C5Qg2AAgmgmQgmgmAAg2IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA2gmAmQgmAmg2AAg");
	this.shape_71.setTransform(1091,250.7);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#000000").s().p("AgPAOIAAgbIAfAAIAAAbg");
	this.shape_72.setTransform(678.9,258.5);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#000000").s().p("AgzBPIAAgUIAyg3QAKgLAFgJQAFgIAAgIQAAgKgGgGQgFgGgJAAQgLAAgFAHQgGAHAAANIgeAAIgBgBQAAgVAOgPQAPgOAYAAQAXAAAOAMQAOANAAAVQAAAOgIAMQgIALgRATIgbAfIAAAAIBBAAIAAAYg");
	this.shape_73.setTransform(669.4,251.9);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#000000").s().p("AAeBWIAAhPQAAgPgHgHQgHgHgMAAQgJAAgIAFQgIAEgEAJQgDAHgBAPIAABEIgUAAIAAirIAUAAIAAA+QAPgRAVgBQAOAAALAGQAKAGAEAJQAFAKAAARIAABPg");
	this.shape_74.setTransform(800.8,251.6);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#000000").s().p("AApBWIg8hWIgcAbIAAA7IgXAAIAAirIAXAAIAABVIBUhVIAfAAIhHBGIBKBlg");
	this.shape_75.setTransform(787,251.6);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#000000").s().p("AguA3QgKgKgBgQQABgJAEgHQAEgIAHgEQAGgEAJgCIATgDQAYgEANgEIAAgFQAAgNgHgFQgHgIgQAAQgPAAgHAGQgGAFgEANIgUgDQACgNAHgIQAGgIAMgFQANgEAPAAQAPAAALAEQAKADAEAGQAEAGACAJIABATIAAAbQAAAeACAHQACAIADAHIgVAAQgEgGgBgJQgMAKgKAEQgLAEgMAAQgUAAgMgKgAgEAIQgNACgGACQgFACgDAFQgDAEAAAGQAAAIAGAGQAHAGANAAQAKAAAKgGQAKgFAEgJQADgIABgNIAAgIQgMAEgWAEg");
	this.shape_76.setTransform(771.4,254);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#000000").s().p("AgaBPQgNgIgHgPQgHgOAAgUQAAgSAHgPQAFgPANgHQANgJAPAAQAKAAAJAFQAJAEAFAJIAAg/IAVAAIAACrIgTAAIAAgPQgMASgXAAQgOABgMgJgAgWgNQgJAMAAAXQAAAZAKALQAKAMANAAQAOAAAKgMQAJgKAAgYQAAgZgKgMQgKgMgOABQgNAAgKALg");
	this.shape_77.setTransform(757.7,251.8);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#000000").s().p("AgKBWIAAirIAVAAIAACrg");
	this.shape_78.setTransform(748.7,251.6);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#000000").s().p("AgRBXIAAhrIgTAAIAAgQIATAAIAAgNQAAgNADgGQACgIAJgFQAGgFAOAAQAJAAAKACIgDASIgMgBQgKAAgEAEQgDAEAAALIAAAMIAXAAIAAAQIgXAAIAABrg");
	this.shape_79.setTransform(743.3,251.5);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#000000").s().p("AAZBWIgng/IgPANIAAAyIgVAAIAAirIAVAAIAABhIAxgxIAbAAIgvAtIAzBOg");
	this.shape_80.setTransform(727.6,251.6);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#000000").s().p("AgMBGQAHgDADgFQACgGABgLIgLAAIAAgYIAXAAIAAAYQAAANgFAJQgFAIgJAEgAgKg2IAAgYIAXAAIAAAYg");
	this.shape_81.setTransform(717.4,255.7);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#000000").s().p("AA/BAIAAhOQAAgMgCgGQgCgFgFgEQgGgDgHAAQgNAAgJAJQgIAIAAATIAABIIgUAAIAAhQQAAgOgGgHQgFgHgMAAQgIAAgIAEQgIAFgDAJQgDAJAAARIAABAIgVAAIAAh8IASAAIAAASQAGgJAKgGQAKgGAMAAQAOAAAJAGQAIAGADAKQAPgWAYAAQASAAAKALQAKAKAAAVIAABVg");
	this.shape_82.setTransform(704.2,253.8);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("EgmqAD/Qg2AAgmgmQgmgmAAg2IAAj5QAAg2AmgmQAmgmA2AAMBNVAAAQA1AAAnAmQAmAmAAA2IAAD5QAAA2gmAmQgnAmg1AAg");
	this.shape_83.setTransform(900.5,251);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f().s("#0097A7").ss(3).p("AG4C5ItvAAQg2AAgmgmQgmgnAAg1IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA1gmAnQgmAmg2AAg");
	this.shape_84.setTransform(1091,188.4);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#05686D").s().p("Am3C5Qg2AAgmgmQgmgnAAg1IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA1gmAnQgmAmg2AAg");
	this.shape_85.setTransform(1091,188.4);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#000000").s().p("AgPAOIAAgbIAfAAIAAAbg");
	this.shape_86.setTransform(678.9,196.2);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#000000").s().p("AAABOIAAh/IgeAAIAAgXIA+gFIAACbg");
	this.shape_87.setTransform(668.2,189.7);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#000000").s().p("AAdBWIAAhPQABgPgHgHQgHgHgMAAQgJAAgIAFQgJAFgDAIQgEAIABAOIAABEIgVAAIAAirIAVAAIAAA+QAOgRAVAAQAOgBALAGQAKAGAEAJQAFAKAAARIAABPg");
	this.shape_88.setTransform(806.1,189.3);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#000000").s().p("AApBWIg8hWIgcAaIAAA8IgXAAIAAirIAXAAIAABVIBUhVIAfAAIhHBGIBKBlg");
	this.shape_89.setTransform(792.3,189.3);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#000000").s().p("AghBAIAAh8IATAAIAAATQAIgNAGgEQAFgFAHAAQALAAALAHIgHAUQgIgFgIAAQgHAAgEAFQgGAEgCAHQgEALAAAOIAABAg");
	this.shape_90.setTransform(780.4,191.5);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#000000").s().p("AgiA3QgNgKgEgUIAVgDQACAMAIAHQAIAHANAAQAPAAAHgGQAHgGAAgIQAAgHgGgEQgFgDgRgEQgWgGgJgEQgJgEgFgHQgEgIAAgJQAAgIAEgIQAEgHAGgEQAFgEAJgCQAIgDAKAAQAOAAAKAEQALAEAGAIQAFAHACAMIgUADQgCgKgGgFQgIgGgLAAQgOAAgHAFQgFAFAAAGQgBAEADAEQADADAFACIARAGQAXAGAJAEQAJACAEAIQAFAHABALQgBAKgFAKQgHAJgLAFQgMAFgPAAQgXAAgMgKg");
	this.shape_91.setTransform(769.4,191.6);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#000000").s().p("AgJBWIAAirIAUAAIAACrg");
	this.shape_92.setTransform(760.7,189.3);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#000000").s().p("AgMBGQAHgDADgFQACgGABgLIgLAAIAAgYIAXAAIAAAYQAAANgFAJQgFAIgJAEgAgKg2IAAgYIAXAAIAAAYg");
	this.shape_93.setTransform(748.1,193.3);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#000000").s().p("AgLAMIAAgXIAXAAIAAAXg");
	this.shape_94.setTransform(741.6,196.7);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#000000").s().p("AhGBbICAi1IATAAIiAC1gABDBXIAAgSIgtAAIAAgOIAwg3IAOAAIAAA5IAMAAIAAAMIgMAAIAAASgAAoA5IAbAAIAAgfgAhUgFQgJgHgCgMIARgBQACAGAEADQAFADAHAAQAIAAAFgEQAFgFAAgGQAAgGgEgEQgEgDgKAAIgHABIADgOQAJAAAFgDQAEgEAAgFQAAgEgEgDQgDgDgHAAQgHAAgDACQgEADgDAHIgQgDQAEgMAIgFQAHgFAOAAQARAAAIAGQAHAHAAAJQAAAHgEAEQgEAFgIADQALADAFAFQAEAGAAAIQAAALgJAIQgJAHgRAAQgRAAgIgFg");
	this.shape_95.setTransform(728.2,189.5);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#000000").s().p("AgjA3QgMgKgEgUIAVgDQACAMAIAHQAIAHANAAQAPAAAHgGQAHgGAAgIQAAgHgGgEQgEgDgSgEQgWgGgJgEQgJgEgFgHQgEgIAAgJQAAgIAEgIQADgHAHgEQAFgEAJgCQAIgDAKAAQANAAAMAEQALAEAFAIQAFAHACAMIgVADQgBgKgHgFQgHgGgLAAQgPAAgFAFQgHAFAAAGQABAEACAEQACADAGACIARAGQAXAGAJAEQAJACAEAIQAGAHAAALQAAAKgHAKQgFAJgMAFQgMAFgPAAQgWAAgOgKg");
	this.shape_96.setTransform(712.1,191.6);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#000000").s().p("AAZBWIgng/IgPANIAAAyIgVAAIAAirIAVAAIAABhIAwgyIAcAAIgvAtIAzBPg");
	this.shape_97.setTransform(700.9,189.3);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#FFFFFF").s().p("EgmqAD/Qg2AAgmgmQgmgmAAg2IAAj5QAAg2AmgmQAmgmA2AAMBNVAAAQA1AAAnAmQAmAmAAA2IAAD5QAAA2gmAmQgnAmg1AAg");
	this.shape_98.setTransform(900.5,188.7);

	this.instance_1 = new lib.Path_4();
	this.instance_1.parent = this;
	this.instance_1.setTransform(364,329.2,1,1,0,0,0,13.8,28.1);
	this.instance_1.alpha = 0.898;

	this.instance_2 = new lib.Path_1_2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(145.4,502.5,1,1,0,0,0,17.4,28.3);
	this.instance_2.alpha = 0.898;

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#000000").s().p("AAGA5IAAgZIgtAAIgBgOIAuhKIAWAAIAABGIAOAAIAAASIgOAAIAAAZgAAEgWIgXAkIAZAAIAAgoIAAAAg");
	this.shape_99.setTransform(290.1,413.6);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#FFB300").s().p("AhYBZQglglAAg0QAAgzAlglQAlglAzABQA0gBAlAlQAlAlAAAzQAAA0glAlQglAlg0gBQgzABglglg");
	this.shape_100.setTransform(289.9,413.1);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#FF6F00").s().p("AhnBoQgsgrABg9QgBg8AsgrQArgrA8gBQA9ABArArQArArABA8QgBA9grArQgrArg9ABQg8gBgrgrg");
	this.shape_101.setTransform(290,413.1);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#000000").s().p("AgbAyQgMgIABgQIAAAAIAWAAQAAAGAFAFQAEAEAHAAQAHAAAFgEQAFgFAAgHQAAgJgFgEQgEgFgIAAIgNAAIAAgQIANAAQAHABAEgFQAEgEAAgHQAAgHgEgFQgEgEgHAAQgGAAgEAEQgFAEAAAGIgWAAIAAgBQAAgNAKgJQALgJAQAAQARABAKAIQALAJAAAPQAAAIgFAGQgFAHgIADQAJADAFAHQAFAHAAAJQAAAQgLAJQgLAJgRAAQgQAAgLgJg");
	this.shape_102.setTransform(239.3,457.7);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#FFB300").s().p("AhYBZQglglAAg0QAAgzAlglQAlglAzAAQA0AAAlAlQAlAlAAAzQAAA0glAlQglAlg0AAQgzAAglglg");
	this.shape_103.setTransform(239.3,457.2);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#FF6F00").s().p("AhnBoQgsgrAAg9QAAg8AsgrQArgrA8gBQA9ABArArQAsArAAA8QAAA9gsArQgrAsg9AAQg8AAgrgsg");
	this.shape_104.setTransform(239.3,457.2);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f().s("#000000").ss(2).p("AgWAAIAtAA");
	this.shape_105.setTransform(405.3,356.3);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#000000").s().p("AJYAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBQAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAG4AKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBQAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAEYAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBQAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAB4AKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBQAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAgnAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBPAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAjHAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBQAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAlnAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBQAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAoHAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBQAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAqnAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBQAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAg");
	this.shape_106.setTransform(328.9,356.3);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f().s("#000000").ss(2).p("AgWAAIAtAA");
	this.shape_107.setTransform(249.8,356.3);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f().s("#000000").ss(2).p("AgWAAIAtAA");
	this.shape_108.setTransform(283.5,474.6);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#000000").s().p("AMcAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIAcAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAJoAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBaAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAG0AKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBaAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAEAAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBaAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgABMAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBaAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAhnAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBaAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAkbAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBaAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAnPAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBaAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAqDAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBaAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAs3AKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBaAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAg");
	this.shape_109.setTransform(194.1,474.6);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f().s("#000000").ss(2).p("AgWAAIAtAA");
	this.shape_110.setTransform(100,474.6);

	this.instance_3 = new lib.Path_3();
	this.instance_3.parent = this;
	this.instance_3.setTransform(252.3,420.8,1,1,0,0,0,13,14.1);
	this.instance_3.alpha = 0.898;

	this.instance_4 = new lib.Path_1_1();
	this.instance_4.parent = this;
	this.instance_4.setTransform(358.6,280.3,1,1,0,0,0,14,14.5);
	this.instance_4.alpha = 0.898;

	this.instance_5 = new lib.Path_2_0();
	this.instance_5.parent = this;
	this.instance_5.setTransform(135.2,571.1,1,1,0,0,0,14,14.5);
	this.instance_5.alpha = 0.898;

	this.instance_6 = new lib.Path_0();
	this.instance_6.parent = this;
	this.instance_6.setTransform(255.1,417.1,1,1,0,0,0,67.1,62.4);
	this.instance_6.alpha = 0.898;

	this.instance_7 = new lib.Path_1_0();
	this.instance_7.parent = this;
	this.instance_7.setTransform(151.5,548.5,1,1,0,0,0,40.5,72.6);
	this.instance_7.alpha = 0.898;

	this.instance_8 = new lib.Path_2();
	this.instance_8.parent = this;
	this.instance_8.setTransform(359.5,285.5,1,1,0,0,0,40.4,72.6);
	this.instance_8.alpha = 0.898;

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#558B2F").s().p("AqaanMAAAg1NIU0AAMAAAA1Ng");
	this.shape_111.setTransform(255.9,424.1);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#000000").s().p("AgRA5IABgMQADgZAIgUQAIgVAPgRIg5AAIAAgSIBPAAIAAASQgSAVgHASQgGASgDAaIgBAMg");
	this.shape_112.setTransform(74.4,475.1);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#FFB300").s().p("AhYBZQglglAAg0QAAgzAlglQAlglAzAAQA0AAAlAlQAlAlAAAzQAAA0glAlQglAlg0AAQgzAAglglg");
	this.shape_113.setTransform(74.4,474.6);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#FF6F00").s().p("AhnBoQgrgrAAg9QAAg8ArgrQArgsA8AAQA9AAAsAsQArArAAA8QAAA9grArQgsAsg9AAQg8AAgrgsg");
	this.shape_114.setTransform(74.4,474.6);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#000000").s().p("AgRA5IABgMQADgZAIgUQAIgVAPgRIg5AAIAAgSIBPAAIAAASQgSAVgHASQgGASgDAaIgBAMg");
	this.shape_115.setTransform(430.5,356.8);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#FFB300").s().p("AhYBZQglglAAg0QAAgzAlglQAlglAzAAQA0AAAlAlQAlAlAAAzQAAA0glAlQglAlg0AAQgzAAglglg");
	this.shape_116.setTransform(430.5,356.2);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#FF6F00").s().p("AhoBoQgrgrAAg9QAAg8ArgsQAsgrA8AAQA9AAArArQArAsAAA8QAAA9grArQgrAsg9gBQg8ABgsgsg");
	this.shape_117.setTransform(430.5,356.3);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#000000").s().p("AgbAvQgLgNAAgWIAAgUQAAgXAMgNQANgOAUAAQAGABAFABIALAEIgEAQIgJgDIgJgBQgKAAgGAIQgGAIAAAOIAAAAQAEgEAGgCQAFgDAHAAQAPAAAJAMQAJAJgBARQABASgLALQgLALgRgBQgQABgMgMgAgIAAQgFACgDADIAAAIQAAANAGAIQAFAHAGAAQAIAAAEgHQAEgGAAgJQAAgJgFgGQgEgFgHAAQgFAAgEABg");
	this.shape_118.setTransform(415,231.4);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#FFB300").s().p("AhYBZQglglAAg0QAAgzAlglQAlglAzAAQA0AAAlAlQAlAlAAAzQAAA0glAlQglAlg0AAQgzAAglglg");
	this.shape_119.setTransform(414.7,230.9);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#FF6F00").s().p("AhoBoQgrgrAAg9QAAg8ArgrQAsgrA8gBQA9ABArArQArArAAA8QAAA9grArQgrArg9AAQg8AAgsgrg");
	this.shape_120.setTransform(414.8,231);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#000000").s().p("AgaAxQgLgIAAgQIABAAIAVgBQAAAHAFAFQAEAEAGAAQAIAAAEgGQAEgFAAgKQAAgJgFgGQgEgFgHAAQgGAAgEABQgDADgCAEIgUgBIAHg+IA+AAIAAASIgrAAIgEAaIAIgEQAFgBAEgBQAQAAAJAKQAJAKAAARQAAARgJALQgKALgTAAQgPAAgLgJg");
	this.shape_121.setTransform(355.6,336);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#FFB300").s().p("AhYBZQglglAAg0QAAgzAlglQAlgkAzAAQA0AAAlAkQAlAlAAAzQAAA0glAlQglAlg0AAQgzAAglglg");
	this.shape_122.setTransform(355.5,335.4);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#FF6F00").s().p("AhnBoQgrgrgBg9QABg8ArgsQArgqA8gBQA9ABArAqQAsAsAAA8QAAA9gsArQgrArg9ABQg8gBgrgrg");
	this.shape_123.setTransform(355.5,335.4);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#000000").s().p("AglA6IAAgPIAlgoQAHgIADgGQADgGAAgGQAAgHgDgEQgEgFgGAAQgIAAgEAFQgEAGAAAJIgXAAIAAgBQAAgPAKgLQALgLASAAQAQAAAKAKQAKAJAAAPQAAAKgFAJQgGAIgNAOIgTAWIAAAAIAvAAIAAASg");
	this.shape_124.setTransform(157.8,495.5);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#FFB300").s().p("AhYBZQgkglAAg0QAAgzAkglQAlglAzAAQA0AAAlAlQAlAlAAAzQAAA0glAlQglAlg0AAQgzAAglglg");
	this.shape_125.setTransform(157.7,495.1);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#FF6F00").s().p("AhoBoQgrgrAAg9QAAg8ArgrQAsgsA8AAQA9AAArAsQAsArgBA8QABA9gsArQgrAsg9AAQg8AAgsgsg");
	this.shape_126.setTransform(157.7,495.1);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#000000").s().p("AAAA5IAAhdIgWAAIAAgQIAtgEIAABxg");
	this.shape_127.setTransform(100.9,591.5);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("#FFB300").s().p("AhYBZQglglAAg0QAAgzAlglQAlglAzAAQA0AAAlAlQAlAlAAAzQAAA0glAlQglAlg0AAQgzAAglglg");
	this.shape_128.setTransform(101.7,591);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("#FF6F00").s().p("AhoBoQgqgrAAg9QAAg8AqgrQAsgsA8AAQA9AAArAsQArArAAA8QAAA9grArQgrAsg9AAQg8AAgsgsg");
	this.shape_129.setTransform(101.8,591);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#FFFFFF").s().p("AgGAfIgFggIAAgcIAXAAIAAAcIgFAgg");
	this.shape_130.setTransform(1089.6,81.2);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f("#FFFFFF").s().p("AAaBWIgphAIgPAPIAAAxIgVAAIAAirIAVAAIAABhIAxgxIAcAAIgvAtIA0BOg");
	this.shape_131.setTransform(1082.1,86.7);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f("#FFFFFF").s().p("AgtA3QgMgKAAgQQAAgJAFgHQAEgIAHgEQAHgEAIgCIATgDQAYgEAMgEIAAgFQABgNgHgFQgHgIgQAAQgOAAgIAGQgGAFgEANIgUgDQACgNAHgIQAGgIAMgFQAMgEAPAAQAQAAAKAEQALADAEAGQAEAGACAJIABATIAAAbQAAAeACAHQABAIAEAHIgVAAQgEgGgBgJQgLAKgLAEQgKAEgNAAQgVAAgKgKgAgEAIQgNACgGACQgFACgDAFQgDAEAAAGQAAAIAHAGQAGAGANAAQALAAAJgGQAJgFAFgJQADgIAAgNIAAgIQgLAEgWAEg");
	this.shape_132.setTransform(1068.6,89.1);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f("#FFFFFF").s().p("AAZBWIgohAIgOAPIAAAxIgWAAIAAirIAWAAIAABhIAwgxIAcAAIgvAtIA0BOg");
	this.shape_133.setTransform(1056.8,86.7);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f("#FFFFFF").s().p("AgpAwQgQgQAAggQAAgiATgQQAQgOAWAAQAaAAAQARQAQARAAAdQAAAXgHAOQgHAOgOAHQgOAIgQAAQgZAAgQgRgAgZgjQgLAMAAAXQAAAYALAMQAKAMAPAAQAQAAAKgMQALgMAAgYQAAgXgLgMQgKgMgQAAQgPAAgKAMg");
	this.shape_134.setTransform(1043.3,89.1);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f("#FFFFFF").s().p("AgbBPQgMgIgHgOQgHgQAAgTQAAgSAGgPQAHgPAMgHQANgJAPAAQAKAAAJAFQAIAEAGAJIAAg+IAVAAIAACrIgUAAIAAgQQgLASgXAAQgOAAgNgIgAgVgNQgKALAAAYQAAAZAKALQAKAMANAAQANAAAKgMQAKgLAAgXQAAgYgKgMQgKgMgOAAQgNAAgJALg");
	this.shape_135.setTransform(1029.5,86.9);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f("#FFFFFF").s().p("AAdBWIAAhPQABgPgHgHQgHgHgNAAQgIAAgIAFQgJAFgDAIQgEAHABAPIAABEIgWAAIAAirIAWAAIAAA+QAOgSAWAAQAOABAKAFQAKAGAEAJQAEAKABARIAABPg");
	this.shape_136.setTransform(1016.6,86.7);

	this.shape_137 = new cjs.Shape();
	this.shape_137.graphics.f("#FFFFFF").s().p("AgRBXIAAhrIgSAAIAAgQIASAAIAAgNQAAgNACgGQAEgIAIgFQAGgFAOAAQAIAAALACIgCASIgNgBQgKAAgDAEQgEAEgBALIAAAMIAYAAIAAAQIgYAAIAABrg");
	this.shape_137.setTransform(1007.2,86.6);

	this.shape_138 = new cjs.Shape();
	this.shape_138.graphics.f("#FFFFFF").s().p("AgiA3QgNgKgDgUIAUgDQACAMAIAHQAIAHAOAAQAOAAAHgGQAHgGAAgIQAAgHgGgEQgFgDgQgEQgXgGgJgEQgJgEgEgHQgFgIAAgJQAAgIAEgIQADgHAHgEQAFgEAIgCQAJgDAJAAQAOAAALAEQALAEAGAIQAFAHACAMIgUADQgCgKgGgFQgIgGgLAAQgOAAgHAFQgFAFAAAGQAAAEACAEQADADAFACIASAGQAWAGAJAEQAIACAGAIQAEAHAAALQAAAKgFAKQgHAJgLAFQgMAFgOAAQgXAAgNgKg");
	this.shape_138.setTransform(990.6,89.1);

	this.shape_139 = new cjs.Shape();
	this.shape_139.graphics.f("#FFFFFF").s().p("AgtBVIgDgUQAIACAEAAQAIAAAEgDQAEgBADgFQACgDAEgNIACgFIgvh7IAXAAIAaBIIAIAcQADgOAFgOIAbhIIAVAAIgvB9QgIAVgEAIQgFAKgHAEQgHAGgKgBQgHAAgHgCg");
	this.shape_139.setTransform(978.8,91.6);

	this.shape_140 = new cjs.Shape();
	this.shape_140.graphics.f("#FFFFFF").s().p("AgXBtIAEgSIAKACQAGAAADgEQADgFgBgRIAAiCIAWAAIAACDQAAAXgGAJQgIALgQAAQgJAAgIgCgAAChWIAAgYIAWAAIAAAYg");
	this.shape_140.setTransform(968.6,89.2);

	this.shape_141 = new cjs.Shape();
	this.shape_141.graphics.f("#FFFFFF").s().p("AgRBXIAAhrIgTAAIAAgQIATAAIAAgNQAAgNACgGQAEgIAIgFQAGgFAOAAQAIAAALACIgCASIgNgBQgKAAgDAEQgEAEAAALIAAAMIAXAAIAAAQIgXAAIAABrg");
	this.shape_141.setTransform(964.5,86.6);

	this.shape_142 = new cjs.Shape();
	this.shape_142.graphics.f("#FFFFFF").s().p("AAgBYIAAg9QgEAGgJAFQgJAFgKAAQgVAAgQgSQgQgRAAgfQAAgSAGgPQAHgPAMgIQAMgHAPgBQAXAAANAUIAAgQIATAAIAACrgAgWg6QgJAMAAAYQgBAYAKALQALAMANABQANAAAKgMQAKgMAAgVQAAgZgLgNQgKgNgNAAQgNAAgKAMg");
	this.shape_142.setTransform(953.6,91.3);

	this.shape_143 = new cjs.Shape();
	this.shape_143.graphics.f("#FFFFFF").s().p("AgkBPQgOgKABgUIAUADQABAKAGAEQAIAFANABQAOgBAIgFQAIgGADgLQABgGABgVQgPARgTAAQgZAAgOgTQgPgSAAgZQAAgSAHgPQAGgQANgHQALgJARAAQAVAAAOASIAAgPIAUAAIAABqQAAAdgGANQgGAMgMAHQgOAHgSAAQgWAAgNgKgAgWg7QgKAMAAAWQAAAYAKALQAJALAOAAQAOAAAKgLQAKgLAAgXQAAgXgKgLQgKgMgPAAQgMAAgKALg");
	this.shape_143.setTransform(940.2,91.5);

	this.shape_144 = new cjs.Shape();
	this.shape_144.graphics.f("#FFFFFF").s().p("AgbBPQgMgIgHgOQgHgQAAgTQAAgSAGgPQAGgPANgHQAMgJAQAAQAKAAAJAFQAIAEAGAJIAAg+IAVAAIAACrIgUAAIAAgQQgMASgWAAQgOAAgNgIgAgVgNQgKALAAAYQAAAZAKALQAKAMANAAQANAAAKgMQAKgLAAgXQAAgYgKgMQgKgMgOAAQgNAAgJALg");
	this.shape_144.setTransform(926.8,86.9);

	this.shape_145 = new cjs.Shape();
	this.shape_145.graphics.f("#FFFFFF").s().p("AgJBWIAAirIATAAIAACrg");
	this.shape_145.setTransform(917.9,86.7);

	this.shape_146 = new cjs.Shape();
	this.shape_146.graphics.f("#FFFFFF").s().p("AgQBXIAAhrIgTAAIAAgQIATAAIAAgNQAAgNABgGQAEgIAHgFQAHgFAOAAQAJAAALACIgDASIgNgBQgKAAgDAEQgFAEAAALIAAAMIAZAAIAAAQIgZAAIAABrg");
	this.shape_146.setTransform(912.5,86.6);

	this.shape_147 = new cjs.Shape();
	this.shape_147.graphics.f("#FFFFFF").s().p("AgjA3QgMgKgDgUIAUgDQACAMAIAHQAIAHAOAAQAOAAAHgGQAHgGAAgIQAAgHgGgEQgFgDgQgEQgXgGgJgEQgJgEgEgHQgFgIAAgJQAAgIAEgIQAEgHAGgEQAFgEAIgCQAJgDAJAAQAOAAAMAEQALAEAFAIQAFAHACAMIgVADQgBgKgHgFQgGgGgMAAQgPAAgFAFQgGAFgBAGQAAAEADAEQADADAFACIASAGQAWAGAJAEQAIACAGAIQAEAHAAALQABAKgHAKQgFAJgMAFQgMAFgOAAQgYAAgNgKg");
	this.shape_147.setTransform(895.9,89.1);

	this.shape_148 = new cjs.Shape();
	this.shape_148.graphics.f("#FFFFFF").s().p("AgMBGQAHgDADgFQACgGABgLIgLAAIAAgYIAXAAIAAAYQAAANgFAJQgFAIgJAEgAgKg2IAAgYIAXAAIAAAYg");
	this.shape_148.setTransform(886.6,90.8);

	this.shape_149 = new cjs.Shape();
	this.shape_149.graphics.f("#FFFFFF").s().p("AgoAwQgQgRAAgeQAAgeAQgSQAQgRAZAAQAZAAAPARQAQARAAAeIAAAFIhcAAQACAVAKALQAKALAPAAQAMAAAIgGQAIgGAFgOIAWADQgGATgOALQgNAKgWAAQgaAAgQgRgAgWglQgKAKgBAQIBEAAQgBgQgHgHQgKgNgQAAQgOAAgJAKg");
	this.shape_149.setTransform(876.6,89.1);

	this.shape_150 = new cjs.Shape();
	this.shape_150.graphics.f("#FFFFFF").s().p("AAYA+IgThJIgFgVIgYBeIgWAAIgmh7IAVAAIAVBHIAGAaIAHgZIAUhIIAVAAIATBHIAFAYIAHgYIAWhHIAVAAIgnB7g");
	this.shape_150.setTransform(861.2,89.1);

	this.shape_151 = new cjs.Shape();
	this.shape_151.graphics.f("#FFFFFF").s().p("AgtA3QgLgKAAgQQgBgJAFgHQAEgIAHgEQAGgEAJgCIASgDQAZgEAMgEIAAgFQAAgNgFgFQgJgIgPAAQgOAAgIAGQgGAFgEANIgVgDQADgNAGgIQAHgIAMgFQAMgEAPAAQAQAAAKAEQALADAEAGQAFAGABAJIACATIAAAbQAAAeABAHQABAIAEAHIgVAAQgEgGgBgJQgLAKgLAEQgKAEgNAAQgVAAgKgKgAgEAIQgNACgGACQgFACgDAFQgDAEAAAGQAAAIAHAGQAGAGANAAQALAAAJgGQAJgFAFgJQAEgIgBgNIAAgIQgLAEgWAEg");
	this.shape_151.setTransform(839.2,89.1);

	this.shape_152 = new cjs.Shape();
	this.shape_152.graphics.f("#FFFFFF").s().p("AAdBWIAAhPQABgPgHgHQgHgHgMAAQgJAAgIAFQgJAFgDAIQgEAHABAPIAABEIgVAAIAAirIAVAAIAAA+QAOgSAVAAQAOABALAFQAKAGAEAJQAFAKAAARIAABPg");
	this.shape_152.setTransform(825.9,86.7);

	this.shape_153 = new cjs.Shape();
	this.shape_153.graphics.f("#FFFFFF").s().p("AgRBXIAAhrIgTAAIAAgQIATAAIAAgNQAAgNACgGQAEgIAIgFQAGgFAOAAQAIAAALACIgCASIgNgBQgKAAgDAEQgEAEAAALIAAAMIAXAAIAAAQIgXAAIAABrg");
	this.shape_153.setTransform(816.5,86.6);

	this.shape_154 = new cjs.Shape();
	this.shape_154.graphics.f("#FFFFFF").s().p("AgHBRQgLgIgGgNIASgIQAGALAFAEQAGAEAHAAQAIAAAGgGQAHgGAAgIQAAgHgDgEQgDgDgLgIQgUgNgDgHQgEgGgBgGIACgKIAGgPQAHgKABgFQACgFAAgEQAAgHgGgGQgGgGgKAAQgMAAgJAIQgHAIgBAaIAABzIgVAAIAAh0QAAgWAGgLQAFgMAMgGQAMgHAQAAQASAAAMAKQAMAKAAAOQAAAGgCAGIgIAQIgGANIgCAGQAAAEADADQACADAMAIQAQALAFAGQAIAJAAAMQAAAQgNAMQgLAMgTAAQgPAAgKgIg");
	this.shape_154.setTransform(805.8,86.7);

	this.shape_155 = new cjs.Shape();
	this.shape_155.graphics.f("#FFFFFF").s().p("Ag/BsIAAirIB7AAIAAAUIhlAAIAAA0IBfAAIAAAUIhfAAIAAA6IBpAAIAAAVgAAAhKIgbghIAcAAIAQAhg");
	this.shape_155.setTransform(790.3,84.6);

	this.shape_156 = new cjs.Shape();
	this.shape_156.graphics.f("#FFFFFF").s().p("AghBFIAAAQIgUAAIAAirIAVAAIAAA9QANgRAVAAQALAAAKAFQALAFAGAIQAGAJAEALQAEALAAAOQAAAfgQASQgQARgVAAQgWAAgMgSgAgYgMQgKALABAXQgBAWAHAJQAJARASAAQAMAAAKgMQAKgMAAgYQAAgXgJgMQgKgLgNAAQgNAAgLAMg");
	this.shape_156.setTransform(775.6,86.9);

	this.shape_157 = new cjs.Shape();
	this.shape_157.graphics.f("#FFFFFF").s().p("AAdBWIAAhPQABgPgHgHQgHgHgMAAQgJAAgIAFQgJAFgDAIQgDAHAAAPIAABEIgVAAIAAirIAVAAIAAA+QAOgSAWAAQANABALAFQAKAGAEAJQAFAKAAARIAABPg");
	this.shape_157.setTransform(755.3,86.7);

	this.shape_158 = new cjs.Shape();
	this.shape_158.graphics.f("#FFFFFF").s().p("AgJBWIAAirIAUAAIAACrg");
	this.shape_158.setTransform(745.9,86.7);

	this.shape_159 = new cjs.Shape();
	this.shape_159.graphics.f("#FFFFFF").s().p("AAhA+IgagmIgHgLIgfAxIgaAAIAtg/Igqg8IAaAAIATAdIAJAOIAJgOIAVgdIAZAAIgrA6IAuBBg");
	this.shape_159.setTransform(737.3,89.1);

	this.shape_160 = new cjs.Shape();
	this.shape_160.graphics.f("#FFFFFF").s().p("AAZA+IgUhJIgFgVIgYBeIgWAAIgmh7IAVAAIAUBHIAHAaIAHgZIAUhIIAVAAIATBHIAFAYIAHgYIAWhHIAVAAIgoB7g");
	this.shape_160.setTransform(722.5,89.1);

	this.shape_161 = new cjs.Shape();
	this.shape_161.graphics.f("#FFFFFF").s().p("Ag4AKIAAgTIBxgwIAAAVIhaAkIBaAlIAAAVg");
	this.shape_161.setTransform(700.3,86.8);

	this.shape_162 = new cjs.Shape();
	this.shape_162.graphics.f("#FFFFFF").s().p("AgaBPQgNgIgHgOQgHgQAAgTQAAgSAHgPQAFgPANgHQAMgJAQAAQAKAAAJAFQAJAEAFAJIAAg+IAVAAIAACrIgTAAIAAgQQgMASgXAAQgOAAgMgIgAgWgNQgJALAAAYQAAAZAKALQAKAMANAAQAOAAAKgMQAJgLAAgXQAAgYgKgMQgKgMgOAAQgNAAgKALg");
	this.shape_162.setTransform(686.1,86.9);

	this.shape_163 = new cjs.Shape();
	this.shape_163.graphics.f("#FFFFFF").s().p("AgpAwQgQgQAAggQAAgiATgQQAQgOAWAAQAaAAAQARQAQARAAAdQAAAXgHAOQgHAOgOAHQgOAIgQAAQgZAAgQgRgAgZgjQgLAMAAAXQAAAYALAMQAKAMAPAAQAQAAAKgMQALgMAAgYQAAgXgLgMQgKgMgQAAQgPAAgKAMg");
	this.shape_163.setTransform(673.2,89.1);

	this.shape_164 = new cjs.Shape();
	this.shape_164.graphics.f("#FFFFFF").s().p("AAYA+IgThJIgFgVIgYBeIgWAAIgmh7IAVAAIAVBHIAGAaIAHgZIAUhIIAVAAIATBHIAFAYIAHgYIAWhHIAVAAIgnB7g");
	this.shape_164.setTransform(657.8,89.1);

	this.shape_165 = new cjs.Shape();
	this.shape_165.graphics.f("#FFFFFF").s().p("AgkBPQgOgKAAgUIAVADQACAKAFAEQAIAFAOABQANgBAIgFQAIgGADgLQABgGABgVQgOARgUAAQgaAAgNgTQgPgSAAgZQAAgSAHgPQAGgQANgHQAMgJAQAAQAVAAAOASIAAgPIAUAAIAABqQAAAdgGANQgGAMgNAHQgNAHgRAAQgXAAgNgKgAgWg7QgKAMAAAWQAAAYAJALQALALANAAQAOAAALgLQAJgLAAgXQAAgXgKgLQgKgMgPAAQgMAAgKALg");
	this.shape_165.setTransform(635.5,91.5);

	this.shape_166 = new cjs.Shape();
	this.shape_166.graphics.f("#FFFFFF").s().p("AgcA7QgJgEgFgGQgFgHgCgJIgBgUIAAhMIAVAAIAABEIABAXQADAIAGAFQAGAEAKAAQAIAAAJgEQAIgFAEgJQADgIAAgQIAAhCIAVAAIAAB8IgTAAIAAgTQgOAWgYAAQgLAAgKgFg");
	this.shape_166.setTransform(622.5,89.2);

	this.shape_167 = new cjs.Shape();
	this.shape_167.graphics.f("#FFFFFF").s().p("AAaBWIgphAIgOAPIAAAxIgWAAIAAirIAWAAIAABhIAwgxIAcAAIgvAtIA0BOg");
	this.shape_167.setTransform(610.6,86.7);

	this.shape_168 = new cjs.Shape();
	this.shape_168.graphics.f("#FFFFFF").s().p("AAhBYIAAg9QgGAGgIAFQgJAFgKAAQgVAAgQgSQgQgRAAgfQAAgSAHgPQAGgPAMgIQANgHAPgBQAWAAANAUIAAgQIATAAIAACrgAgWg6QgKAMABAYQAAAYAKALQAKAMANABQAOAAAJgMQAKgMAAgVQAAgZgKgNQgLgNgOAAQgMAAgKAMg");
	this.shape_168.setTransform(590.1,91.3);

	this.shape_169 = new cjs.Shape();
	this.shape_169.graphics.f("#FFFFFF").s().p("AgJBWIAAh7IAUAAIAAB7gAgJg9IAAgYIAUAAIAAAYg");
	this.shape_169.setTransform(581.2,86.7);

	this.shape_170 = new cjs.Shape();
	this.shape_170.graphics.f("#FFFFFF").s().p("AgtBtIgDgUQAHACAGAAQAGAAAFgCQAEgCADgFQACgDAEgMIACgGIgvh7IAXAAIAaBHIAIAdQADgPAFgOIAbhHIAVAAIgvB9QgIAVgEAIQgFAKgHAFQgHAFgLAAQgFAAgIgDgAALhXIAAgYIAWAAIAAAYgAgghXIAAgYIAXAAIAAAYg");
	this.shape_170.setTransform(572.6,89.2);

	this.shape_171 = new cjs.Shape();
	this.shape_171.graphics.f("#FFFFFF").s().p("AAhBYIAAg9QgGAGgIAFQgJAFgKAAQgVAAgQgSQgQgRAAgfQAAgSAHgPQAGgPANgIQAMgHAPgBQAWAAANAUIAAgQIATAAIAACrgAgWg6QgKAMAAAYQAAAYALALQAKAMANABQAOAAAJgMQAKgMAAgVQAAgZgKgNQgLgNgOAAQgMAAgKAMg");
	this.shape_171.setTransform(559.4,91.3);

	this.shape_172 = new cjs.Shape();
	this.shape_172.graphics.f("#FFFFFF").s().p("AgKBWIAAh7IAVAAIAAB7gAgKg9IAAgYIAVAAIAAAYg");
	this.shape_172.setTransform(550.5,86.7);

	this.shape_173 = new cjs.Shape();
	this.shape_173.graphics.f("#FFFFFF").s().p("AglBPQgNgKABgUIAUADQACAKAFAEQAIAFANABQAOgBAIgFQAIgGADgLQACgGgBgVQgOARgTAAQgZAAgOgTQgPgSAAgZQAAgSAHgPQAGgQAMgHQANgJAQAAQAVAAAOASIAAgPIAUAAIAABqQAAAdgGANQgGAMgMAHQgNAHgTAAQgVAAgPgKgAgWg7QgKAMAAAWQAAAYAKALQAKALANAAQAOAAAKgLQAKgLAAgXQAAgXgKgLQgKgMgPAAQgNAAgJALg");
	this.shape_173.setTransform(534.1,91.5);

	this.shape_174 = new cjs.Shape();
	this.shape_174.graphics.f("#FFFFFF").s().p("AgbA7QgKgEgFgGQgEgHgDgJIgBgUIAAhMIAWAAIAABEIABAXQACAIAGAFQAHAEAJAAQAIAAAJgEQAIgFAEgJQADgIAAgQIAAhCIAWAAIAAB8IgUAAIAAgTQgOAWgYAAQgLAAgJgFg");
	this.shape_174.setTransform(521.1,89.2);

	this.shape_175 = new cjs.Shape();
	this.shape_175.graphics.f("#FFFFFF").s().p("AgPBsIAAirIAWAAIAACrgAgVhKIAQghIAbAAIgZAhg");
	this.shape_175.setTransform(511.6,84.6);

	this.shape_176 = new cjs.Shape();
	this.shape_176.graphics.f("#FFFFFF").s().p("AgjA3QgMgKgEgUIAVgDQACAMAIAHQAIAHANAAQAPAAAHgGQAHgGAAgIQAAgHgGgEQgFgDgRgEQgWgGgJgEQgJgEgFgHQgEgIAAgJQAAgIAEgIQADgHAHgEQAFgEAJgCQAIgDAKAAQAOAAALAEQAKAEAGAIQAFAHACAMIgUADQgCgKgHgFQgHgGgLAAQgPAAgFAFQgHAFABAGQgBAEADAEQACADAGACIARAGQAXAGAJAEQAJACAEAIQAFAHABALQgBAKgGAKQgGAJgLAFQgMAFgPAAQgXAAgNgKg");
	this.shape_176.setTransform(501.7,89.1);

	this.shape_177 = new cjs.Shape();
	this.shape_177.graphics.f("#FFFFFF").s().p("AgJBWIAAirIAUAAIAACrg");
	this.shape_177.setTransform(493.1,86.7);

	this.shape_178 = new cjs.Shape();
	this.shape_178.graphics.f("#FFFFFF").s().p("AgKBXIAAh8IAUAAIAAB8gAALg+IAAgXIAWAAIAAAXgAggg+IAAgXIAWAAIAAAXg");
	this.shape_178.setTransform(480.6,86.7);

	this.shape_179 = new cjs.Shape();
	this.shape_179.graphics.f("#FFFFFF").s().p("AAaBWIgphAIgPAPIAAAxIgVAAIAAirIAVAAIAABhIAygxIAbAAIgvAtIA0BOg");
	this.shape_179.setTransform(471.9,86.7);

	this.shape_180 = new cjs.Shape();
	this.shape_180.graphics.f("#FFFFFF").s().p("AgJBWIAAh7IATAAIAAB7gAgJg9IAAgYIATAAIAAAYg");
	this.shape_180.setTransform(455.8,86.7);

	this.shape_181 = new cjs.Shape();
	this.shape_181.graphics.f("#FFFFFF").s().p("AglBPQgNgKAAgUIAVADQABAKAGAEQAIAFANABQAOgBAIgFQAIgGADgLQABgGAAgVQgNARgUAAQgaAAgOgTQgOgSAAgZQAAgSAGgPQAHgQAMgHQANgJAQAAQAVAAAPASIAAgPIATAAIAABqQAAAdgGANQgGAMgMAHQgNAHgTAAQgVAAgPgKgAgWg7QgKAMAAAWQAAAYAJALQALALANAAQAPAAAJgLQAKgLAAgXQAAgXgKgLQgKgMgOAAQgOAAgJALg");
	this.shape_181.setTransform(446.1,91.5);

	this.shape_182 = new cjs.Shape();
	this.shape_182.graphics.f("#FFFFFF").s().p("AgaBPQgNgIgHgOQgHgQAAgTQAAgSAGgPQAHgPAMgHQANgJAPAAQAKAAAJAFQAIAEAGAJIAAg+IAVAAIAACrIgTAAIAAgQQgMASgXAAQgOAAgMgIgAgWgNQgJALAAAYQAAAZAKALQAKAMANAAQANAAALgMQAJgLAAgXQAAgYgKgMQgKgMgOAAQgNAAgKALg");
	this.shape_182.setTransform(432.7,86.9);

	this.shape_183 = new cjs.Shape();
	this.shape_183.graphics.f("#FFFFFF").s().p("AgKBWIAAirIAUAAIAACrg");
	this.shape_183.setTransform(423.7,86.7);

	this.shape_184 = new cjs.Shape();
	this.shape_184.graphics.f("#FFFFFF").s().p("AgRBXIAAhrIgTAAIAAgQIATAAIAAgNQAAgNADgGQACgIAJgFQAGgFAOAAQAJAAAKACIgDASIgMgBQgKAAgEAEQgDAEAAALIAAAMIAXAAIAAAQIgXAAIAABrg");
	this.shape_184.setTransform(418.3,86.6);

	this.shape_185 = new cjs.Shape();
	this.shape_185.graphics.f("#FFFFFF").s().p("AguA3QgKgKgBgQQABgJAEgHQAEgIAHgEQAGgEAJgCIATgDQAYgEANgEIAAgFQAAgNgHgFQgHgIgQAAQgPAAgHAGQgGAFgEANIgUgDQACgNAHgIQAGgIAMgFQANgEAPAAQAPAAALAEQAJADAFAGQAEAGACAJIABATIAAAbQABAeABAHQACAIADAHIgVAAQgEgGgBgJQgMAKgKAEQgLAEgMAAQgUAAgMgKgAgEAIQgNACgGACQgFACgDAFQgDAEAAAGQAAAIAGAGQAHAGANAAQAKAAAKgGQAKgFAEgJQADgIABgNIAAgIQgMAEgWAEg");
	this.shape_185.setTransform(401.1,89.1);

	this.shape_186 = new cjs.Shape();
	this.shape_186.graphics.f("#FFFFFF").s().p("AgJBWIAAirIAUAAIAACrg");
	this.shape_186.setTransform(391.7,86.7);

	this.shape_187 = new cjs.Shape();
	this.shape_187.graphics.f("#FFFFFF").s().p("AgCBRQgGgDgDgGQgDgGAAgTIAAhHIgPAAIAAgQIAPAAIAAgfIAUgNIAAAsIAVAAIAAAQIgVAAIAABIQAAAJABADQACACACACQACABAFAAIAJgBIAEATIgQACQgMAAgFgEg");
	this.shape_187.setTransform(385.9,87);

	this.shape_188 = new cjs.Shape();
	this.shape_188.graphics.f("#FFFFFF").s().p("AgtA3QgLgKAAgQQgBgJAFgHQAEgIAHgEQAGgEAJgCIASgDQAZgEAMgEIAAgFQAAgNgFgFQgJgIgPAAQgPAAgGAGQgIAFgDANIgVgDQADgNAGgIQAHgIAMgFQANgEAOAAQARAAAJAEQALADAEAGQAEAGADAJIABATIAAAbQAAAeABAHQABAIAEAHIgVAAQgEgGgBgJQgMAKgLAEQgKAEgMAAQgVAAgKgKgAgEAIQgNACgGACQgFACgDAFQgDAEAAAGQAAAIAHAGQAGAGANAAQAKAAAKgGQAKgFAEgJQAEgIgBgNIAAgIQgLAEgWAEg");
	this.shape_188.setTransform(369.1,89.1);

	this.shape_189 = new cjs.Shape();
	this.shape_189.graphics.f("#FFFFFF").s().p("AgKBWIAAirIAUAAIAACrg");
	this.shape_189.setTransform(359.7,86.7);

	this.shape_190 = new cjs.Shape();
	this.shape_190.graphics.f("#FFFFFF").s().p("AgDBRQgFgDgDgGQgDgGAAgTIAAhHIgQAAIAAgQIAQAAIAAgfIAUgNIAAAsIAWAAIAAAQIgWAAIAABIQAAAJACADQABACACACQADABAEAAIAKgBIACATIgQACQgLAAgGgEg");
	this.shape_190.setTransform(353.9,87);

	this.shape_191 = new cjs.Shape();
	this.shape_191.graphics.f("#FFFFFF").s().p("AgiA3QgNgKgEgUIAVgDQACAMAIAHQAIAHAOAAQAOAAAHgGQAHgGAAgIQAAgHgGgEQgEgDgSgEQgWgGgJgEQgJgEgEgHQgFgIAAgJQAAgIAEgIQAEgHAGgEQAFgEAIgCQAJgDAJAAQAPAAAKAEQALAEAGAIQAFAHACAMIgUADQgCgKgGgFQgIgGgLAAQgOAAgHAFQgFAFAAAGQAAAEACAEQADADAFACIASAGQAWAGAJAEQAJACAFAIQAEAHAAALQAAAKgFAKQgHAJgLAFQgMAFgOAAQgXAAgNgKg");
	this.shape_191.setTransform(337.7,89.1);

	this.shape_192 = new cjs.Shape();
	this.shape_192.graphics.f("#FFFFFF").s().p("AgtBVIgDgUQAIACAFAAQAGAAAFgDQAEgBADgFQACgDAEgNIACgFIgvh7IAXAAIAaBIIAIAcQADgOAFgOIAbhIIAVAAIgvB9QgIAVgEAIQgFAKgHAEQgHAGgLgBQgFAAgIgCg");
	this.shape_192.setTransform(325.9,91.6);

	this.shape_193 = new cjs.Shape();
	this.shape_193.graphics.f("#FFFFFF").s().p("AgbA7QgKgEgFgGQgFgHgCgJIgBgUIAAhMIAWAAIAABEIABAXQACAIAGAFQAHAEAJAAQAJAAAIgEQAIgFAEgJQADgIAAgQIAAhCIAVAAIAAB8IgTAAIAAgTQgOAWgYAAQgLAAgJgFg");
	this.shape_193.setTransform(313.1,89.2);

	this.shape_194 = new cjs.Shape();
	this.shape_194.graphics.f("#FFFFFF").s().p("AgRBXIAAhrIgTAAIAAgQIATAAIAAgNQABgNACgGQACgIAJgFQAGgFAOAAQAJAAAKACIgDASIgMgBQgJAAgFAEQgDAEAAALIAAAMIAXAAIAAAQIgXAAIAABrg");
	this.shape_194.setTransform(303.7,86.6);

	this.shape_195 = new cjs.Shape();
	this.shape_195.graphics.f("#FFFFFF").s().p("AgFAfIgGggIAAgcIAXAAIAAAcIgGAgg");
	this.shape_195.setTransform(290.8,81.2);

	this.shape_196 = new cjs.Shape();
	this.shape_196.graphics.f("#FFFFFF").s().p("AgMBGQAHgDADgFQACgGABgLIgLAAIAAgYIAXAAIAAAYQAAANgFAJQgFAIgJAEgAgKg2IAAgYIAXAAIAAAYg");
	this.shape_196.setTransform(285.2,90.8);

	this.shape_197 = new cjs.Shape();
	this.shape_197.graphics.f("#FFFFFF").s().p("AgoAwQgQgRAAgeQAAgeAQgSQAQgRAZAAQAZAAAPARQAQARAAAeIAAAFIhcAAQACAVAKALQAKALAPAAQAMAAAIgGQAIgGAFgOIAWADQgGATgOALQgNAKgWAAQgaAAgQgRgAgWglQgKAKgBAQIBEAAQgBgQgHgHQgKgNgQAAQgOAAgJAKg");
	this.shape_197.setTransform(275.2,89.1);

	this.shape_198 = new cjs.Shape();
	this.shape_198.graphics.f("#FFFFFF").s().p("AAZA+IgUhJIgFgVIgYBeIgWAAIgmh7IAVAAIAUBHIAHAaIAHgZIAUhIIAUAAIAUBHIAFAYIAHgYIAWhHIAVAAIgoB7g");
	this.shape_198.setTransform(259.8,89.1);

	this.shape_199 = new cjs.Shape();
	this.shape_199.graphics.f("#FFFFFF").s().p("AgaBPQgNgIgHgPQgHgOAAgUQAAgSAGgOQAHgQAMgIQANgIAPAAQAKAAAJAFQAIAEAGAIIAAg+IAVAAIAACrIgUAAIAAgPQgMASgWABQgOAAgMgJgAgWgNQgJALAAAYQAAAYAKAMQAKAMANAAQAOAAAJgMQAKgKAAgYQAAgZgKgMQgKgMgOAAQgNAAgKAMg");
	this.shape_199.setTransform(1087.3,55.2);

	this.shape_200 = new cjs.Shape();
	this.shape_200.graphics.f("#FFFFFF").s().p("AgXBtIAEgSIAKACQAGAAADgEQACgFAAgRIAAiCIAWAAIAACDQAAAXgGAJQgIALgQAAQgJAAgIgCgAAChWIAAgYIAWAAIAAAYg");
	this.shape_200.setTransform(1077.1,57.6);

	this.shape_201 = new cjs.Shape();
	this.shape_201.graphics.f("#FFFFFF").s().p("AguA3QgLgKABgQQAAgJAEgHQAEgIAHgEQAHgEAIgCIASgDQAZgEAMgEIAAgFQAAgNgFgFQgJgIgPAAQgPAAgGAGQgIAFgDANIgVgDQADgNAGgIQAHgIAMgFQAMgEAQAAQAQAAAJAEQAKADAFAGQAFAGACAJIABATIAAAbQgBAeACAHQABAIAFAHIgXAAQgDgGgBgJQgMAKgLAEQgKAEgMAAQgUAAgMgKgAgEAIQgNACgFACQgGACgDAFQgDAEAAAGQAAAIAGAGQAHAGAMAAQALAAAKgGQAJgFAFgJQADgIAAgNIAAgIQgLAEgWAEg");
	this.shape_201.setTransform(1069,57.4);

	this.shape_202 = new cjs.Shape();
	this.shape_202.graphics.f("#FFFFFF").s().p("AgKBWIAAirIAUAAIAACrg");
	this.shape_202.setTransform(1059.7,55.1);

	this.shape_203 = new cjs.Shape();
	this.shape_203.graphics.f("#FFFFFF").s().p("AgpAwQgQgQAAggQAAgiATgQQAQgOAWAAQAaAAAQARQAQARAAAdQAAAXgHAOQgHAOgOAHQgOAIgQAAQgZAAgQgRgAgZgjQgLAMAAAXQAAAYALAMQAKAMAPAAQAQAAAKgMQALgMAAgYQAAgXgLgMQgKgMgQAAQgPAAgKAMg");
	this.shape_203.setTransform(1050.3,57.4);

	this.shape_204 = new cjs.Shape();
	this.shape_204.graphics.f("#FFFFFF").s().p("AgiA3QgNgKgDgUIAUgDQACAMAIAHQAIAHAOAAQAOAAAHgGQAHgGAAgIQAAgHgGgEQgFgDgQgEQgXgGgJgEQgJgEgEgHQgFgIAAgJQAAgIAEgIQADgHAHgEQAFgEAIgCQAJgDAJAAQAOAAALAEQAMAEAFAIQAFAHACAMIgVADQgBgKgGgFQgIgGgLAAQgPAAgGAFQgFAFgBAGQAAAEADAEQACADAGACIASAGQAWAGAJAEQAIACAGAIQAEAHAAALQAAAKgFAKQgHAJgLAFQgMAFgOAAQgXAAgNgKg");
	this.shape_204.setTransform(1031,57.4);

	this.shape_205 = new cjs.Shape();
	this.shape_205.graphics.f("#FFFFFF").s().p("AgtBVIgDgUQAHACAFAAQAIAAAEgCQAEgCADgFQACgDAEgNIACgFIgvh7IAXAAIAZBIIAIAbQAEgNAFgOIAbhIIAVAAIgwB9QgHAVgEAHQgFALgHAFQgIAEgJAAQgHAAgHgCg");
	this.shape_205.setTransform(1019.2,60);

	this.shape_206 = new cjs.Shape();
	this.shape_206.graphics.f("#FFFFFF").s().p("AgcA7QgJgEgFgGQgFgHgCgJIAAgUIAAhMIAUAAIAABEIABAXQADAIAGAFQAGAEAKAAQAIAAAJgEQAIgFAEgJQADgIAAgQIAAhCIAVAAIAAB8IgTAAIAAgTQgOAWgYAAQgLAAgKgFg");
	this.shape_206.setTransform(1006.3,57.6);

	this.shape_207 = new cjs.Shape();
	this.shape_207.graphics.f("#FFFFFF").s().p("AgRBXIAAhrIgTAAIAAgQIATAAIAAgNQAAgNACgGQAEgIAIgFQAGgFAOAAQAIAAALACIgCASIgNgBQgKAAgEAEQgDAEAAALIAAAMIAXAAIAAAQIgXAAIAABrg");
	this.shape_207.setTransform(996.9,54.9);

	this.shape_208 = new cjs.Shape();
	this.shape_208.graphics.f("#FFFFFF").s().p("AguA3QgLgKAAgQQABgJAEgHQAEgIAHgEQAGgEAJgCIATgDQAYgEANgEIAAgFQAAgNgHgFQgHgIgQAAQgPAAgHAGQgGAFgEANIgUgDQACgNAHgIQAGgIAMgFQANgEAPAAQAPAAALAEQAJADAFAGQAEAGACAJIABATIAAAbQABAeABAHQACAIAEAHIgXAAQgDgGgBgJQgLAKgMAEQgJAEgNAAQgUAAgMgKgAgEAIQgNACgFACQgGACgDAFQgDAEAAAGQAAAIAGAGQAHAGAMAAQAMAAAJgGQAKgFAEgJQADgIABgNIAAgIQgMAEgWAEg");
	this.shape_208.setTransform(979.7,57.4);

	this.shape_209 = new cjs.Shape();
	this.shape_209.graphics.f("#FFFFFF").s().p("AgKBWIAAirIAVAAIAACrg");
	this.shape_209.setTransform(970.3,55.1);

	this.shape_210 = new cjs.Shape();
	this.shape_210.graphics.f("#FFFFFF").s().p("AAZBWIgng/IgQANIAAAyIgUAAIAAirIAUAAIAABhIAygxIAbAAIgvAsIAzBPg");
	this.shape_210.setTransform(962.5,55.1);

	this.shape_211 = new cjs.Shape();
	this.shape_211.graphics.f("#FFFFFF").s().p("AgtBVIgDgUQAHACAGAAQAGAAAFgCQAEgCADgFQACgDAEgNIACgFIgvh7IAWAAIAbBIIAIAbQADgNAFgOIAbhIIAVAAIgvB9QgIAVgDAHQgGALgHAFQgIAEgKAAQgFAAgIgCg");
	this.shape_211.setTransform(949.8,60);

	this.shape_212 = new cjs.Shape();
	this.shape_212.graphics.f("#FFFFFF").s().p("AglBPQgNgKABgUIAUADQABAKAGADQAIAHAOAAQANAAAIgHQAIgFADgLQABgGABgVQgPAQgTAAQgZAAgOgSQgPgTAAgZQAAgRAHgPQAGgPANgIQALgJARAAQAVAAAOASIAAgPIAUAAIAABqQAAAegGAMQgGAMgMAHQgOAHgRAAQgXAAgOgKgAgWg7QgKAMAAAWQAAAYAKALQAJALAOAAQAOAAAKgLQAKgLAAgXQAAgXgKgMQgKgMgPAAQgMAAgKAMg");
	this.shape_212.setTransform(936.6,59.8);

	this.shape_213 = new cjs.Shape();
	this.shape_213.graphics.f("#FFFFFF").s().p("AgJBWIAAh7IAUAAIAAB7gAgJg9IAAgYIAUAAIAAAYg");
	this.shape_213.setTransform(927.7,55.1);

	this.shape_214 = new cjs.Shape();
	this.shape_214.graphics.f("#FFFFFF").s().p("AApBWIg8hWIgcAaIAAA8IgXAAIAAirIAXAAIAABVIBUhVIAfAAIhHBGIBKBlg");
	this.shape_214.setTransform(911.2,55.1);

	this.shape_215 = new cjs.Shape();
	this.shape_215.graphics.f("#FFFFFF").s().p("AghBAIAAh8IATAAIAAATQAIgNAGgEQAFgFAHAAQALAAALAHIgHAUQgIgFgIAAQgHAAgEAFQgGAEgCAHQgEALAAAOIAABAg");
	this.shape_215.setTransform(899.3,57.3);

	this.shape_216 = new cjs.Shape();
	this.shape_216.graphics.f("#FFFFFF").s().p("AgiA3QgNgKgEgUIAVgDQACAMAIAHQAIAHANAAQAPAAAHgGQAHgGAAgIQAAgHgGgEQgFgDgRgEQgWgGgJgEQgJgEgFgHQgEgIAAgJQAAgIAEgIQADgHAHgEQAFgEAJgCQAIgDAKAAQAOAAALAEQAKAEAGAIQAFAHACAMIgUADQgCgKgHgFQgHgGgLAAQgPAAgFAFQgHAFABAGQgBAEADAEQACADAGACIARAGQAXAGAJAEQAJACAEAIQAFAHABALQgBAKgGAKQgGAJgLAFQgMAFgPAAQgXAAgMgKg");
	this.shape_216.setTransform(888.3,57.4);

	this.shape_217 = new cjs.Shape();
	this.shape_217.graphics.f("#FFFFFF").s().p("AgJBWIAAirIATAAIAACrg");
	this.shape_217.setTransform(879.6,55.1);

	this.shape_218 = new cjs.Shape();
	this.shape_218.graphics.f("#FFFFFF").s().p("AAaBWIgpg/IgPANIAAAyIgUAAIAAirIAUAAIAABhIAygxIAbAAIgvAsIAzBPg");
	this.shape_218.setTransform(865.1,55.1);

	this.shape_219 = new cjs.Shape();
	this.shape_219.graphics.f("#FFFFFF").s().p("AgXBtIAEgSIAKACQAGAAADgEQACgFAAgRIAAiCIAWAAIAACDQAAAXgGAJQgIALgQAAQgJAAgIgCgAAChWIAAgYIAWAAIAAAYg");
	this.shape_219.setTransform(854.3,57.6);

	this.shape_220 = new cjs.Shape();
	this.shape_220.graphics.f("#FFFFFF").s().p("AguA3QgLgKABgQQAAgJAEgHQAEgIAHgEQAHgEAIgCIASgDQAZgEANgEIAAgFQgBgNgFgFQgJgIgPAAQgPAAgGAGQgIAFgDANIgVgDQADgNAGgIQAHgIAMgFQAMgEAQAAQAQAAAJAEQAKADAFAGQAFAGACAJIABATIAAAbQgBAeACAHQABAIAFAHIgXAAQgDgGgBgJQgMAKgLAEQgKAEgMAAQgUAAgMgKgAgEAIQgNACgFACQgGACgDAFQgDAEAAAGQAAAIAGAGQAHAGAMAAQALAAAKgGQAJgFAFgJQAEgIAAgNIAAgIQgMAEgWAEg");
	this.shape_220.setTransform(846.3,57.4);

	this.shape_221 = new cjs.Shape();
	this.shape_221.graphics.f("#FFFFFF").s().p("AAZBWIgng/IgQANIAAAyIgUAAIAAirIAUAAIAABhIAygxIAbAAIgvAsIAzBPg");
	this.shape_221.setTransform(834.4,55.1);

	this.shape_222 = new cjs.Shape();
	this.shape_222.graphics.f("#FFFFFF").s().p("AA/BAIAAhOQAAgMgCgGQgCgFgFgEQgGgDgHAAQgNAAgJAJQgIAIAAATIAABIIgUAAIAAhQQAAgOgGgHQgFgHgMAAQgIAAgIAEQgIAFgDAJQgDAJAAARIAABAIgVAAIAAh8IASAAIAAASQAGgJAKgGQAKgGAMAAQAOAAAJAGQAIAGADAKQAPgWAYAAQASAAAKALQAKAKAAAVIAABVg");
	this.shape_222.setTransform(817.7,57.3);

	this.shape_223 = new cjs.Shape();
	this.shape_223.graphics.f("#FFFFFF").s().p("AgRBXIAAhrIgTAAIAAgQIATAAIAAgNQAAgNADgGQACgIAJgFQAGgFAOAAQAIAAALACIgCASIgNgBQgKAAgEAEQgDAEAAALIAAAMIAXAAIAAAQIgXAAIAABrg");
	this.shape_223.setTransform(804.9,54.9);

	this.shape_224 = new cjs.Shape();
	this.shape_224.graphics.f("#FFFFFF").s().p("AguA3QgKgKgBgQQAAgJAFgHQAEgIAHgEQAGgEAJgCIATgDQAYgEANgEIAAgFQAAgNgHgFQgHgIgQAAQgPAAgHAGQgGAFgEANIgUgDQACgNAHgIQAGgIAMgFQANgEAPAAQAPAAALAEQAKADAEAGQAEAGACAJIABATIAAAbQABAeABAHQACAIADAHIgVAAQgEgGgBgJQgMAKgKAEQgLAEgMAAQgUAAgMgKgAgEAIQgNACgGACQgFACgDAFQgDAEAAAGQAAAIAGAGQAHAGANAAQAKAAAKgGQAKgFAEgJQADgIABgNIAAgIQgMAEgWAEg");
	this.shape_224.setTransform(787.6,57.4);

	this.shape_225 = new cjs.Shape();
	this.shape_225.graphics.f("#FFFFFF").s().p("AgJBWIAAirIAUAAIAACrg");
	this.shape_225.setTransform(778.3,55.1);

	this.shape_226 = new cjs.Shape();
	this.shape_226.graphics.f("#FFFFFF").s().p("AgXBtIAEgSIAKACQAHAAACgEQACgFAAgRIAAiCIAWAAIAACDQAAAXgGAJQgIALgRAAQgIAAgIgCgAAChWIAAgYIAWAAIAAAYg");
	this.shape_226.setTransform(771.6,57.6);

	this.shape_227 = new cjs.Shape();
	this.shape_227.graphics.f("#FFFFFF").s().p("AgaBPQgNgIgHgPQgHgOAAgUQAAgSAHgOQAFgQANgIQAMgIAQAAQAKAAAJAFQAJAEAFAIIAAg+IAVAAIAACrIgTAAIAAgPQgMASgXABQgOAAgMgJgAgWgNQgJALAAAYQAAAYAKAMQAKAMANAAQAOAAAKgMQAJgKAAgYQAAgZgKgMQgKgMgOAAQgNAAgKAMg");
	this.shape_227.setTransform(763.2,55.2);

	this.shape_228 = new cjs.Shape();
	this.shape_228.graphics.f("#FFFFFF").s().p("AgLA+IAAgYIAXAAIAAAYgAgLglIAAgYIAXAAIAAAYg");
	this.shape_228.setTransform(753.7,57.4);

	this.shape_229 = new cjs.Shape();
	this.shape_229.graphics.f("#FFFFFF").s().p("AguA3QgLgKABgQQgBgJAFgHQAEgIAHgEQAGgEAJgCIASgDQAZgEAMgEIAAgFQAAgNgFgFQgJgIgPAAQgPAAgGAGQgIAFgDANIgVgDQADgNAGgIQAHgIAMgFQANgEAOAAQARAAAJAEQALADAEAGQAEAGADAJIABATIAAAbQgBAeACAHQACAIAEAHIgXAAQgDgGgBgJQgMAKgLAEQgKAEgMAAQgVAAgLgKgAgEAIQgNACgFACQgGACgDAFQgDAEAAAGQAAAIAHAGQAGAGAMAAQALAAAKgGQAJgFAFgJQADgIAAgNIAAgIQgLAEgWAEg");
	this.shape_229.setTransform(743.6,57.4);

	this.shape_230 = new cjs.Shape();
	this.shape_230.graphics.f("#FFFFFF").s().p("AgJBWIAAh7IATAAIAAB7gAgJg9IAAgYIATAAIAAAYg");
	this.shape_230.setTransform(734.3,55.1);

	this.shape_231 = new cjs.Shape();
	this.shape_231.graphics.f("#FFFFFF").s().p("AgXBtIAEgSIAKACQAGAAADgEQADgFAAgRIAAiCIAVAAIAACDQAAAXgGAJQgIALgQAAQgJAAgIgCgAADhWIAAgYIAVAAIAAAYg");
	this.shape_231.setTransform(727.6,57.6);

	this.shape_232 = new cjs.Shape();
	this.shape_232.graphics.f("#FFFFFF").s().p("AAYA+IgThJIgFgVIgYBeIgWAAIgnh7IAXAAIAUBHIAHAaIAGgZIAUhIIAUAAIATBHIAHAYIAHgYIAVhHIAUAAIgmB7g");
	this.shape_232.setTransform(717.5,57.4);

	this.shape_233 = new cjs.Shape();
	this.shape_233.graphics.f("#FFFFFF").s().p("AAaBWIgpg/IgOANIAAAyIgWAAIAAirIAWAAIAABhIAwgxIAcAAIgvAsIA0BPg");
	this.shape_233.setTransform(697,55.1);

	this.shape_234 = new cjs.Shape();
	this.shape_234.graphics.f("#FFFFFF").s().p("AghBAIAAh8IATAAIAAATQAIgNAGgEQAFgFAHAAQALAAALAHIgHAUQgIgFgIAAQgHAAgEAFQgGAEgCAHQgEALAAAOIAABAg");
	this.shape_234.setTransform(687.2,57.3);

	this.shape_235 = new cjs.Shape();
	this.shape_235.graphics.f("#FFFFFF").s().p("AgKBWIAAirIAVAAIAACrg");
	this.shape_235.setTransform(679.5,55.1);

	this.shape_236 = new cjs.Shape();
	this.shape_236.graphics.f("#FFFFFF").s().p("AgtA3QgMgKAAgQQAAgJAFgHQAEgIAHgEQAHgEAIgCIATgDQAYgEANgEIAAgFQAAgNgHgFQgHgIgQAAQgOAAgIAGQgGAFgEANIgUgDQACgNAHgIQAGgIAMgFQANgEAPAAQAPAAALAEQAKADAEAGQAEAGACAJIABATIAAAbQAAAeACAHQACAIADAHIgVAAQgEgGgBgJQgMAKgKAEQgLAEgMAAQgVAAgKgKgAgEAIQgNACgGACQgFACgDAFQgDAEAAAGQAAAIAHAGQAGAGANAAQALAAAJgGQAJgFAFgJQADgIABgNIAAgIQgMAEgWAEg");
	this.shape_236.setTransform(663.5,57.4);

	this.shape_237 = new cjs.Shape();
	this.shape_237.graphics.f("#FFFFFF").s().p("AAaBWIgpg/IgOANIAAAyIgWAAIAAirIAWAAIAABhIAwgxIAcAAIgvAsIA0BPg");
	this.shape_237.setTransform(651.7,55.1);

	this.shape_238 = new cjs.Shape();
	this.shape_238.graphics.f("#FFFFFF").s().p("AgbA7QgKgEgFgGQgFgHgBgJIgCgUIAAhMIAWAAIAABEIABAXQACAIAGAFQAGAEAKAAQAIAAAJgEQAIgFAEgJQADgIAAgQIAAhCIAWAAIAAB8IgTAAIAAgTQgPAWgYAAQgLAAgJgFg");
	this.shape_238.setTransform(638.2,57.6);

	this.shape_239 = new cjs.Shape();
	this.shape_239.graphics.f("#FFFFFF").s().p("AgLAMIAAgXIAXAAIAAAXg");
	this.shape_239.setTransform(628.3,62.5);

	this.shape_240 = new cjs.Shape();
	this.shape_240.graphics.f("#FFFFFF").s().p("AgbBTQgKgEgFgGQgEgHgDgJIgBgUIAAhMIAWAAIAABEIABAXQACAIAGAFQAHAEAJAAQAIAAAJgEQAIgFAEgJQADgIAAgQIAAhCIAWAAIAAB8IgUAAIAAgTQgOAWgYAAQgLAAgJgFgAACg2IgaghIAbAAIAQAhg");
	this.shape_240.setTransform(611.5,55.2);

	this.shape_241 = new cjs.Shape();
	this.shape_241.graphics.f("#FFFFFF").s().p("AAeBWIAAhPQAAgPgHgHQgHgHgMAAQgJAAgIAFQgIAEgEAJQgDAIgBAOIAABEIgUAAIAAirIAUAAIAAA+QAPgRAVAAQAOgBALAGQAKAGAEAJQAFAKAAARIAABPg");
	this.shape_241.setTransform(591.6,55.1);

	this.shape_242 = new cjs.Shape();
	this.shape_242.graphics.f("#FFFFFF").s().p("AAaBWIgpg/IgOANIAAAyIgWAAIAAirIAWAAIAABhIAwgxIAcAAIgvAsIA0BPg");
	this.shape_242.setTransform(579.7,55.1);

	this.shape_243 = new cjs.Shape();
	this.shape_243.graphics.f("#FFFFFF").s().p("AgMBGQAHgDADgFQACgGABgLIgLAAIAAgYIAXAAIAAAYQAAANgFAJQgFAIgJAEgAgKg2IAAgYIAXAAIAAAYg");
	this.shape_243.setTransform(569.5,59.1);

	this.shape_244 = new cjs.Shape();
	this.shape_244.graphics.f("#FFFFFF").s().p("AhGBaICAi0IATAAIiAC0gABDBXIAAgSIgtAAIAAgOIAwg3IAOAAIAAA5IAMAAIAAAMIgMAAIAAASgAAoA5IAbAAIAAgfgAhUgFQgJgHgCgMIARgCQACAIAEACQAFADAHABQAIgBAFgEQAFgEAAgHQAAgGgEgEQgEgDgKAAIgHABIADgOQAJABAFgEQAEgEAAgFQAAgEgEgDQgDgDgHAAQgHAAgDACQgEADgDAHIgQgCQAEgNAIgFQAHgFAOAAQARAAAIAGQAHAHAAAJQAAAHgEAEQgEAFgIADQALADAFAGQAEAFAAAIQAAALgJAIQgJAHgRAAQgRAAgIgFg");
	this.shape_244.setTransform(556.3,55.3);

	this.shape_245 = new cjs.Shape();
	this.shape_245.graphics.f("#FFFFFF").s().p("AgXBtIAEgSIAKACQAHAAACgEQACgFAAgRIAAiCIAWAAIAACDQAAAXgGAJQgIALgRAAQgIAAgIgCgAAChWIAAgYIAWAAIAAAYg");
	this.shape_245.setTransform(542.2,57.6);

	this.shape_246 = new cjs.Shape();
	this.shape_246.graphics.f("#FFFFFF").s().p("AgbBPQgMgIgHgPQgHgOAAgUQAAgSAHgOQAFgQANgIQAMgIAQAAQAKAAAJAFQAIAEAGAIIAAg+IAVAAIAACrIgUAAIAAgPQgMASgWABQgOAAgNgJgAgVgNQgKALAAAYQAAAYAKAMQAKAMANAAQANAAAKgMQAKgKAAgYQAAgZgKgMQgKgMgOAAQgNAAgJAMg");
	this.shape_246.setTransform(527.1,55.2);

	this.shape_247 = new cjs.Shape();
	this.shape_247.graphics.f("#FFFFFF").s().p("AgtBVIgDgUQAHACAGAAQAGAAAFgCQAEgCADgFQACgDAEgNIACgFIgvh7IAXAAIAaBIIAIAbQADgNAFgOIAbhIIAVAAIgvB9QgIAVgEAHQgFALgHAFQgHAEgLAAQgFAAgIgCg");
	this.shape_247.setTransform(515,60);

	this.shape_248 = new cjs.Shape();
	this.shape_248.graphics.f("#FFFFFF").s().p("AghBAIAAh8IATAAIAAATQAIgNAGgEQAFgFAHAAQALAAALAHIgHAUQgIgFgIAAQgHAAgEAFQgGAEgCAHQgEALAAAOIAABAg");
	this.shape_248.setTransform(505.8,57.3);

	this.shape_249 = new cjs.Shape();
	this.shape_249.graphics.f("#FFFFFF").s().p("AgtBVIgDgUQAIACAEAAQAIAAAEgCQAEgCADgFQACgDAEgNIACgFIgvh7IAXAAIAaBIIAIAbQADgNAFgOIAbhIIAVAAIgvB9QgIAVgEAHQgFALgHAFQgHAEgKAAQgHAAgHgCg");
	this.shape_249.setTransform(495,60);

	this.shape_250 = new cjs.Shape();
	this.shape_250.graphics.f("#FFFFFF").s().p("AgtA3QgMgKAAgQQAAgJAFgHQAEgIAHgEQAHgEAIgCIATgDQAYgEAMgEIAAgFQABgNgHgFQgHgIgQAAQgOAAgIAGQgGAFgEANIgUgDQACgNAHgIQAGgIAMgFQAMgEAPAAQAQAAALAEQAKADAEAGQAEAGACAJIABATIAAAbQAAAeACAHQABAIAEAHIgVAAQgEgGgBgJQgLAKgLAEQgKAEgNAAQgVAAgKgKgAgEAIQgNACgGACQgFACgDAFQgDAEAAAGQAAAIAHAGQAGAGANAAQALAAAJgGQAJgFAFgJQADgIAAgNIAAgIQgLAEgWAEg");
	this.shape_250.setTransform(475.5,57.4);

	this.shape_251 = new cjs.Shape();
	this.shape_251.graphics.f("#FFFFFF").s().p("AgJBWIAAirIATAAIAACrg");
	this.shape_251.setTransform(466.2,55.1);

	this.shape_252 = new cjs.Shape();
	this.shape_252.graphics.f("#FFFFFF").s().p("AgXBtIAEgSIAKACQAHAAACgEQACgFAAgRIAAiCIAWAAIAACDQAAAXgGAJQgIALgRAAQgIAAgIgCgAAChWIAAgYIAWAAIAAAYg");
	this.shape_252.setTransform(459.5,57.6);

	this.shape_253 = new cjs.Shape();
	this.shape_253.graphics.f("#FFFFFF").s().p("AgoAwQgQgRAAgeQAAgeAQgSQAQgRAZAAQAZAAAPARQAQARAAAeIAAAFIhcAAQACAVAKALQAKALAPAAQAMAAAIgGQAIgGAFgOIAWADQgGATgOALQgNAKgWAAQgaAAgQgRgAgWglQgKAKgBAQIBEAAQgBgQgHgHQgKgNgQAAQgOAAgJAKg");
	this.shape_253.setTransform(451.5,57.4);

	this.shape_254 = new cjs.Shape();
	this.shape_254.graphics.f("#FFFFFF").s().p("AghBAIAAh8IATAAIAAATQAIgNAGgEQAFgFAHAAQALAAALAHIgHAUQgIgFgIAAQgHAAgEAFQgGAEgCAHQgEALAAAOIAABAg");
	this.shape_254.setTransform(441.8,57.3);

	this.shape_255 = new cjs.Shape();
	this.shape_255.graphics.f("#FFFFFF").s().p("AgtBtIgDgUQAHACAGAAQAGAAAFgCQAEgCADgFQACgDAEgMIACgGIgvh7IAXAAIAaBHIAIAdQADgPAFgOIAbhHIAVAAIgvB9QgIAVgDAIQgGAKgHAFQgIAFgKAAQgFAAgIgDgAALhXIAAgYIAWAAIAAAYgAgghXIAAgYIAXAAIAAAYg");
	this.shape_255.setTransform(431,57.5);

	this.shape_256 = new cjs.Shape();
	this.shape_256.graphics.f("#FFFFFF").s().p("AgbBTQgKgEgFgGQgFgHgBgJIgCgUIAAhMIAWAAIAABEIABAXQACAIAGAFQAGAEAKAAQAIAAAJgEQAIgFAEgJQADgIAAgQIAAhCIAWAAIAAB8IgTAAIAAgTQgPAWgYAAQgLAAgJgFgAACg2IgaghIAbAAIAQAhg");
	this.shape_256.setTransform(418.1,55.2);

	this.shape_257 = new cjs.Shape();
	this.shape_257.graphics.f("#FFFFFF").s().p("AgtA3QgMgKAAgQQAAgJAFgHQAEgIAHgEQAHgEAIgCIATgDQAYgEAMgEIAAgFQABgNgHgFQgHgIgQAAQgOAAgIAGQgGAFgEANIgUgDQACgNAHgIQAGgIAMgFQAMgEAPAAQAQAAAKAEQALADAEAGQAEAGACAJIABATIAAAbQAAAeACAHQABAIAEAHIgVAAQgEgGgBgJQgLAKgLAEQgKAEgNAAQgVAAgKgKgAgEAIQgNACgGACQgFACgDAFQgDAEAAAGQAAAIAHAGQAGAGANAAQALAAAJgGQAJgFAFgJQADgIAAgNIAAgIQgLAEgWAEg");
	this.shape_257.setTransform(398.1,57.4);

	this.shape_258 = new cjs.Shape();
	this.shape_258.graphics.f("#FFFFFF").s().p("AgJBWIAAirIATAAIAACrg");
	this.shape_258.setTransform(388.8,55.1);

	this.shape_259 = new cjs.Shape();
	this.shape_259.graphics.f("#FFFFFF").s().p("AAeBWIAAhPQgBgPgGgHQgHgHgNAAQgIAAgIAFQgJAEgDAJQgEAIAAAOIAABEIgVAAIAAirIAVAAIAAA+QAPgRAVAAQAOgBAKAGQALAGAFAJQADAKAAARIAABPg");
	this.shape_259.setTransform(379.5,55.1);

	this.shape_260 = new cjs.Shape();
	this.shape_260.graphics.f("#FFFFFF").s().p("AApBWIg8hWIgcAaIAAA8IgXAAIAAirIAXAAIAABVIBUhVIAfAAIhHBGIBKBlg");
	this.shape_260.setTransform(365.7,55.1);

	this.shape_261 = new cjs.Shape();
	this.shape_261.graphics.f("#FFFFFF").s().p("AghBAIAAh8IATAAIAAATQAIgNAGgEQAFgFAHAAQALAAALAHIgHAUQgIgFgIAAQgHAAgEAFQgGAEgCAHQgEALAAAOIAABAg");
	this.shape_261.setTransform(353.7,57.3);

	this.shape_262 = new cjs.Shape();
	this.shape_262.graphics.f("#FFFFFF").s().p("AgjA3QgMgKgDgUIAUgDQACAMAIAHQAIAHANAAQAPAAAHgGQAHgGAAgIQAAgHgGgEQgEgDgRgEQgXgGgJgEQgJgEgFgHQgEgIAAgJQAAgIAEgIQAEgHAGgEQAFgEAIgCQAJgDAJAAQAOAAAMAEQALAEAFAIQAFAHACAMIgVADQgBgKgHgFQgGgGgMAAQgPAAgFAFQgHAFAAAGQABAEACAEQACADAGACIARAGQAXAGAJAEQAJACAEAIQAGAHAAALQAAAKgHAKQgFAJgMAFQgMAFgPAAQgWAAgOgKg");
	this.shape_262.setTransform(342.7,57.4);

	this.shape_263 = new cjs.Shape();
	this.shape_263.graphics.f("#FFFFFF").s().p("AgKBWIAAirIAUAAIAACrg");
	this.shape_263.setTransform(334.1,55.1);

	this.shape_264 = new cjs.Shape();
	this.shape_264.graphics.f("#FFFFFF").s().p("AgJBWIAAirIAUAAIAACrg");
	this.shape_264.setTransform(322.1,55.1);

	this.shape_265 = new cjs.Shape();
	this.shape_265.graphics.f("#FFFFFF").s().p("AgtA3QgLgKAAgQQgBgJAFgHQAEgIAHgEQAHgEAIgCIATgDQAYgEAMgEIAAgFQAAgNgFgFQgJgIgPAAQgOAAgIAGQgGAFgEANIgUgDQACgNAHgIQAGgIAMgFQAMgEAPAAQAQAAAKAEQALADAEAGQAFAGABAJIACATIAAAbQAAAeABAHQABAIAEAHIgVAAQgEgGgBgJQgLAKgLAEQgKAEgNAAQgVAAgKgKgAgEAIQgNACgGACQgFACgDAFQgDAEAAAGQAAAIAHAGQAGAGANAAQALAAAJgGQAJgFAFgJQAEgIgBgNIAAgIQgLAEgWAEg");
	this.shape_265.setTransform(312.7,57.4);

	this.shape_266 = new cjs.Shape();
	this.shape_266.graphics.f("#FFFFFF").s().p("AgbBPQgMgIgHgPQgHgOAAgUQAAgSAHgOQAGgQAMgIQAMgIAQAAQAKAAAJAFQAIAEAGAIIAAg+IAVAAIAACrIgUAAIAAgPQgMASgWABQgOAAgNgJgAgVgNQgKALAAAYQAAAYAKAMQAKAMANAAQANAAAKgMQAKgKAAgYQAAgZgKgMQgKgMgOAAQgNAAgJAMg");
	this.shape_266.setTransform(299,55.2);

	this.shape_267 = new cjs.Shape();
	this.shape_267.graphics.f("#FFFFFF").s().p("AgMAUQAHgDADgFQACgHABgJIgLAAIAAgYIAXAAIAAAYQAAAMgFAIQgFAJgJAEg");
	this.shape_267.setTransform(289.4,64.2);

	this.shape_268 = new cjs.Shape();
	this.shape_268.graphics.f("#FFFFFF").s().p("AgRBXIAAhrIgTAAIAAgQIATAAIAAgNQAAgNADgGQACgIAJgFQAGgFAOAAQAJAAAKACIgDASIgMgBQgKAAgEAEQgDAEAAALIAAAMIAXAAIAAAQIgXAAIAABrg");
	this.shape_268.setTransform(283.3,54.9);

	this.shape_269 = new cjs.Shape();
	this.shape_269.graphics.f("#FFFFFF").s().p("AgbBPQgMgIgHgPQgHgOAAgUQAAgSAHgOQAFgQANgIQAMgIAQAAQAKAAAJAFQAIAEAGAIIAAg+IAVAAIAACrIgTAAIAAgPQgNASgWABQgOAAgNgJgAgVgNQgKALAAAYQAAAYAKAMQAKAMANAAQANAAALgMQAJgKAAgYQAAgZgKgMQgKgMgOAAQgNAAgJAMg");
	this.shape_269.setTransform(272.3,55.2);

	this.shape_270 = new cjs.Shape();
	this.shape_270.graphics.f("#FFFFFF").s().p("AAZA+IgUhJIgFgVIgYBeIgWAAIgnh7IAXAAIATBHIAIAaIAGgZIAUhIIAUAAIATBHIAGAYIAIgYIAVhHIAUAAIgnB7g");
	this.shape_270.setTransform(257.4,57.4);

	this.shape_271 = new cjs.Shape();
	this.shape_271.graphics.f("#0097A7").s().p("EhNbAJgQhbAAhBhAQhAhBAAhbIAAvjMChvAAAIAAPjQAABbhABBQhBBAhbAAg");
	this.shape_271.setTransform(669.4,60.8);

	this.instance_9 = new lib.Path_1();
	this.instance_9.parent = this;
	this.instance_9.setTransform(676.4,64.3,1,1,0,0,0,517.6,63.3);
	this.instance_9.alpha = 0.602;

	this.shape_272 = new cjs.Shape();
	this.shape_272.graphics.f("#FFFFFF").s().p("EgoQA44QgtAAggggQgfggAAgsMAAAhuXQAAgsAfghQAggfAtAAMBEJAAAMAOEBPxMgGQAh+g");
	this.shape_272.setTransform(288.5,384);

	this.instance_10 = new lib.Group_1();
	this.instance_10.parent = this;
	this.instance_10.setTransform(298.5,384,1,1,0,0,0,278.5,364);
	this.instance_10.alpha = 0.602;

	this.shape_273 = new cjs.Shape();
	this.shape_273.graphics.f("#00ACC1").s().p("AhhBiQgpgoAAg6QAAg5ApgpQApgoA4AAQA6AAAoAoQApApAAA5QAAA5gpApQgoAqg6AAQg4AAgpgqg");
	this.shape_273.setTransform(1188,561.7);

	this.shape_274 = new cjs.Shape();
	this.shape_274.graphics.f("#80DEEA").s().p("Ah3B4QgxgyAAhGQAAhFAxgyQAygxBFgBQBGABAyAxQAxAyAABFQAABGgxAyQgyAyhGAAQhFAAgygyg");
	this.shape_274.setTransform(1188,561.7);

	this.shape_275 = new cjs.Shape();
	this.shape_275.graphics.f("#00ACC1").s().p("AhhBiQgpgoAAg6QAAg4ApgpQApgpA4AAQA6AAAoApQApApAAA4QAAA6gpAoQgoApg6AAQg4AAgpgpg");
	this.shape_275.setTransform(1188,498.9);

	this.shape_276 = new cjs.Shape();
	this.shape_276.graphics.f("#80DEEA").s().p("Ah3B4QgxgyAAhGQAAhFAxgyQAygxBFAAQBGAAAyAxQAxAyAABFQAABGgxAyQgyAxhGAAQhFAAgygxg");
	this.shape_276.setTransform(1188,498.9);

	this.shape_277 = new cjs.Shape();
	this.shape_277.graphics.f("#00ACC1").s().p("AhhBiQgpgoAAg6QAAg5ApgpQApgoA4AAQA6AAAoAoQApApAAA5QAAA5gpApQgoAqg6AAQg4AAgpgqg");
	this.shape_277.setTransform(1188,437.1);

	this.shape_278 = new cjs.Shape();
	this.shape_278.graphics.f("#80DEEA").s().p("Ah3B4QgxgyAAhGQAAhFAxgyQAygxBFgBQBGABAyAxQAxAyAABFQAABGgxAyQgyAyhGgBQhFABgygyg");
	this.shape_278.setTransform(1188,437.1);

	this.shape_279 = new cjs.Shape();
	this.shape_279.graphics.f("#00ACC1").s().p("AhhBiQgpgoAAg6QAAg5ApgpQApgpA4ABQA6gBAoApQApApAAA5QAAA5gpApQgoApg6ABQg4gBgpgpg");
	this.shape_279.setTransform(1188,374.3);

	this.shape_280 = new cjs.Shape();
	this.shape_280.graphics.f("#80DEEA").s().p("Ah3B4QgxgyAAhGQAAhFAxgyQAygxBFAAQBGAAAyAxQAxAyAABFQAABGgxAyQgyAyhGgBQhFABgygyg");
	this.shape_280.setTransform(1188,374.3);

	this.shape_281 = new cjs.Shape();
	this.shape_281.graphics.f("#00ACC1").s().p("AhhBjQgpgpAAg6QAAg4ApgpQApgpA4gBQA6ABAoApQApApAAA4QAAA5gpAqQgoApg6gBQg4ABgpgpg");
	this.shape_281.setTransform(1188,312.5);

	this.shape_282 = new cjs.Shape();
	this.shape_282.graphics.f("#80DEEA").s().p("Ah3B4QgxgyAAhGQAAhFAxgyQAygyBFABQBGgBAyAyQAxAyAABFQAABGgxAyQgyAxhGABQhFgBgygxg");
	this.shape_282.setTransform(1188,312.5);

	this.shape_283 = new cjs.Shape();
	this.shape_283.graphics.f("#00ACC1").s().p("AhhBjQgpgqAAg5QAAg4ApgpQApgpA4gBQA6ABAoApQApApAAA4QAAA5gpAqQgoApg6gBQg4ABgpgpg");
	this.shape_283.setTransform(1188,250.8);

	this.shape_284 = new cjs.Shape();
	this.shape_284.graphics.f("#80DEEA").s().p("Ah3B4QgxgyAAhGQAAhGAxgxQAygyBFABQBGgBAyAyQAxAxAABGQAABGgxAyQgyAxhGAAQhFAAgygxg");
	this.shape_284.setTransform(1188,250.8);

	this.shape_285 = new cjs.Shape();
	this.shape_285.graphics.f("#00ACC1").s().p("AhhBjQgpgpAAg6QAAg5ApgpQApgpA4AAQA6AAAoApQApApAAA5QAAA6gpApQgoAog6AAQg4AAgpgog");
	this.shape_285.setTransform(1188,189);

	this.shape_286 = new cjs.Shape();
	this.shape_286.graphics.f("#80DEEA").s().p("Ah3B4QgxgyAAhGQAAhFAxgyQAygyBFAAQBGAAAyAyQAxAyAABFQAABGgxAyQgyAxhGABQhFgBgygxg");
	this.shape_286.setTransform(1188,189);

	this.instance_11 = new lib.Path();
	this.instance_11.parent = this;
	this.instance_11.setTransform(640,384,1,1,0,0,0,640.5,384.5);
	this.instance_11.alpha = 0.602;

	this.shape_287 = new cjs.Shape();
	this.shape_287.graphics.f().s("#26C6DA").p("EBkAA8AMjH/AAAMAAAh3/MDH/AAAg");
	this.shape_287.setTransform(640,384);

	this.shape_288 = new cjs.Shape();
	this.shape_288.graphics.f("#26C6DA").s().p("Ehj/A8AMAAAh3/MDH/AAAMAAAB3/g");
	this.shape_288.setTransform(640,384);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_288},{t:this.shape_287},{t:this.instance_11},{t:this.shape_286},{t:this.shape_285},{t:this.shape_284},{t:this.shape_283},{t:this.shape_282},{t:this.shape_281},{t:this.shape_280},{t:this.shape_279},{t:this.shape_278},{t:this.shape_277},{t:this.shape_276},{t:this.shape_275},{t:this.shape_274},{t:this.shape_273},{t:this.instance_10},{t:this.shape_272},{t:this.instance_9},{t:this.shape_271},{t:this.shape_270},{t:this.shape_269},{t:this.shape_268},{t:this.shape_267},{t:this.shape_266},{t:this.shape_265},{t:this.shape_264},{t:this.shape_263},{t:this.shape_262},{t:this.shape_261},{t:this.shape_260},{t:this.shape_259},{t:this.shape_258},{t:this.shape_257},{t:this.shape_256},{t:this.shape_255},{t:this.shape_254},{t:this.shape_253},{t:this.shape_252},{t:this.shape_251},{t:this.shape_250},{t:this.shape_249},{t:this.shape_248},{t:this.shape_247},{t:this.shape_246},{t:this.shape_245},{t:this.shape_244},{t:this.shape_243},{t:this.shape_242},{t:this.shape_241},{t:this.shape_240},{t:this.shape_239},{t:this.shape_238},{t:this.shape_237},{t:this.shape_236},{t:this.shape_235},{t:this.shape_234},{t:this.shape_233},{t:this.shape_232},{t:this.shape_231},{t:this.shape_230},{t:this.shape_229},{t:this.shape_228},{t:this.shape_227},{t:this.shape_226},{t:this.shape_225},{t:this.shape_224},{t:this.shape_223},{t:this.shape_222},{t:this.shape_221},{t:this.shape_220},{t:this.shape_219},{t:this.shape_218},{t:this.shape_217},{t:this.shape_216},{t:this.shape_215},{t:this.shape_214},{t:this.shape_213},{t:this.shape_212},{t:this.shape_211},{t:this.shape_210},{t:this.shape_209},{t:this.shape_208},{t:this.shape_207},{t:this.shape_206},{t:this.shape_205},{t:this.shape_204},{t:this.shape_203},{t:this.shape_202},{t:this.shape_201},{t:this.shape_200},{t:this.shape_199},{t:this.shape_198},{t:this.shape_197},{t:this.shape_196},{t:this.shape_195},{t:this.shape_194},{t:this.shape_193},{t:this.shape_192},{t:this.shape_191},{t:this.shape_190},{t:this.shape_189},{t:this.shape_188},{t:this.shape_187},{t:this.shape_186},{t:this.shape_185},{t:this.shape_184},{t:this.shape_183},{t:this.shape_182},{t:this.shape_181},{t:this.shape_180},{t:this.shape_179},{t:this.shape_178},{t:this.shape_177},{t:this.shape_176},{t:this.shape_175},{t:this.shape_174},{t:this.shape_173},{t:this.shape_172},{t:this.shape_171},{t:this.shape_170},{t:this.shape_169},{t:this.shape_168},{t:this.shape_167},{t:this.shape_166},{t:this.shape_165},{t:this.shape_164},{t:this.shape_163},{t:this.shape_162},{t:this.shape_161},{t:this.shape_160},{t:this.shape_159},{t:this.shape_158},{t:this.shape_157},{t:this.shape_156},{t:this.shape_155},{t:this.shape_154},{t:this.shape_153},{t:this.shape_152},{t:this.shape_151},{t:this.shape_150},{t:this.shape_149},{t:this.shape_148},{t:this.shape_147},{t:this.shape_146},{t:this.shape_145},{t:this.shape_144},{t:this.shape_143},{t:this.shape_142},{t:this.shape_141},{t:this.shape_140},{t:this.shape_139},{t:this.shape_138},{t:this.shape_137},{t:this.shape_136},{t:this.shape_135},{t:this.shape_134},{t:this.shape_133},{t:this.shape_132},{t:this.shape_131},{t:this.shape_130},{t:this.shape_129},{t:this.shape_128},{t:this.shape_127},{t:this.shape_126},{t:this.shape_125},{t:this.shape_124},{t:this.shape_123},{t:this.shape_122},{t:this.shape_121},{t:this.shape_120},{t:this.shape_119},{t:this.shape_118},{t:this.shape_117},{t:this.shape_116},{t:this.shape_115},{t:this.shape_114},{t:this.shape_113},{t:this.shape_112},{t:this.shape_111},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.instance_2},{t:this.instance_1},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(470.2,383,1450.9,770);
// library properties:
lib.properties = {
	width: 1280,
	height: 768,
	fps: 25,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"sounds/audio.mp3?1519368851666", id:"audio"},
		{src:"https://code.jquery.com/jquery-2.2.4.min.js?1519368851666", id:"lib/jquery-2.2.4.min.js"},
		{src:"components/sdk/anwidget.js?1519368851666", id:"sdk/anwidget.js"},
		{src:"components/ui/src/textinput.js?1519368851666", id:"an.TextInput"},
		{src:"components/ui/src/css.js?1519368851666", id:"an.CSS"}
	],
	preloads: []
};


function _updateVisibility(evt) {
	if((this.getStage() == null || this._off || this._lastAddedFrame != this.parent.currentFrame) && this._element) {
		this._element.detach();
		stage.removeEventListener('drawstart', this._updateVisibilityCbk);
		this._updateVisibilityCbk = false;
	}
}
function _handleDrawEnd(evt) {
	var props = this.getConcatenatedDisplayProps(this._props), mat = props.matrix;
	var tx1 = mat.decompose(); var sx = tx1.scaleX; var sy = tx1.scaleY;
	var dp = window.devicePixelRatio || 1; var w = this.nominalBounds.width * sx; var h = this.nominalBounds.height * sy;
	mat.tx/=dp;mat.ty/=dp; mat.a/=(dp*sx);mat.b/=(dp*sx);mat.c/=(dp*sy);mat.d/=(dp*sy);
	this._element.setProperty('transform-origin', this.regX + 'px ' + this.regY + 'px');
	var x = (mat.tx + this.regX*mat.a + this.regY*mat.c - this.regX);
	var y = (mat.ty + this.regX*mat.b + this.regY*mat.d - this.regY);
	var tx = 'matrix(' + mat.a + ',' + mat.b + ',' + mat.c + ',' + mat.d + ',' + x + ',' + y + ')';
	this._element.setProperty('transform', tx);
	this._element.setProperty('width', w);
	this._element.setProperty('height', h);
	this._element.update();
}

function _tick(evt) {
	var stage = this.getStage();
	stage&&stage.on('drawend', this._handleDrawEnd, this, true);
	if(!this._updateVisibilityCbk) {
		this._updateVisibilityCbk = stage.on('drawstart', this._updateVisibility, this, false);
	}
}


})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;