(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.an_TextInput = function(options) {
	this._element = new $.an.TextInput(options);
	this._el = this._element.create();
	var $this = this;
	this.addEventListener('added', function() {
		$this._lastAddedFrame = $this.parent.currentFrame;
		$this._element.attach($('#dom_overlay_container'));
	});
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,22);

p._tick = _tick;
p._handleDrawEnd = _handleDrawEnd;
p._updateVisibility = _updateVisibility;



(lib.Path_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(2).p("AiJkOQB4ArBIBoQBMBrAACCQAABSgfBL");
	this.shape.setTransform(14.1,28.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_4, new cjs.Rectangle(-0.6,0,28.9,56.3), null);


(lib.Path_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3).p("AiHhrID+gLIgxD6");
	this.shape.setTransform(13.7,13.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_3, new cjs.Rectangle(-1.4,0.1,28.8,28.1), null);


(lib.Path_2_0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3).p("AiLgcIDvhYIAeD8");
	this.shape.setTransform(14.5,13.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_2_0, new cjs.Rectangle(-1,-0.2,30,29.2), null);


(lib.Path_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3).p("AGHrOIsNWd");
	this.shape.setTransform(40.4,72.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_2, new cjs.Rectangle(-0.2,-0.8,81.2,146.8), null);


(lib.Path_1_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(2).p("ACoEYQiNgdhchwQhehyAAiSQAAhRAghN");
	this.shape.setTransform(17,28.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_1_2, new cjs.Rectangle(0,-0.6,34.8,58), null);


(lib.Path_1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3).p("AiLgcIDvhYIAeD8");
	this.shape.setTransform(14.5,13.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_1_1, new cjs.Rectangle(-1,-0.2,30,29.2), null);


(lib.Path_1_0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3).p("AGHrOIsNWd");
	this.shape.setTransform(40.5,72.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_1_0, new cjs.Rectangle(-0.1,-0.8,81.2,146.8), null);


(lib.Path_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EhNbAJ5QhbAAhBhAQhAhBAAhbIAAwVMChvAAAIAAQVQAABbhABBQhBBAhbAAg");
	this.shape.setTransform(517.6,63.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_1, new cjs.Rectangle(0,0,1035.3,126.6), null);


(lib.Path_0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(3).p("AqUJlIUpzJ");
	this.shape.setTransform(67.1,62.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_0, new cjs.Rectangle(-0.5,-0.4,135.3,125.6), null);


(lib.Path = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#26C6DA").p("EBkAA8AMjH/AAAMAAAh3/MDH/AAAg");
	this.shape.setTransform(640.5,384.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#26C6DA").s().p("Ehj/A8AMAAAh3/MDH/AAAMAAAB3/g");
	this.shape_1.setTransform(640.5,384.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path, new cjs.Rectangle(-0.5,-0.5,1282,770), null);


(lib.Group_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Egp1A44QgsAAgggfQgfggAAgtMAAAhuXQAAgtAfggQAggfAsAAMBHSAAAMAOEBPxMgGQAh+g");
	this.shape.setTransform(278.5,364);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Group_1, new cjs.Rectangle(0,0,557,728), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAQAoIAAgPQAKAAAGgGQAGgIAAgKQAAgKgGgIQgGgIgKABQgLgBgHAJQAJABAGAIQAGAHAAAJQAAAOgLAJQgJAJgQgBQglAAAAgoIAAgGIAOAAIAAABQAAADAEAAQADAAADgCIAFgEIACgFIABgCQAAgEgCgDQgBgCgDAAQgGAAAAAIIgPAAIgBgEQAAgIAGgFQAGgHAJAAQAHABAFADQAFADABAGQAKgNASAAQAQAAALAMQAKAMABAPQAAARgJAMQgJANgSgBIgDAAgAgPAYQAGAAAFgDQAEgEABgFIAAgEQAAgGgEgDIgFgBQgBAAgBAAQAAAAgBAAQAAABgBAAQAAAAgBAAIgJAIQgGAEgFAAIgFgBQAEAPATgBg");
	this.shape.setTransform(147,18.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgKAUIAAgZQAAgFgGAAQgGAAgCAFIAAgNQACgDAEgCQAEgCAEAAQAIAAACAHQACgHAKAAQAGAAADAFQAEAFABAGQAAAMgNADQgGABgEgCIAAAVQgHgFgGgBgAADgKIAAAHIADABQAEAAABgGQAAAAgBgBQAAAAAAgBQAAAAAAgBQgBAAAAAAQAAgBgBAAQAAAAAAgBQgBAAAAAAQAAAAgBAAQAAAAgBAAQAAAAgBABQAAAAAAAAQgBABAAABg");
	this.shape_1.setTransform(138.4,13.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAQAoIAAgPQAKAAAGgGQAGgIAAgKQAAgKgGgIQgGgIgKABQgLgBgHAJQAJABAGAIQAGAHAAAJQAAAOgLAJQgJAJgQgBQglAAAAgoIAAgGIAOAAIAAABQAAADAEAAQADAAADgCIAFgEIACgFIABgCQAAgEgCgDQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAAAgBAAQgGAAAAAIIgPAAIgBgEQAAgIAGgFQAGgHAJAAQAHABAFADQAFADABAGQAKgNASAAQAQAAALAMQAKAMABAPQAAARgJAMQgJANgSgBIgDAAgAgPAYQAGAAAFgDQAEgEABgFIAAgEQAAgGgEgDIgFgBQgBAAgBAAQAAAAgBAAQAAABgBAAQAAAAgBAAIgJAIQgGAEgFAAIgFgBQAEAPATgBg");
	this.shape_2.setTransform(135.3,18.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AABAgQgFAIgQAAQgPAAgHgLQgIgLABgSQABgQALgMQALgMAQAAIAFABIAAAOQgLgBgIAHQgHAIgBALQgBAJADAHQAEAIAIAAQAMAAAAgLIAAgHIAOAAIAAAHQAAAFAEADQAEADAGAAQAGAAADgEQADgEgBgGQAAgNgTAAIgSAAIAAgIQAAgbAYgCQAWAAACAVIgQAAQAAgGgHAAQgEAAgCACQgDACAAAEIAIAAQANAAAJAIQAIAIAAAMQAAANgHAIQgHAIgKAAIgCABQgSAAgFgJg");
	this.shape_3.setTransform(124.2,19);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgjAhQgOgJAAgQQAAgKAJgIQAIgHANAAIAMAAQAAgIgMAAQgLAAACAHIgRAAIAAgDQAAgJAIgFQAHgFALAAQAOAAAFAIQAFgEAEgCQAGgCAJAAQALAAAHAHQADAEAAAHIgBAEIgRAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAgBgBAAQgBgDgHAAQgKAAAAAIIALAAQANAAAIAFQAKAJAAAMQAAAQgPAIQgOAIgVAAQgWAAgNgIgAgbAAQgFACABAFQgBAJAKAFQAJAEANAAQAOAAAJgEQAJgFAAgIQAAgGgFgCQgEgDgHAAIggAAQgHAAgEADg");
	this.shape_4.setTransform(113.6,19);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgSAoQgPAAgIgIQgHgIAAgOQABgJAHgEIgMAAIAAgNIAvAAQAAgJgLAAQgKAAAAAHIgQAAQgBgKAIgGQAHgFAKAAIAMABQAGACACAEQAEgFAHgBIANgBQAIAAAGAGQAGAGAAAIIgRAAQABgGgKAAQgKAAAAAJIAOAAQAdAAAAAcQAAANgIAHQgJAIgMAAQgRAAgFgJQgEAJgPAAIgBAAgAAKAHIAAAHQAAAKAOAAQAFAAAEgEQADgFAAgGQAAgGgFgDQgFgDgHAAIgiAAQgGAAgFADQgFAEAAAGQAAAGAEAEQADAEAGAAQAGAAAEgDQADgCAAgFIAAgHg");
	this.shape_5.setTransform(102.7,18.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgQAhQgJgIABgRQAAgKAJgKQAHgJAIgDIgZAAIAAgNIAwAAIAAALQgNAJgHAHQgIAJgBAJIABAJQACAHAFgBQAEAAACgDQACgCAAgEIAAgEIAQAAIAAAHQAAAKgGAGQgHAGgJAAQgNgBgHgFg");
	this.shape_6.setTransform(88.8,19.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgUAyQgJgEgGgHQgJgMAAgOQAAgHACgGQAGgSAVgIIAJgFIAKgJQAEgEAEgJIATAAQgCAIgEAGQgHAJgIAEIAGABQAOADAIAMQAHAKAAANQAAAKgEAJQgFAKgMAGQgLAGgNAAQgKAAgKgEgAgUgDQgGAGAAAJQAAALAIAHQAHAGAMAAQAOAAAHgJQAFgHAAgIQAAgLgIgHQgHgGgMAAQgNAAgHAJg");
	this.shape_7.setTransform(81.2,17.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgSAPQgHgGAAgJQgCgSARgCIAAAOQAAAGADADQAEAEAFAAQAEAAADgDQACgDAAgCIAAgEIAPAAIABAFQAAAIgIAHQgHAGgKAAQgNAAgHgGg");
	this.shape_8.setTransform(74,23.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgmAlQgKgEgCgJQAAgCABgDQABgBAAgBQAAgBABAAQAAgBAAAAQAAgBAAAAIgCgBIgEACIgCADIAAgOIAOgOIANgLIAAgCQAAAAAAgBQAAAAAAAAQAAgBgBAAQAAAAAAAAIgEgBQgEABAAAFIABAEIgQAAIAAgFQAAgIAGgFQAFgFAIAAQAOgBADALQAPgKASAAQAQAAALAKQAMAMAAATQAAARgKAKQgKALgRAAIgEgBIAAgNQALAAAGgGQAHgHAAgLQAAgMgHgHQgHgHgNAAQgHAAgFACQAIADAHAIQAGAIAAAKQAAAPgMAJQgKAJgQAAQgMAAgJgDgAgZgEIgLAJQACACACADIAAACIAAACIgBADQAAAEAFACQAEACAFAAQAIAAAHgEQAHgFAAgHQABgIgEgFQgFgFgHAAIgDAAQgEAAgGAFg");
	this.shape_9.setTransform(70.5,19);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAZAPQABgIgJgEQgHgDgKAAQgJAAgHACQgJAEAAAGIABADIgRAAIgBgHQAAgKAOgGQANgGAPABQARAAAMAEQAOAHAAAJIgCAIg");
	this.shape_10.setTransform(59.2,13.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AggApQgOgLgCgTIgBgGQAAgWAOgHQAAgMAHgHQAHgHALAAQAKAAAFAHQAGgIAMABQAKAAAHAKQAFAKAAAMIAAADIg5AAIgHABQABAKAHAFQAHAGAKAAQAPAAAGgOIARAAQgEANgKAIQgLAIgPAAQgMAAgKgGQgKgHgEgKIAAADQAAAPAKAKQAKAJAOAAQALAAAIgFQAJgFADgKIARAAQgCARgOAJQgNAKgTAAQgUAAgOgLgAALgcIASAAQgCgJgHAAQgHAAgCAJgAgTgcIAQAAQgBgJgHAAQgHAAgBAJg");
	this.shape_11.setTransform(59.5,20.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAZAPQABgIgJgEQgHgDgKAAQgJAAgHACQgJAEAAAGIABADIgRAAIgBgHQAAgKAOgGQANgGAPABQARAAAMAEQAOAHAAAJIgCAIg");
	this.shape_12.setTransform(48.8,13.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgjAhQgOgJAAgQQAAgKAJgIQAIgHANAAIAMAAQAAgIgMAAQgLAAABAHIgQAAIgBgDQAAgJAJgFQAHgFAKAAQAPAAAFAIQAFgEAEgCQAGgCAJAAQALAAAHAHQADAEAAAHIgBAEIgRAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAgBgBAAQgBgDgHAAQgJAAgBAIIALAAQANAAAIAFQAKAJAAAMQAAAQgQAIQgNAIgVAAQgVAAgOgIgAgbAAQgFACABAFQAAAJAJAFQAJAEANAAQAOAAAJgEQAJgFAAgIQAAgGgFgCQgEgDgHAAIggAAQgHAAgEADg");
	this.shape_13.setTransform(48.9,19);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#F44336").s().p("As2C8QhNABg4g4Qg3g4AAhNQAAhNA3g3QA4g4BNAAIZtAAQBOAAA3A4QA3A3AABNQAABNg3A4Qg3A4hOgBg");
	this.shape_14.setTransform(101.1,18.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F44336").s().p("AiBhPIEDAAIiCCfg");
	this.shape_15.setTransform(101.1,45.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,202.3,53.4), null);


(lib.an_CSS = function(options) {
	this._element = new $.an.CSS(options);
	this._el = this._element.create();
	var $this = this;
	this.addEventListener('added', function() {
		$this._lastAddedFrame = $this.parent.currentFrame;
		$this._element.attach($('#dom_overlay_container'));
	});
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,100,22);

p._tick = _tick;
p._handleDrawEnd = _handleDrawEnd;
p._updateVisibility = _updateVisibility;



(lib.correct = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhrgEIAdgeIA4A3IBlhmIAeAeIiDCFg");
	this.shape.setTransform(0.4,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#419946").s().p("AAXBpIAAABIAAgBIAAAAIgCgCIAAABIAAgBIgCgBIAAgBIAAABIgCgBIgBgBIAAgBIgBAAIAAAAIgBgBIAAgBIgBgBIgCgCIgCAAIAAgBIgBAAIAAAAIgBgBIgBgBIgBAAIgBgCIgBgBIgBgBIAAAAIgCgBIAAAAIAAgBIgBgBIgBgBIgBgBIgBgBIgBAAIAAgBIgBAAIAAgBIgCgBIgCgCIgBAAIAAgBIgBgBIAAAAIgBAAIAAgBIgBAAIgDgCIgBgBIAAgBIgBgBIgBAAIAAAAIgBgBIAAgBIgCgBIgCgBIAAgBIgBAAIgBgBIAAAAIgBgBIgBgBIgBgCIgBAAIhWhWIAegfIACACIABAAIABADIABAAIABABIABABIACABIABAAIACACIAAABIABABIABAAIACADIAAAAIACABIAAAAIABABIABAAIAAABIAAAAIACACIAAgBIABABIAAAAIACACIAAgBIAAACIABAAIABABIAAAAIADACIAAAAIAAAAIAAAAIACACIAAgBIAAABIABABIABABIAAgBIADADIAAAAIAAABIABAAIAAAAIABABIAAAAIABABIAAAAIABABIAAgBIACADIAAgBIABABIABAAIAAABIABABIAAgBIABACIABAAIAAABIAAAAIACACIAAAAIABAAIABABIAAABIABgBIABABIABABIAAABIAAgBIACADIAAAAIABABIABAAIAAABIABAAIAAABIBghjIABAAIAAABIABAAIABABIAAAAIABABIABAAIACADIABAAIABABIAAABIABAAIAAAAIABABIABAAIACACIABABIABABIABAAIAAABIABAAIAAABIAAABIABAAIACACIABAAIABABIABAAIAAABIABAAIAAABIABAAIAAAAIACADIABAAIAAABIABABIADABIAAAAIACACIABABIAAABIABAAQAMAbAAAbIAAADQAAArgaAjQgbAigpAMg");
	this.shape_1.setTransform(1.3,2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#52B855").s().p("AgDCCQg0AAgmgmQglgmAAg1IAAgBQAAg2AlgmQAmglA0AAIAHAAQA0AAAmAlQAlAmAAA2IAAABQAAA1glAmQgmAmg0AAg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgDCLQg4AAgogoQgngpAAg5IAAgBQAAg6AngoQAogoA4AAIAHAAQA4AAAoAoQAnAoAAA6IAAABQAAA5gnApQgoAog4AAg");

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAAAeIg3A5IgdgeIA3g5Ig3g4IAdgeIA3A4IA4g4IAdAeIg3A4IA3A5IgdAeg");
	this.shape_4.setTransform(0,0.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#C4342B").s().p("AgaBtIgBgBIAAAAIgBAAIgBgBIAAgBIAAABIgCgDIAAABIgBgBIAAAAIgCgCIAAABIgBgBIgBgBIAAgBIAAABIgDgDIAAABIAAgBIgBAAIgBgCIAAABIAAgBIgBgBIgBgBIAAABIgDgDIAAABIAAgBIgBAAIgBgCIAAABIgBgBIAAgBIgBgBIAAABIgCgDIAAABIgBgBIgBAAIgBgCIAAABIgBgBIgBgBIAAgBIAAABIgCgDIAAABIgBgBIAAAAIgCgCIAAABIgBgBIgBgBIAAgBIAAABIgDgDIAAABIAAgBIAAAAIgBgBIAAgBIgBABIAAgBIgCgBIAAgBIAAABIgDgDIAAABIAAgBIgBgBIgBgBIAAABIAAgBIgBgBIgegeIA4g4Ig4g5IAegeIABABIABACIABAAIAAAAIABABIAAAAIABABIABAAIAAABIACABIACACIAAAAIABABIAAAAIABABIABAAIAAABIABABIADACIABAAIAAABIABAAIAAABIABAAIAAABIABABIACACIACAAIAAABIABAAIAAABIABAAIABABIAAABIACACIABAAIABABIAAAAIABABIABAAIABABIABABIABACIABAAIABABIAAAAIABABIABAAIAAABIABABIADACIABAAIAAABIAAAAIABABIABAAIAAABIABABIACACIACAAIAAABIABAAIAAABIABAAIAAABIABABIACACIABAAIABABIAyg0IAAABIABAAIABABIABABIACACIABAAIABACIABAAIABABIABABIACACIABAAIACACIABAAIAAABIABABIACACIAAAAIACACIACAAIAAABIABABIACACIAAAAIACACIABAAIABABIABABIACACIABAAIABACIABAAIABABIABABIACACIABAAIACACIABAAIAAABIABABIACACIAAAAIADACIABAAIAAABIABABIACACIAAAAIABAAIAAABIABAAIAAABIACAAIAAABIABABIACACIABAAIAAABIABAAIAAABIABAAIABABIABABIACACIABAAIABABIAAAAIAAABIABAAIABABIABABIACAXIAAACQAAA1glAmQgmAmgzAAg");
	this.shape_5.setTransform(2.3,2.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#EE4036").s().p("AgDCCQg0ABgmgmQglgnAAg1IAAgCQAAg0AlgnQAmglA0gBIAHAAQA0ABAmAlQAlAnAAA0IAAACQAAA1glAnQgmAmg0gBg");
	this.shape_6.setTransform(0,0.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgDCLQg4AAgogoQgngoAAg6IAAgBQAAg5AngpQAogoA4AAIAHAAQA4AAAoAoQAnApAAA5IAAABQAAA6gnAoQgoAog4AAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-13.9,-13.9,27.9,27.9);


(lib.confirm = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#004D40").s().p("AgcA/QgVgBgOgOQgOgOgBgUQgBgSAGgNIAPAAQgBAFACACQABACAFAAQAHAAAGgEQALgHADgJIABgFQAAgKgHgDQgGgCgFACQAJADAAALQAAAGgFAEQgEAEgHAAIgGgBQgLgDAAgOIACgJQAGgRAXAAQAJAAAHAFQAHAEACAHQAMgRAaABQAbACAOAZQAJARAAATQgBAqgeAPQgKAFgLAAIgOgCIAAgKQgKAHgIADQgHACgLAAIgFAAgAAOgPQAOALAAATQAAATgMANIAKABQALAAAGgEQAPgMAAgYQAAgQgLgMQgKgMgQgBQgTgBgJAMIACAAQALAAAIAHgAg+AJQAAAKAGAGQAHAKASAAIAKAAQAMgCAIgIQAGgIAAgKQAAgGgDgDQgDgHgKgBQgFAAgIAFIgNAHQgGADgIAAQgGAAgEgDIgBAHg");
	this.shape.setTransform(63.2,4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#004D40").s().p("AgQAeIAAgZIgLAAIAAgLIALAAIAAgQIgLAAIAAgLIAgAAQAKAAAGAEQAHAHAAAJQAAAJgIAFQgJAFgMgBIAAAdgAgBgWIAAAQQALAAAAgHQAAgJgJAAIgCAAg");
	this.shape_1.setTransform(52.1,-4.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#004D40").s().p("AgcA/QgVgBgOgOQgOgOgBgUQgBgSAGgNIAPAAQgBAFACACQABACAFAAQAHAAAGgEQALgHADgJIABgFQAAgKgHgDQgGgCgFACQAJADAAALQAAAGgFAEQgEAEgHAAIgGgBQgLgDAAgOIACgJQAGgRAXAAQAJAAAHAFQAHAEACAHQAMgRAaABQAbACAOAZQAJARAAATQgBAqgeAPQgKAFgLAAIgOgCIAAgKQgKAHgIADQgHACgLAAIgFAAgAAOgPQAOALAAATQAAATgMANIAKABQALAAAGgEQAPgMAAgYQAAgQgLgMQgKgMgQgBQgTgBgJAMIACAAQALAAAIAHgAg+AJQAAAKAGAGQAHAKASAAIAKAAQAMgCAIgIQAGgIAAgKQAAgGgDgDQgDgHgKgBQgFAAgIAFIgNAHQgGADgIAAQgGAAgEgDIgBAHg");
	this.shape_2.setTransform(46.7,4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#004D40").s().p("AgEBPQgbgBgRgQQgQgRAAgaQgBgRAHgMQAGgMAOgNQAKgIASgSQAJgLADgHIAZAAQgEAJgIAJQgHAIgLAIQARgBANAFQAmAOAAAsQAAAbgUASQgTASgaAAIgEgBgAgjgRQgKAMAAAPQAAAPAKAKQANAOAWAAQARAAAMgJQAQgNAAgTQAAgIgDgIQgFgLgLgHQgLgGgOAAQgXAAgNAPg");
	this.shape_3.setTransform(31.8,2.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#004D40").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_4.setTransform(16.4,4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#004D40").s().p("AgbAzQgLgJAAgSQAAgNAFgKQADgFAJgKQAJgMAZgTIgwAAIAAgPIBHAAIAAAOIgQANQgIAFgHAHQgJAMgDAFQgHAJABALQAAAHAGAEQAEADAGAAQANABAEgLIABgHQAAgFgDgFIAPAAQAHALgBANQgBAOgMAKQgLAKgPgBQgQAAgLgJg");
	this.shape_5.setTransform(-4,4.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#004D40").s().p("AgEBPQgbgBgRgQQgQgRAAgaQgBgRAHgMQAGgMAOgNQAKgIASgSQAJgLADgHIAZAAQgEAJgIAJQgHAIgLAIQARgBANAFQAmAOAAAsQAAAbgUASQgTASgaAAIgEgBgAgjgRQgKAMAAAPQAAAPAKAKQANAOAWAAQARAAAMgJQAQgNAAgTQAAgIgDgIQgFgLgLgHQgLgGgOAAQgXAAgNAPg");
	this.shape_6.setTransform(-15,2.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#004D40").s().p("Ag+AqIAAgXIBvAAIAAgnIAHgKQAFgGACgFIAABTg");
	this.shape_7.setTransform(-29.4,10.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#004D40").s().p("AgzBCQgVgPABgbQABgXATgJQgKgCAAgKQAAgOAOgGQAKgFAOAAQARABAKAOQAKANAAARIgrAAQgJAAgHAFQgHAGABAJQABAWAdAGQAJACAMAAQAYAAAPgQQAOgPABgXQAAgYgOgSQgPgTgcAAQgggBgRAYIgQAAQAIgSASgKQARgJAWAAQAhAAAVAXQAVAXAAAhQAAAhgTAWQgUAZghAAQggAAgTgOgAgigcQADACABADQACAGgCAFIAbAAQgCgHgFgGQgGgFgJAAQgFAAgEACg");
	this.shape_8.setTransform(-29.4,2.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#004D40").s().p("AASAwQANABAKgGQAKgGAEgMQAFgMgBgMQgBgOgKgKQgKgLgOAAQgXACgFAUQAEgDAFAAQAHAAAEADQAOAJgBAWQAAAUgSAMQgPAMgXAAQgWAAgQgOQgPgOAAgVQAAgXARgLQgFgCgDgEQgDgFAAgGQAAgLAKgHQAJgGAMAAQALAAAIAHQAIAGACALQAOgXAcgBQAXgBAPAUQAPATAAAYQAAAbgSARQgRASgcAAgAg2gHQgHAGAAAJQAAAMAMAHQALAGAPAAQANAAAKgFQAKgGABgKQAAgFgCgEQgDgDgEAAQgDAAgCACQgDADAAADIgOAAIAAgLIABgKIgSAAQgKAAgHAGgAgygtQADACADAGQABAFgBAFIAPAAQgBgKgFgEQgEgEgHAAIgEAAg");
	this.shape_9.setTransform(-45.3,4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#004D40").s().p("AhIAkQgEgMADgJIgLAAIAAgMIAJgJIALgMQgIAAgEgFQgEgFAAgIQAAgLAJgHQAIgHAMgBQATgBAJAOQARgNAaAAIAJAAQAZACAQATQAQAUgBAYQgCAZgQARQgQASgZAAIgJgBIAAgMQgQAMgYABIgDAAQgmAAgIgbgAgHgkQAHABAHAFQAIAEAFAFQAMANAAAQQABAYgNAPIAFABQALAAAIgFQAQgLACgVQABgNgHgNQgGgMgNgGQgLgFgQAAQgKAAgHACgAgrgQQgJAHgOANIAKAAQgEAJACAJQAGARAagBQAOgBALgJQAJgJABgNQABgLgJgJQgHgJgNAAIgCAAQgMAAgKAHgAg0gvIgGADQAGABACADQACAFgBAFIAHgGIAGgFQgDgEgGgCIgDAAIgEAAg");
	this.shape_10.setTransform(-62.4,4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFCA28").s().p("As6ENQgOAAgKgJQgLgIgCgNIhUluQgEgRALgOQALgOARgBIcdhfQATgBAMAOQAMAOgDASIhWHMQgCAOgLAJQgLAJgNAAg");

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AthE1QgOAAgLgIQgKgJgDgOIhWm9QgDgRALgOQALgNARgBIdwhgQASAAAMANQAMANgDASIhYIdQgCAOgLAJQgKAJgOAAg");

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF6F00").s().p("As6ENQgOAAgKgJQgLgIgCgNIhUluQgEgRALgOQALgOARgBIcdhfQATgBAMAOQAMAOgDASIhWHMQgCAOgLAJQgLAJgNAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_12},{t:this.shape_13},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-99,-30.9,198.1,61.8);


(lib.answer = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#004D40").s().p("AgEBPQgbgBgRgQQgQgRAAgaQgBgRAHgMQAGgMAOgNQAKgIASgSQAJgLADgHIAZAAQgEAJgIAJQgHAIgLAIQARgBANAFQAmAOAAAsQAAAbgUASQgTASgaAAIgEgBgAgjgRQgKAMAAAPQAAAPAKAKQANAOAWAAQARAAAMgJQAQgNAAgTQAAgIgDgIQgFgLgLgHQgLgGgOAAQgXAAgNAPg");
	this.shape.setTransform(60.7,2.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#004D40").s().p("AgWAWQgJgHgDgNIgBgFQAAgPAKgKIAAAQIAGAAQAAAJAGAFQAGAHAIAAQAKAAAFgHQAHgHgCgJIAPAAIAAAHQAAAPgKAKQgKALgQAAQgMAAgKgHg");
	this.shape_1.setTransform(49.9,11.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#004D40").s().p("AhIAkQgEgMADgJIgLAAIAAgMIAJgJIALgMQgIAAgEgFQgEgFAAgIQAAgLAJgHQAIgHAMgBQATgBAJAOQARgNAaAAIAJAAQAZACAQATQAQAUgBAYQgCAZgQARQgQASgZAAIgJgBIAAgMQgQAMgYABIgDAAQgmAAgIgbgAgHgkQAHABAHAFQAIAEAFAFQAMANAAAQQABAYgNAPIAFABQALAAAIgFQAQgLACgVQABgNgHgNQgGgMgNgGQgLgFgQAAQgKAAgHACgAgrgQQgJAHgOANIAKAAQgEAJACAJQAGARAagBQAOgBALgJQAJgJABgNQABgLgJgJQgHgJgNAAIgCAAQgMAAgKAHgAg0gvIgGADQAGABACADQACAFgBAFIAHgGIAGgFQgDgEgGgCIgDAAIgEAAg");
	this.shape_2.setTransform(45,4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#004D40").s().p("AAoAdQALgHgBgKIAAgDQgCgHgQgEQgOgDgSAAQgRAAgOAEQgQADgCAHIgBAEQAAAKAKAGIgOAAQgMgEAAgRQAAgGACgDQAIgQASgGQAOgFAYAAQA2AAALAbQACADAAAGQAAAQgMAFg");
	this.shape_3.setTransform(28.4,-3.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#004D40").s().p("AgvA4QgWgRAAgcQAAgaATgPQgBgNAFgJQALgVAWACQALABAHAIQAIgKAQABQAPAAAHAKQAJALAAAYIg8AAIgaABQAAAQAMALQAMALAPgBQAMgBAJgHQAJgGACgKIAPAAQgEARgOALQgOAMgTAAQgkAAgPgaIgBAKQAAASAPAMQAOAMASAAQAcgBAOgRQAJgKgBgNIAPAAQABARgLAPQgTAcgnAAQgcAAgUgRgAgXg0QgDAFABAIIALgBIAMAAQAAgQgLAAQgGAAgEAEgAANgoIAZAAQgBgQgLAAQgNAAAAAQg");
	this.shape_4.setTransform(29,5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#004D40").s().p("AAoAdQAKgHAAgKIAAgDQgCgHgQgEQgNgDgTAAQgRAAgOAEQgQADgDAHIAAAEQAAAKAJAGIgNAAQgMgEAAgRQABgGACgDQAHgQATgGQAOgFAXAAQA1AAAMAbQABADAAAGQAAAQgLAFg");
	this.shape_5.setTransform(13.8,-3.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#004D40").s().p("Ag3AtQgRgPAAgZQAAgJAEgKQAFgJAHgFQgHgDAAgJIABgHQAGgPAbAAQAVAAAIANQAJgNAXAAQALAAAIAFQAKAGABAJQABAIgEAGQAGAFAEAJQAEAKAAAJQAAAagSAPQgVARgiAAQgjAAgUgSgAgygNQgGAGABAKQABAQATAHQAOAGAVAAQAUAAAPgGQATgHAAgQQABgKgGgGQgGgGgJgBIhEAAQgKABgGAGgAAQgsQgHAEgBAHIAjAAIAAgEQAAgEgEgEQgFgDgFAAIgBAAQgHAAgFAEgAgngvQAEADABAEQAAAEgCADIAaAAQgBgMgPgDIgGAAQgEAAgDABg");
	this.shape_6.setTransform(13.8,4.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#004D40").s().p("AApAdQAJgHABgKIgBgDQgCgHgPgEQgOgDgTAAQgSAAgNAEQgQADgDAHIAAAEQAAAKAJAGIgNAAQgLgEAAgRQAAgGACgDQAHgQATgGQAOgFAXAAQA1AAALAbQACADAAAGQABAQgMAFg");
	this.shape_7.setTransform(-9.4,-3.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#004D40").s().p("AADBNQgLgGAAgMQgBgJAHgFQgaABgSgPQgSgOACgWQABgbAWgMQgDgBgDgEQgDgEAAgFIABgFQAFgKAMgEQAIgDAQAAQAdAAANAPQAJAMABARIgtAAQgNAAgJAIQgJAJgBAMQgBAPAMALQAMAKAQABQAZABASgKIAAAPIgRAGQgQAEAAAGQgBAFAFADQAEADAGAAQAHAAAGgEQAFgDAAgHIgBgGIAOAAIABAHQAAANgLAJQgKAHgPABIgGABQgKAAgJgEgAgTg/IADACQABAAAAABQAAAAABABQAAAAAAABQAAAAABABQAAAFgEACIAHAAIAkAAQgFgKgLgDQgGgCgJAAQgJAAgFACg");
	this.shape_8.setTransform(-7.3,5.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#004D40").s().p("AgEBPQgbgBgRgQQgQgRAAgaQgBgRAHgMQAGgMAOgNQAKgIASgSQAJgLADgHIAZAAQgEAJgIAJQgHAIgLAIQARgBANAFQAmAOAAAsQAAAbgUASQgTASgaAAIgEgBgAgjgRQgKAMAAAPQAAAPAKAKQANAOAWAAQARAAAMgJQAQgNAAgTQAAgIgDgIQgFgLgLgHQgLgGgOAAQgXAAgNAPg");
	this.shape_9.setTransform(-20.6,2.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#004D40").s().p("AgbAzQgLgJAAgSQAAgNAFgKQADgFAJgKQAJgMAZgTIgwAAIAAgPIBHAAIAAAOIgQANQgIAFgHAHQgJAMgDAFQgHAJABALQAAAHAGAEQAEADAGAAQANABAEgLIABgHQAAgFgDgFIAPAAQAHALgBANQgBAOgMAKQgLAKgPgBQgQAAgLgJg");
	this.shape_10.setTransform(-31.6,4.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#004D40").s().p("AgzBCQgVgPABgbQABgXATgJQgKgCAAgKQAAgOAOgGQAKgFAOAAQARABAKAOQAKANAAARIgrAAQgJAAgHAFQgHAGABAJQABAWAdAGQAJACAMAAQAYAAAPgQQAOgPABgXQAAgYgOgSQgPgTgcAAQgggBgRAYIgQAAQAIgSASgKQARgJAWAAQAhAAAVAXQAVAXAAAhQAAAhgTAWQgUAZghAAQggAAgTgOgAgigcQADACABADQACAGgCAFIAbAAQgCgHgFgGQgGgFgJAAQgFAAgEACg");
	this.shape_11.setTransform(-43.4,2.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#004D40").s().p("AAoAdQAKgHAAgKIAAgDQgCgHgQgEQgNgDgTAAQgRAAgOAEQgQADgDAHIAAAEQAAAKAJAGIgNAAQgMgEAAgRQAAgGADgDQAHgQASgGQAOgFAYAAQA1AAAMAbQABADAAAGQAAAQgLAFg");
	this.shape_12.setTransform(-58.7,-3.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#004D40").s().p("AgcA/QgVgBgOgOQgOgOgBgUQgBgSAGgNIAPAAQgBAFACACQABACAFAAQAHAAAGgEQALgHADgJIABgFQAAgKgHgDQgGgCgFACQAJADAAALQAAAGgFAEQgEAEgHAAIgGgBQgLgDAAgOIACgJQAGgRAXAAQAJAAAHAFQAHAEACAHQAMgRAaABQAbACAOAZQAJARAAATQgBAqgeAPQgKAFgLAAIgOgCIAAgKQgKAHgIADQgHACgLAAIgFAAgAAOgPQAOALAAATQAAATgMANIAKABQALAAAGgEQAPgMAAgYQAAgQgLgMQgKgMgQgBQgTgBgJAMIACAAQALAAAIAHgAg+AJQAAAKAGAGQAHAKASAAIAKAAQAMgCAIgIQAGgIAAgKQAAgGgDgDQgDgHgKgBQgFAAgIAFIgNAHQgGADgIAAQgGAAgEgDIgBAHg");
	this.shape_13.setTransform(-59.1,4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFCA28").s().p("As6ENQgOAAgKgJQgLgIgCgNIhUluQgEgRALgOQALgOARgBIcdhfQATgBAMAOQAMAOgDASIhWHMQgCAOgLAJQgLAJgNAAg");

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AthE1QgOAAgLgIQgKgJgDgOIhWm9QgDgRALgOQALgNARgBIdwhgQASAAAMANQAMANgDASIhYIdQgCAOgLAJQgKAJgOAAg");

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF6F00").s().p("As6ENQgOAAgKgJQgLgIgCgNIhUluQgEgRALgOQALgOARgBIcdhfQATgBAMAOQAMAOgDASIhWHMQgCAOgLAJQgLAJgNAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_15},{t:this.shape_16},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-99,-30.9,198.1,61.8);


(lib.enterans = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_30 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(30).call(this.frame_30).wait(1));

	// Layer 1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(0,7.7,1,1,0,0,0,101.1,26.7);
	this.instance.alpha = 0;

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAQAoIAAgPQAKAAAGgGQAGgIAAgKQAAgKgGgIQgGgHgKgBQgLABgHAIQAJACAGAHQAGAHAAAJQAAAPgLAIQgJAIgQAAQglAAAAgoIAAgHIAOAAIAAACQAAADAEAAQADAAADgCIAFgEIACgFIABgDQAAgDgCgDQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAgBgBAAQgGAAAAAJIgPAAIgBgEQAAgIAGgGQAGgFAJgBQAHAAAFAEQAFADABAHQAKgOASAAQAQAAALAMQAKALABARQAAAQgJAMQgJAMgSAAIgDAAgAgPAYQAGgBAFgDQAEgDABgFIAAgDQAAgIgEgBIgFgCQgBAAgBAAQAAAAgBAAQAAABgBAAQAAAAgBABIgJAIQgGADgFAAIgFgBQAEAPATgBg");
	this.shape.setTransform(44.9,-7.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgKAUIAAgZQAAgFgGAAQgGAAgDAFIAAgNQADgDAEgCQAEgCAEAAQAHAAADAHQACgHAKAAQAGAAAEAFQADAFAAAGQAAAMgMADQgGABgEgCIAAAVQgGgFgHgBgAADgKIAAAHIADABQAFAAgBgGQAAAAAAgBQAAAAAAgBQAAAAAAgBQgBAAAAAAQAAgBgBAAQAAAAAAgBQgBAAAAAAQgBAAAAAAQgBAAAAAAQAAAAgBABQAAAAAAAAQgBABAAABg");
	this.shape_1.setTransform(36.3,-13.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAQAoIAAgPQAKAAAGgGQAGgIAAgKQAAgKgGgIQgGgHgKgBQgLABgHAIQAJACAGAHQAGAHAAAJQAAAPgLAIQgJAIgQAAQglAAAAgoIAAgHIAOAAIAAACQAAABAAAAQABABAAAAQABABAAAAQABAAABAAQADAAADgCIAFgEIACgFIABgDQAAgDgCgDQgBgDgDAAQgGAAAAAJIgPAAIgBgEQAAgIAGgGQAGgFAJgBQAHAAAFAEQAFADABAHQAKgOASAAQAQAAALAMQAKALABARQAAAQgJAMQgJAMgSAAIgDAAgAgPAYQAGgBAFgDQAEgDABgFIAAgDQAAgIgEgBIgFgCQgBAAgBAAQAAAAgBAAQAAABgBAAQAAAAgBABIgJAIQgGADgFAAIgFgBQAEAPATgBg");
	this.shape_2.setTransform(33.2,-7.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AABAgQgFAIgQAAQgPAAgHgLQgIgLABgSQABgQALgMQALgMAQAAIAFABIAAAOQgLgBgIAHQgHAIgBALQgBAJADAHQAEAIAIAAQAMAAAAgLIAAgHIAOAAIAAAHQAAAFAEADQAEADAGAAQAGAAADgEQADgEgBgGQAAgNgTAAIgSAAIAAgIQAAgbAYgCQAWAAACAVIgQAAQAAgGgHAAQgEAAgCACQgDACAAAEIAIAAQANAAAJAIQAIAIAAAMQAAANgHAIQgHAIgKAAIgCABQgSAAgFgJg");
	this.shape_3.setTransform(22.1,-7.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgiAhQgPgJAAgQQAAgKAIgIQAJgHANAAIAMAAQAAgIgLAAQgMAAABAHIgQAAIAAgDQgBgJAJgFQAHgFAKAAQAPAAAGAIQADgEAGgCQAEgCAKAAQAMAAAFAHQAEAEAAAHIAAAEIgSAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAgBAAAAQgCgDgGAAQgLAAAAAIIALAAQAOAAAGAFQALAJAAAMQAAAQgPAIQgOAIgVAAQgVAAgNgIgAgbAAQgEACgBAFQAAAJAKAFQAJAEANAAQAOAAAJgEQAJgFAAgIQAAgGgEgCQgFgDgGAAIghAAQgHAAgEADg");
	this.shape_4.setTransform(11.5,-7.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgSAoQgPAAgIgIQgHgIAAgOQABgJAHgEIgMAAIAAgNIAvAAQAAgJgLAAQgKAAAAAHIgQAAQgBgKAIgGQAHgFAKAAIAMABQAGACACAEQAEgFAHgBIANgBQAIAAAGAGQAGAGAAAIIgRAAQABgGgKAAQgKAAAAAJIAOAAQAdAAAAAcQAAANgIAHQgJAIgMAAQgRAAgFgJQgEAJgPAAIgBAAgAAKAHIAAAHQAAAKAOAAQAFAAAEgEQADgFAAgGQAAgGgFgDQgFgDgHAAIgiAAQgGAAgFADQgFAEAAAGQAAAGAEAEQADAEAGAAQAGAAAEgDQADgCAAgFIAAgHg");
	this.shape_5.setTransform(0.6,-7.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgQAgQgJgGABgRQAAgLAJgLQAHgIAIgDIgZAAIAAgOIAwAAIAAANQgNAIgHAGQgIAKgBAJIABAKQACAGAFgBQAEAAACgDQACgDAAgDIAAgEIAQAAIAAAHQAAAKgGAGQgHAFgJABQgNgBgHgGg");
	this.shape_6.setTransform(-13.3,-7.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgUAyQgJgEgGgHQgJgMAAgOQAAgHACgGQAGgSAVgIIAJgFIAKgJQAEgEAEgJIATAAQgCAIgEAGQgHAJgIAEIAGABQAOADAIAMQAHAKAAANQAAAKgEAJQgFAKgMAGQgLAGgNAAQgKAAgKgEgAgUgDQgGAGAAAJQAAALAIAHQAHAGAMAAQAOAAAHgJQAFgHAAgIQAAgLgIgHQgHgGgMAAQgNAAgHAJg");
	this.shape_7.setTransform(-20.9,-9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgSAPQgHgGAAgJQgCgSARgCIAAAOQAAAGADADQAEAEAFAAQAEAAADgDQACgDAAgCIAAgEIAPAAIABAFQAAAIgIAHQgHAGgKAAQgNAAgHgGg");
	this.shape_8.setTransform(-28.1,-3.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgmAlQgKgEgCgJQAAgCABgDQABgBAAgBQAAgBABAAQAAgBAAAAQAAgBAAAAIgCgBIgEACIgCADIAAgOIAOgOIANgLIAAgCQAAAAAAgBQAAAAAAAAQAAgBgBAAQAAAAAAAAIgEgBQgEABAAAFIABAEIgQAAIAAgFQAAgIAGgFQAFgFAIAAQAOgBADALQAPgKASAAQAQAAALAKQAMAMAAATQAAARgKAKQgKALgRAAIgEgBIAAgNQALAAAGgGQAHgHAAgLQAAgMgHgHQgHgHgNAAQgHAAgFACQAIADAHAIQAGAIAAAKQAAAPgMAJQgKAJgQAAQgMAAgJgDgAgZgEIgLAJQACACACADIAAACIAAACIgBADQAAAEAFACQAEACAFAAQAIAAAHgEQAHgFAAgHQABgIgEgFQgFgFgHAAIgDAAQgEAAgGAFg");
	this.shape_9.setTransform(-31.6,-7.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAZAOQABgHgJgEQgHgDgKAAQgJAAgHADQgJADAAAGIABACIgRAAIgBgGQAAgKAOgGQANgFAPAAQARAAAMAEQAOAHAAAJIgCAHg");
	this.shape_10.setTransform(-42.9,-13);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AggApQgOgLgCgTIgBgGQAAgWAOgHQAAgMAHgHQAHgHALAAQAKAAAFAHQAGgIAMABQAKAAAHAKQAFAKAAAMIAAADIg5AAIgHABQABAKAHAFQAHAGAKAAQAPAAAGgOIARAAQgEANgKAIQgLAIgPAAQgMAAgKgGQgKgHgEgKIAAADQAAAPAKAKQAKAJAOAAQALAAAIgFQAJgFADgKIARAAQgCARgOAJQgNAKgTAAQgUAAgOgLgAALgcIASAAQgCgJgHAAQgHAAgCAJgAgTgcIAQAAQgBgJgHAAQgHAAgBAJg");
	this.shape_11.setTransform(-42.6,-6.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAZAOQABgHgJgEQgHgDgKAAQgJAAgHADQgJADAAAGIABACIgRAAIgBgGQAAgKAOgGQANgFAPAAQARAAAMAEQAOAHAAAJIgCAHg");
	this.shape_12.setTransform(-53.3,-13);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgiAhQgPgJAAgQQAAgKAIgIQAJgHANAAIAMAAQAAgIgLAAQgMAAACAHIgRAAIAAgDQgBgJAJgFQAHgFALAAQAOAAAGAIQAEgEAFgCQAEgCAKAAQAMAAAFAHQAEAEAAAHIAAAEIgSAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAgBAAAAQgCgDgGAAQgLAAAAAIIALAAQAOAAAGAFQALAJAAAMQAAAQgPAIQgOAIgVAAQgWAAgMgIgAgbAAQgEACgBAFQAAAJAKAFQAJAEANAAQAOAAAJgEQAJgFAAgIQAAgGgEgCQgFgDgGAAIghAAQgGAAgFADg");
	this.shape_13.setTransform(-53.2,-7.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#F44336").s().p("Ai4hxIFxAAIi5Djg");
	this.shape_14.setTransform(0,15.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F44336").s().p("As2C8QhNABg4g4Qg3g4AAhNQAAhNA3g3QA4g4BNAAIZtAAQBOAAA3A4QA3A3AABNQAABNg3A4Qg3A4hOgBg");
	this.shape_15.setTransform(0,-7.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.instance}]},21).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({y:6.9,alpha:0.25},0).wait(1).to({y:6.1,alpha:0.5},0).wait(1).to({y:5.3,alpha:0.75},0).wait(1).to({y:4.4,alpha:1},0).to({_off:true},1).wait(21).to({_off:false},0).wait(1).to({y:5.3,alpha:0.75},0).wait(1).to({y:6.1,alpha:0.5},0).wait(1).to({y:6.9,alpha:0.25},0).wait(1).to({y:7.7,alpha:0},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-101.1,-19,202.3,53.4);


// stage content:
(lib.slide_15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		createjs.Touch.enable(stage);
		var checkcount = 7; 
		var loopcount = checkcount +1;
		this.enterans.visible = false;
		this.enterans.stop();
		this.answer.mouseEnabled = false;
		this.confirm.mouseEnabled = false;
		createjs.Touch.enable(stage);
			setTimeout(function (){
				$("#text1").prop("disabled", true);
				$("#text2").prop("disabled", true);
				$("#text3").prop("disabled", true);
				$("#text4").prop("disabled", true);
				$("#text5").prop("disabled", true);
				$("#text6").prop("disabled", true);
				$("#text7").prop("disabled", true);
		},0.00001);
		
		
		
		var aud1 = createjs.Sound.play("audio");
		
		aud1.on("complete", audComplete.bind(this));
		
		
		
		function audComplete(event) {
				$("#text1").prop("disabled", false);
				$("#text2").prop("disabled", false);
				$("#text3").prop("disabled", false);
				$("#text4").prop("disabled", false);
				$("#text5").prop("disabled", false);
				$("#text6").prop("disabled", false);
				$("#text7").prop("disabled", false);
				this.confirm.mouseEnabled = true;
		}
		
		
		var clip = this; 
		
		
		for(var i=1 ; i<loopcount; i++)
		{
		
		
			var obj = clip["correct"+i];
			obj.visible = false;
			
		}
		
		
		
		var txt =['6','2','3','4','7','5','1']; //add the answers here |Sanka|
		
		
		
		this.confirm.addEventListener("click", fl_ClickToPosition.bind(this));
		
		
		function fl_ClickToPosition()
		{ 
		
		  var cc = 0;
			for(var at=1; at<loopcount ; at++  )//numbers of text fields *Sanka*
			{
				if ($("#text"+at).val() =='')
				{
					cc = cc -1;
				}
				  else
				  {
					  cc = cc + 1;
				  }
				  
			}
			
			
			if (cc == checkcount )//numbers of text fields *Sanka*
			{
				$("#text1").prop("disabled", true);
				$("#text2").prop("disabled", true);
				$("#text3").prop("disabled", true);
				$("#text4").prop("disabled", true);
				$("#text5").prop("disabled", true);
				$("#text6").prop("disabled", true);
				$("#text7").prop("disabled", true);
				
				this.confirm.mouseEnabled = false;
				this.answer.mouseEnabled = true;
				//checking all the answers are correct or wrong *Sanka* 
				var c = 0;
				for(var a = 1; a<loopcount; a++ )
				{
		
					var obj = clip["correct"+a];
					obj.visible = true;
		
					if($("#text"+a).val() == txt[c]) 
					{
						obj.gotoAndStop(2);
					}else
					{
						obj.gotoAndStop(3);
		
					}
		
					c = c+1;
					
					
				}
			
			}else
			{
				this.enterans.visible = true;
				this.enterans.play();
				
			}
		}
		
		
		this.answer.addEventListener("click", answerbtn.bind(this));
		
		
		function answerbtn()
		{
			this.answer.mouseEnabled = false;
			//this.answer.visible=false;
			
			for(var a = 1; a<loopcount; a++ )
				{
					$("#text"+a).val(txt[a-1]);
				}
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// correct
	this.correct7 = new lib.correct();
	this.correct7.parent = this;
	this.correct7.setTransform(1188,561.7);

	this.correct6 = new lib.correct();
	this.correct6.parent = this;
	this.correct6.setTransform(1188,499.1);

	this.correct5 = new lib.correct();
	this.correct5.parent = this;
	this.correct5.setTransform(1188,437.1);

	this.correct4 = new lib.correct();
	this.correct4.parent = this;
	this.correct4.setTransform(1188,374.3);

	this.correct3 = new lib.correct();
	this.correct3.parent = this;
	this.correct3.setTransform(1188,312.5);

	this.correct2 = new lib.correct();
	this.correct2.parent = this;
	this.correct2.setTransform(1188.2,250.7);

	this.correct1 = new lib.correct();
	this.correct1.parent = this;
	this.correct1.setTransform(1188.2,188.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.correct1},{t:this.correct2},{t:this.correct3},{t:this.correct4},{t:this.correct5},{t:this.correct6},{t:this.correct7}]}).wait(1));

	// btn
	this.enterans = new lib.enterans();
	this.enterans.parent = this;
	this.enterans.setTransform(793.4,607.1);

	this.answer = new lib.answer();
	this.answer.parent = this;
	this.answer.setTransform(1005.5,667.2);
	new cjs.ButtonHelper(this.answer, 0, 1, 2);

	this.confirm = new lib.confirm();
	this.confirm.parent = this;
	this.confirm.setTransform(795.5,667.2);
	new cjs.ButtonHelper(this.confirm, 0, 1, 2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.confirm},{t:this.answer},{t:this.enterans}]}).wait(1));

	// text
	this.text7 = new lib.an_TextInput({'id': 'text7', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.text7.setTransform(1090.5,562.2,0.9,1.382,0,0,0,50,11);

	this.text6 = new lib.an_TextInput({'id': 'text6', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.text6.setTransform(1090.5,500.2,0.9,1.382,0,0,0,50,11);

	this.text5 = new lib.an_TextInput({'id': 'text5', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.text5.setTransform(1090.5,438.2,0.9,1.382,0,0,0,50,11);

	this.text4 = new lib.an_TextInput({'id': 'text4', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.text4.setTransform(1090.5,375.2,0.9,1.382,0,0,0,50,11);

	this.text3 = new lib.an_TextInput({'id': 'text3', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.text3.setTransform(1090.5,313.2,0.9,1.382,0,0,0,50,11);

	this.text2 = new lib.an_TextInput({'id': 'text2', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.text2.setTransform(1090.5,250.3,0.9,1.382,0,0,0,50,11);

	this.text1 = new lib.an_TextInput({'id': 'text1', 'value':'', 'disabled':false, 'visible':true, 'class':'ui-textinput'});

	this.text1.setTransform(1090.5,189.3,0.9,1.382,0,0,0,50,11);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text1},{t:this.text2},{t:this.text3},{t:this.text4},{t:this.text5},{t:this.text6},{t:this.text7}]}).wait(1));

	// bg
	this.instance = new lib.an_CSS({'id': '', 'href':'assets/style.css'});

	this.instance.setTransform(-119.3,442.4,1,1,0,0,0,50,11);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#0097A7").ss(3).p("AG4C5ItvAAQg2AAgmgmQgmgmAAg2IAAhtQAAg1AmgnQAmgmA2AAINvAAQA2AAAmAmQAmAnAAA1IAABtQAAA2gmAmQgmAmg2AAg");
	this.shape.setTransform(1091,562.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#05686D").s().p("Am3C5Qg2AAgmgmQgmgmAAg2IAAhtQAAg1AmgnQAmgmA2AAINvAAQA2AAAmAmQAmAnAAA1IAABtQAAA2gmAmQgmAmg2AAg");
	this.shape_1.setTransform(1091,562.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgPAOIAAgbIAfAAIAAAbg");
	this.shape_2.setTransform(678.9,567.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgVBOIAAgTQAAghAMgaQALgbAXgaIhPAAIAAgYIBtAAIAAAYQgaAfgKAYQgKAZAAAgIAAATg");
	this.shape_3.setTransform(669.4,561.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AAAAyQgKANgRAAQgYAAgMgSQgLgQABgcQAAgeAUgSQAOgNASgBQAIgBAGACIAAAOQgJgDgMAEQgMAEgIALQgHAMAAAOQAAAlAaAAQAJAAAGgFQAHgGAAgJIAAgNIAOAAIAAAMQAAAKAHAGQAGAFAKAAQALAAAIgIQAHgHAAgMQAAgKgIgIQgHgHgMAAIgkAAQAAgWAJgLQAJgKASABQAhAAAAAaIgBAGIgMAAQAMAFAHALQAHALAAAMQAAAXgMAPQgMAPgUAAQgWAAgJgNgAAKggIAWAAIAJAAIAAgEQAAgLgOAAQgPAAgCAPg");
	this.shape_4.setTransform(810.4,564.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("Ag6BJQgQgGgGAAQgGAAgHAJIgLgHIAWghQgKgMAAgTQAAgHACgGQAFgUASgKQANgIASAAIAJAAIAVgfIAQAAIgVAgIALAFIAJAHQADgFADgDQAHgFAJAAQALAAAHAIQAJgJAMABQAhABACA5QAAAbgJASQgMAVgXAAQgQAAgIgIQgJgJAAgOIABgHIAOAAIAAAEQAAASAQAAQAKAAAEgQQACgJABgVQAAgQgCgMQgDgQgHAAQgKAAAAAOIAAAeIgPAAIAAgeQAAgOgJgBQgEgBgDAEQgDACAAAEQAMAOgBAXQgBAcgQAQQgQAQgdABIgBAAQgLAAgPgFgAg9A5QAMAFALAAIALgBQAPgDAKgLQAIgMAAgQQAAgPgJgKQgKgMgPgBIgOATIAIAAQAJAAAGAEQAGAGABAIQABALgFAHQgFAIgKADQgHADgJAAQgOAAgKgGIgIALIAGAAQAGAAAGACgAg+AfQACADAFABQAKACADgCQgIgCgDgFQgCgFABgFgAgpAUQAAAHAHAAQAHAAAAgHQAAgHgHAAQgHAAAAAHgAhEgFQgFAGAAAKQAAAGABAEIAbgnQgQACgHALg");
	this.shape_5.setTransform(792,562.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgEBPQgbgBgRgQQgQgRAAgaQgBgRAHgMQAGgMAOgNQAKgIASgSQAJgLADgHIAZAAQgEAJgIAJQgHAIgLAIQARgBANAFQAmAOAAAsQAAAbgUASQgTASgaAAIgEgBgAgjgRQgKAMAAAPQAAAPAKAKQANAOAWAAQARAAAMgJQAQgNAAgTQAAgIgDgIQgFgLgLgHQgLgGgOAAQgXAAgNAPg");
	this.shape_6.setTransform(775.1,562.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AAoAdQAKgHAAgKIAAgDQgCgHgQgEQgNgDgTAAQgRAAgOAEQgQADgDAHIAAAEQAAAKAJAGIgNAAQgMgEAAgRQAAgGADgDQAHgQATgGQAOgFAXAAQA1AAALAbQACADAAAGQAAAQgLAFg");
	this.shape_7.setTransform(760.4,556.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_8.setTransform(759.6,564.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgcA/QgVgBgOgOQgOgOgBgUQgBgSAGgNIAPAAQgBAFACACQABACAFAAQAHAAAGgEQALgHADgJIABgFQAAgKgHgDQgGgCgFACQAJADAAALQAAAGgFAEQgEAEgHAAIgGgBQgLgDAAgOIACgJQAGgRAXAAQAJAAAHAFQAHAEACAHQAMgRAaABQAbACAOAZQAJARAAATQgBAqgeAPQgKAFgLAAIgOgCIAAgKQgKAHgIADQgHACgLAAIgFAAgAAOgPQAOALAAATQAAATgMANIAKABQALAAAGgEQAPgMAAgYQAAgQgLgMQgKgMgQgBQgTgBgJAMIACAAQALAAAIAHgAg+AJQAAAKAGAGQAHAKASAAIAKAAQAMgCAIgIQAGgIAAgKQAAgGgDgDQgDgHgKgBQgFAAgIAFIgNAHQgGADgIAAQgGAAgEgDIgBAHg");
	this.shape_9.setTransform(735.2,564.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AhIAkQgEgMADgJIgLAAIAAgMIAJgJIALgMQgIAAgEgFQgEgFAAgIQAAgLAJgHQAIgHAMgBQATgBAJAOQARgNAaAAIAJAAQAZACAQATQAQAUgBAYQgCAZgQARQgQASgZAAIgJgBIAAgMQgQAMgYABIgDAAQgmAAgIgbgAgHgkQAHABAHAFQAIAEAFAFQAMANAAAQQABAYgNAPIAFABQALAAAIgFQAQgLACgVQABgNgHgNQgGgMgNgGQgLgFgQAAQgKAAgHACgAgrgQQgJAHgOANIAKAAQgEAJACAJQAGARAagBQAOgBALgJQAJgJABgNQABgLgJgJQgHgJgNAAIgCAAQgMAAgKAHgAg0gvIgGADQAGABACADQACAFgBAFIAHgGIAGgFQgDgEgGgCIgDAAIgEAAg");
	this.shape_10.setTransform(718.3,564.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("Ag3AtQgRgOAAgaQAAgKAEgJQAFgJAHgFQgHgDAAgKIABgGQAGgPAbAAQAVAAAIAMQAJgMAXAAQALAAAIAFQAKAGABAJQABAIgEAGQAGAFAEAJQAEAKAAAJQAAAagSAPQgVARgiAAQgjAAgUgSgAgygNQgGAGABAKQABAQATAIQAOAFAVAAQAUAAAPgFQATgIAAgQQABgKgGgGQgGgHgJAAIhEAAQgKAAgGAHgAAQgsQgHAEgBAHIAjAAIAAgEQAAgEgEgDQgFgEgFAAIgBAAQgHAAgFAEgAgngvQAEACABAEQAAAFgCADIAaAAQgBgMgPgDIgGAAQgEAAgDABg");
	this.shape_11.setTransform(701.8,564.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("EgmqAD/Qg2AAgmgmQgmgnAAg1IAAj5QAAg2AmgmQAmgmA2AAMBNVAAAQA1AAAnAmQAmAmAAA2IAAD5QAAA1gmAnQgnAmg1AAg");
	this.shape_12.setTransform(900.5,562.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#0097A7").ss(3).p("AG4C5ItvAAQg2AAgmgmQgmgmAAg2IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA2gmAmQgmAmg2AAg");
	this.shape_13.setTransform(1091,500);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#05686D").s().p("Am3C5Qg2AAgmgmQgmgmAAg2IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA2gmAmQgmAmg2AAg");
	this.shape_14.setTransform(1091,500);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgPAOIAAgbIAfAAIAAAbg");
	this.shape_15.setTransform(678.9,505.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgmA/QgPgRAAgdIAAgfQAAgeARgSQASgRAagBQAJABAIABQAHABAIAEIgGAXIgNgEIgNgBQgNAAgIAKQgJALAAATIAAAFQAGgGAJgDQAIgEAKAAQAUAAAMAOQALANAAAXQAAAYgPAOQgOAQgYgBQgXAAgQgRgAgMACQgGADgEAFIAAAIQAAATAHAJQAGAKAKAAQAKAAAGgIQAGgIAAgNQAAgNgGgHQgGgHgKAAQgIAAgFACg");
	this.shape_16.setTransform(669.6,499.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AAAAyQgKANgRAAQgYAAgMgSQgLgQABgcQAAgeAUgSQAOgNASgBQAIgBAGACIAAAOQgJgDgMAEQgMAEgIALQgHAMAAAOQAAAlAaAAQAJAAAGgFQAHgGAAgJIAAgNIAOAAIAAAMQAAAKAHAGQAGAFAKAAQALAAAIgIQAHgHAAgMQAAgKgIgIQgHgHgMAAIgkAAQAAgWAJgLQAJgKASABQAhAAAAAaIgBAGIgMAAQAMAFAHALQAHALAAAMQAAAXgMAPQgMAPgUAAQgWAAgJgNgAAKggIAWAAIAJAAIAAgEQAAgLgOAAQgPAAgCAPg");
	this.shape_17.setTransform(829.5,502);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("Ag6BJQgQgGgGAAQgGAAgHAJIgLgHIAWghQgKgMAAgTQAAgHACgGQAFgUASgKQANgIASAAIAJAAIAVgfIAQAAIgVAgIALAFIAJAHQADgFADgDQAHgFAJAAQALAAAHAIQAJgJAMABQAhABACA5QAAAbgJASQgMAVgXAAQgQAAgIgIQgJgJAAgOIABgHIAOAAIAAAEQAAASAQAAQAKAAAEgQQACgJABgVQAAgQgCgMQgDgQgHAAQgKAAAAAOIAAAeIgPAAIAAgeQAAgOgJgBQgEgBgDAEQgDACAAAEQAMAOgBAXQgBAcgQAQQgQAQgdABIgBAAQgLAAgPgFgAg9A5QAMAFALAAIALgBQAPgDAKgLQAIgMAAgQQAAgPgJgKQgKgMgPgBIgOATIAIAAQAJAAAGAEQAGAGABAIQABALgFAHQgFAIgKADQgHADgJAAQgOAAgKgGIgIALIAGAAQAGAAAGACgAg+AfQACADAFABQAKACADgCQgIgCgDgFQgCgFABgFgAgpAUQAAAHAHAAQAHAAAAgHQAAgHgHAAQgHAAAAAHgAhEgFQgFAGAAAKQAAAGABAEIAbgnQgQACgHALg");
	this.shape_18.setTransform(811,500.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgQAeIAAgZIgLAAIAAgLIALAAIAAgQIgLAAIAAgMIAgAAQAKAAAGAGQAHAGAAAJQAAAKgIAEQgJAEgMAAIAAAegAgBgWIAAAQQALAAAAgIQAAgIgJAAIgCAAg");
	this.shape_19.setTransform(798,493.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgcA6IAAgOQAEADAIAAQAIAAAFgOQAFgOAAgRQAAgRgFgOQgFgQgHAAQgIgBgFAEIAAgPQAHgFAMAAQARAAALAWQAKATAAAXQAAAXgKASQgMAUgQAAQgLAAgIgFg");
	this.shape_20.setTransform(797.6,502);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_21.setTransform(786.4,502);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgnAtQgSgSgBgaQABgcASgRQARgSAaAAQAdAAANAMQAJAJABAMIgPAAQAAgJgLgFQgLgEgNAAQgSAAgMALQgOANgBARQAAARANALQANAKARAAQANAAALgHQAMgIAAgOQgBgRgQgEQAHAFAAAMQAAAHgIAGQgHAHgKAAQgLAAgHgHQgHgHgBgLQAAgMAKgIQAJgGANAAQAUAAANALQAOALAAASQAAAYgQAPQgQAQgXAAQgZAAgRgSgAgDgOQgCADAAAEQAAAFACACQADADAEAAQAFAAADgDQACgCAAgEQAAgFgCgDQgDgDgEAAQgFAAgDADg");
	this.shape_22.setTransform(771.8,502);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AhIAkQgEgMADgJIgLAAIAAgMIAJgJIALgMQgIAAgEgFQgEgFAAgIQAAgLAJgHQAIgHAMgBQATgBAJAOQARgNAaAAIAJAAQAZACAQATQAQAUgBAYQgCAZgQARQgQASgZAAIgJgBIAAgMQgQAMgYABIgDAAQgmAAgIgbgAgHgkQAHABAHAFQAIAEAFAFQAMANAAAQQABAYgNAPIAFABQALAAAIgFQAQgLACgVQABgNgHgNQgGgMgNgGQgLgFgQAAQgKAAgHACgAgrgQQgJAHgOANIAKAAQgEAJACAJQAGARAagBQAOgBALgJQAJgJABgNQABgLgJgJQgHgJgNAAIgCAAQgMAAgKAHgAg0gvIgGADQAGABACADQACAFgBAFIAHgGIAGgFQgDgEgGgCIgDAAIgEAAg");
	this.shape_23.setTransform(749.2,502);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgbA+QgZAAgNgSQgNgRAAgZQAAgpAdgRQAJgGAQAAIAIABIAAAOQARgPAXAAQAaABAOARQAOAQAAAbQABAagPATQgQASgZAAIgJAAIAAgMQgKAHgGADQgIADgMAAIgFgBgAAJgUQAWAGgBAaQAAAUgNAPQANACAKgGQAQgLgBgaQgBgPgKgMQgKgMgPgBQgcgCgEATQAGgEAJAAIAHABgAgygjQgJAMAAAPQAAARAJALQAKAMARAAQANABAKgGQAJgGABgLQABgGgEgEQgEgGgGABQgFAAgDADQgDADAAAFIABAGIgPAAQgDgPACgOQABgOALgPIgJgBQgPAAgJAMg");
	this.shape_24.setTransform(732.3,502);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgCBUQgbgBgQgQQgRgRAAgaQAAgRAGgMIACgEIAAgjIgLAAIAAgMIALAAIAAgRIgMAAIAAgLIAhAAQAKAAAGAFQAIAGAAAKQAAAJgJAGQgIAFgOgBIAAASQgJAIgFAJQAGgKANgLIAbgaQAJgLADgHIAZAAQgFAJgGAJQgIAIgLAIQARgBANAFQAmAOAAAsQAAAbgUASQgTASgbAAIgDgBgAghgMQgLAMAAAPQAAAPAKAKQAOAOAVAAQARAAANgJQAQgNAAgTQAAgIgDgIQgFgLgLgHQgMgGgNAAQgXAAgNAPgAgohIIAAAQQANAAAAgIQAAgIgLAAIgCAAg");
	this.shape_25.setTransform(717.3,499.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AApAdQAKgHAAgKIgBgDQgCgHgPgEQgPgDgSAAQgSAAgNAEQgQADgCAHIgBAEQAAAKAKAGIgOAAQgLgEAAgRQAAgGABgDQAIgQASgGQAOgFAYAAQA2AAAKAbQADADAAAGQAAAQgMAFg");
	this.shape_26.setTransform(702.7,494.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgcA/QgVgBgOgOQgOgOgBgUQgBgSAGgNIAPAAQgBAFACACQABACAFAAQAHAAAGgEQALgHADgJIABgFQAAgKgHgDQgGgCgFACQAJADAAALQAAAGgFAEQgEAEgHAAIgGgBQgLgDAAgOIACgJQAGgRAXAAQAJAAAHAFQAHAEACAHQAMgRAaABQAbACAOAZQAJARAAATQgBAqgeAPQgKAFgLAAIgOgCIAAgKQgKAHgIADQgHACgLAAIgFAAgAAOgPQAOALAAATQAAATgMANIAKABQALAAAGgEQAPgMAAgYQAAgQgLgMQgKgMgQgBQgTgBgJAMIACAAQALAAAIAHgAg+AJQAAAKAGAGQAHAKASAAIAKAAQAMgCAIgIQAGgIAAgKQAAgGgDgDQgDgHgKgBQgFAAgIAFIgNAHQgGADgIAAQgGAAgEgDIgBAHg");
	this.shape_27.setTransform(702.3,502);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("EgmqAD/Qg2AAgmgmQgmgmAAg2IAAj5QAAg2AmgmQAmgmA2AAMBNVAAAQA1AAAnAmQAmAmAAA2IAAD5QAAA2gmAmQgnAmg1AAg");
	this.shape_28.setTransform(900.5,500.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#0097A7").ss(3).p("AG4C5ItvAAQg2AAgmgmQgmgmAAg2IAAhtQAAg1AmgnQAmgmA2AAINvAAQA2AAAmAmQAmAnAAA1IAABtQAAA2gmAmQgmAmg2AAg");
	this.shape_29.setTransform(1091,437.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#05686D").s().p("Am3C5Qg2AAgmgmQgmgnAAg1IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA1gmAnQgmAmg2AAg");
	this.shape_30.setTransform(1091,437.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgPAOIAAgbIAfAAIAAAbg");
	this.shape_31.setTransform(678.9,443.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgkBEQgQgMABgTIAAgBIAegCQAAAKAGAFQAGAGAJAAQALAAAFgHQAFgHAAgOQAAgNgGgJQgFgGgKgBQgJABgFACQgFADgCAGIgbgCIAJhWIBWAAIAAAYIg9AAIgEAmQAEgEAHgCQAHgCAHAAQAVAAAMANQAMANAAAZQAAAXgNAPQgNAOgaAAQgVAAgPgLg");
	this.shape_32.setTransform(669.6,437);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AAAAyQgKANgRAAQgYAAgMgSQgLgQABgcQAAgeAUgSQAOgNASgBQAIgBAGACIAAAOQgJgDgMAEQgMAEgIALQgHAMAAAOQAAAlAaAAQAJAAAGgFQAHgGAAgJIAAgNIAOAAIAAAMQAAAKAHAGQAGAFAKAAQALAAAIgIQAHgHAAgMQAAgKgIgIQgHgHgMAAIgkAAQAAgWAJgLQAJgKASABQAhAAAAAaIgBAGIgMAAQAMAFAHALQAHALAAAMQAAAXgMAPQgMAPgUAAQgWAAgJgNgAAKggIAWAAIAJAAIAAgEQAAgLgOAAQgPAAgCAPg");
	this.shape_33.setTransform(779.6,439.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AACBNQgIgEgHgJQgCAHgGAFQgIAEgKAAQgRAAgKgSQgJgQAAgXQAAgZAOgUQAQgVAbAAQASAAAMAMQAMAMAAASQAAAOgLAKIgGAFQgDAEgBAEQAAAOAUAAQAPAAAJgPQAIgPAAgUQAAgZgPgUQgQgTgeAAQgPAAgNAGQgOAHgGAMIgRAAQAHgTASgLQARgKAWAAQAlAAAVAVQAKAKAHARQAHASABAOQABAlgRAWQgOAUgXAAQgNAAgIgDgAgsAwQgCADAAAEQAAADACADQADADAEAAQAEAAADgDQACgDAAgDQAAgEgDgDQgCgDgEAAQgEAAgDADgAg2AEQgFAKAAALQAAAJADAIQAGgMAPgBQALgBAJAIQABgIAHgHQAJgIgBgKQAAgIgGgFQgGgGgJAAQgZAAgJAUg");
	this.shape_34.setTransform(764.1,438);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AgxBDQgTgTgBgdQAAgUAJgOQAKgQAPAAQARAAACANQAGgPAQAAQANAAAIALQAJAKAAANQAAAQgLALQgLALgOAAQgNAAgKgIQgLgHAAgMIAEgTQACgIgJAAQgGAAgEAJQgEAHAAAJQAAAVAQANQAOALAVAAQAVAAAPgOQAPgPAAgWQAAgQgHgMQgJgNgPAAIhUAAIAAgNQAXABgBgCQAAgBgEgCQgFgCAAgFQAAgQASgFQAKgDAWAAQASAAAMAEQAQAEAKAMQAIAJAAAOIgNAAIgBgBIAAABIAAAAIABAAQAUAQAAAlQAAAggUAWQgUAVgeAAQgdAAgUgTgAgRADQAAAGAGAFQAFAGAGAAQAIAAAHgFQAFgGAAgIQgFAJgLAAQgNABgHgNIgBAFgAAAgRQgCACAAAEQAAADACACQABACAEAAQACAAADgCQADgCAAgDQAAgEgDgCQgCgCgDAAQgEAAgBACgAAxgrQgCgMgPgIQgNgGgSAAQgNAAgGACQgJADAAAHQAAADACACQADABADAAIAmAAQANAAAKAEIAHAEIAAAAgAAxgqIAAAAIAAgBIABABgAAxgrIAAAAgAAqgvQgKgEgNAAIgmAAQgDAAgDgBQgCgCAAgDQAAgHAJgDQAGgCANAAQASAAANAGQAPAIACAMIgHgEg");
	this.shape_35.setTransform(749,437.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("Ag0A3QgUgYABghQAAghAVgWQAVgVAiAAQAdAAATATQAUATAAAdQAAAXgNAQQgOASgWAAQgWABgMgOQgOgOACgUQACgSAOgLIgRAAIAAgOIA4AAIAAAOIgKAAQgKAAgIAHQgGAGgBAKQAAAKAHAHQAIAGALAAQAOAAAJgLQAHgKAAgOQgBgSgJgLQgNgQgbAAQgXAAgOAUQgLARAAAZQAAAXALARQANATAUADIALABQAaAAAPgVIARAAQgSAngxAAQghAAgVgYg");
	this.shape_36.setTransform(734.1,441.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("AApAdQAJgHABgKIgBgDQgCgHgPgEQgOgDgTAAQgSAAgNAEQgQADgDAHIAAAEQAAAKAJAGIgNAAQgLgEAAgRQAAgGACgDQAHgQATgGQAOgFAXAAQA1AAALAbQACADAAAGQABAQgMAFg");
	this.shape_37.setTransform(718.7,431.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("AAVA+IAAgPIAHABQAOAAAKgMQALgNAAgPQAAgPgIgMQgJgNgOgCIgHgBQgZAAgDAXQACgCAFgBQAGgBAEACQAKADAEAJQAFAJgBAMQgCATgPAMQgPANgUAAQgdAAgQgSQgLgMAAgQQAAgJAFgIQAEgKAIgEIgQAAQgEAAgDACIAAgPIARAAQgGgCAAgLIABgHQAHgPAWAAQANAAAIAFQAJAGACALQAMgVAcgBQAYAAAPAUQAOAUgBAYQgBAZgQARQgRATgWAAIgGgBgAgwgIQgHAGgBAIQAAAMAKAHQAJAHARAAQAOAAAJgFQAMgGAAgNQAAgJgJgBQgFAAgCACQgEADABAFIgOAAIAAgMIABgKIgOAAQgKAAgHAGgAgmgmQAAAGgDAEIATAAQgBgJgFgFQgEgEgIAAIgDAAQAEACABAGg");
	this.shape_38.setTransform(718.1,439.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#000000").s().p("AAVBMIAAgcQgSACgOAAQgcgCgRgMQgTgOAAgZQAAgMAHgLQAHgJAJgEQgFgCgCgEQgCgEAAgDQAAgIAHgFQAMgKAWAAQAiAAAJAUQACgJAHgGQAIgFAJAAIAOAAIAAAOIgCgBQgFABgBAEQAAACAFAEQAHAGADADQAGAGAAAJQABAPgOAHQgLAGgPgBIAAAgIANgEIAJgDIAAANIgJAFIgNACIAAAfgAgigWQgIAKAAAMQABAaAeAIIAPABQALAAAGgBIAAhCIgiAAQgNAAgIAKgAAlglIAAAYIADAAQAGAAAEgEQAEgDAAgGQAAgHgGgIQgGgJAAgEIAAgCQgFABAAASgAAJgtQgCgJgLgFQgNgDgMADQADADAAAEQAAAFgDACIADAAIADAAIAgAAIAAAAg");
	this.shape_39.setTransform(702,441.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("EgmqAD/Qg2AAgmgmQgmgnAAg1IAAj5QAAg2AmgmQAmgmA2AAMBNVAAAQA1AAAnAmQAmAmAAA2IAAD5QAAA1gmAnQgnAmg1AAg");
	this.shape_40.setTransform(900.5,438);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#0097A7").ss(3).p("AG4C5ItvAAQg2AAgmgmQgmgmAAg2IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA2gmAmQgmAmg2AAg");
	this.shape_41.setTransform(1091,375.4);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#05686D").s().p("Am3C5Qg2AAgmgmQgmgmAAg2IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA2gmAmQgmAmg2AAg");
	this.shape_42.setTransform(1091,375.4);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#000000").s().p("AgPAOIAAgbIAfAAIAAAbg");
	this.shape_43.setTransform(678.9,381);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#000000").s().p("AAIBOIAAgiIg+AAIgCgSIBAhnIAfAAIAABhIASAAIAAAYIgSAAIAAAigAAEgdIgfAxIAjAAIAAg4IAAAAg");
	this.shape_44.setTransform(669.5,374.5);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#000000").s().p("AAAAyQgKANgRAAQgYAAgMgSQgLgQABgcQAAgeAUgSQAOgNASgBQAIgBAGACIAAAOQgJgDgMAEQgMAEgIALQgHAMAAAOQAAAlAaAAQAJAAAGgFQAHgGAAgJIAAgNIAOAAIAAAMQAAAKAHAGQAGAFAKAAQALAAAIgIQAHgHAAgMQAAgKgIgIQgHgHgMAAIgkAAQAAgWAJgLQAJgKASABQAhAAAAAaIgBAGIgMAAQAMAFAHALQAHALAAAMQAAAXgMAPQgMAPgUAAQgWAAgJgNgAAKggIAWAAIAJAAIAAgEQAAgLgOAAQgPAAgCAPg");
	this.shape_45.setTransform(823.6,377.3);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#000000").s().p("Ag6BJQgQgGgGAAQgGAAgHAJIgLgHIAWghQgKgMAAgTQAAgHACgGQAFgUASgKQANgIASAAIAJAAIAVgfIAQAAIgVAgIALAFIAJAHQADgFADgDQAHgFAJAAQALAAAHAIQAJgJAMABQAhABACA5QAAAbgJASQgMAVgXAAQgQAAgIgIQgJgJAAgOIABgHIAOAAIAAAEQAAASAQAAQAKAAAEgQQACgJABgVQAAgQgCgMQgDgQgHAAQgKAAAAAOIAAAeIgPAAIAAgeQAAgOgJgBQgEgBgDAEQgDACAAAEQAMAOgBAXQgBAcgQAQQgQAQgdABIgBAAQgLAAgPgFgAg9A5QAMAFALAAIALgBQAPgDAKgLQAIgMAAgQQAAgPgJgKQgKgMgPgBIgOATIAIAAQAJAAAGAEQAGAGABAIQABALgFAHQgFAIgKADQgHADgJAAQgOAAgKgGIgIALIAGAAQAGAAAGACgAg+AfQACADAFABQAKACADgCQgIgCgDgFQgCgFABgFgAgpAUQAAAHAHAAQAHAAAAgHQAAgHgHAAQgHAAAAAHgAhEgFQgFAGAAAKQAAAGABAEIAbgnQgQACgHALg");
	this.shape_46.setTransform(805.2,375.8);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#000000").s().p("AgEBPQgbgBgRgQQgQgRAAgaQgBgRAHgMQAGgMAOgNQAKgIASgSQAJgLADgHIAZAAQgEAJgIAJQgHAIgLAIQARgBANAFQAmAOAAAsQAAAbgUASQgTASgaAAIgEgBgAgjgRQgKAMAAAPQAAAPAKAKQANAOAWAAQARAAAMgJQAQgNAAgTQAAgIgDgIQgFgLgLgHQgLgGgOAAQgXAAgNAPg");
	this.shape_47.setTransform(788.3,375.7);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#000000").s().p("AAoAdQALgHgBgKIAAgDQgCgHgQgEQgOgDgSAAQgRAAgOAEQgQADgCAHIgBAEQAAAKAKAGIgOAAQgMgEAAgRQAAgGACgDQAIgQASgGQAOgFAYAAQA2AAALAbQACADAAAGQAAAQgMAFg");
	this.shape_48.setTransform(773.6,369.5);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#000000").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_49.setTransform(772.8,377.4);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#000000").s().p("AgcA/QgVgBgOgOQgOgOgBgUQgBgSAGgNIAPAAQgBAFACACQABACAFAAQAHAAAGgEQALgHADgJIABgFQAAgKgHgDQgGgCgFACQAJADAAALQAAAGgFAEQgEAEgHAAIgGgBQgLgDAAgOIACgJQAGgRAXAAQAJAAAHAFQAHAEACAHQAMgRAaABQAbACAOAZQAJARAAATQgBAqgeAPQgKAFgLAAIgOgCIAAgKQgKAHgIADQgHACgLAAIgFAAgAAOgPQAOALAAATQAAATgMANIAKABQALAAAGgEQAPgMAAgYQAAgQgLgMQgKgMgQgBQgTgBgJAMIACAAQALAAAIAHgAg+AJQAAAKAGAGQAHAKASAAIAKAAQAMgCAIgIQAGgIAAgKQAAgGgDgDQgDgHgKgBQgFAAgIAFIgNAHQgGADgIAAQgGAAgEgDIgBAHg");
	this.shape_50.setTransform(748.4,377.4);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#000000").s().p("AhIAkQgEgMADgJIgLAAIAAgMIAJgJIALgMQgIAAgEgFQgEgFAAgIQAAgLAJgHQAIgHAMgBQATgBAJAOQARgNAaAAIAJAAQAZACAQATQAQAUgBAYQgCAZgQARQgQASgZAAIgJgBIAAgMQgQAMgYABIgDAAQgmAAgIgbgAgHgkQAHABAHAFQAIAEAFAFQAMANAAAQQABAYgNAPIAFABQALAAAIgFQAQgLACgVQABgNgHgNQgGgMgNgGQgLgFgQAAQgKAAgHACgAgrgQQgJAHgOANIAKAAQgEAJACAJQAGARAagBQAOgBALgJQAJgJABgNQABgLgJgJQgHgJgNAAIgCAAQgMAAgKAHgAg0gvIgGADQAGABACADQACAFgBAFIAHgGIAGgFQgDgEgGgCIgDAAIgEAAg");
	this.shape_51.setTransform(731.5,377.4);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#000000").s().p("AgDBUQgbgBgPgQQgRgRAAgaQAAgRAGgMIACgEIAAgjIgMAAIAAgMIAMAAIAAgRIgMAAIAAgLIAhAAQAKAAAHAFQAHAGAAAKQAAAJgJAGQgIAFgOgBIAAASQgJAIgFAJQAGgKANgLIAbgaQAJgLADgHIAZAAQgFAJgGAJQgIAIgLAIQARgBANAFQAmAOAAAsQAAAbgUASQgTASgbAAIgEgBgAghgMQgLAMABAPQgBAPAKAKQAOAOAVAAQASAAAMgJQAQgNAAgTQAAgIgDgIQgFgLgLgHQgLgGgOAAQgXAAgNAPgAgohIIAAAQQANAAAAgIQAAgIgLAAIgCAAg");
	this.shape_52.setTransform(715.8,375.2);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#000000").s().p("AgzBCQgVgPABgbQABgYATgIQgKgCAAgKQAAgOAOgGQAKgFAOAAQARABAKAOQAKAOAAAQIgrAAQgJAAgHAFQgHAFABAKQABAWAdAGQAJACAMAAQAYgBAPgQQAOgOABgXQAAgYgOgSQgPgTgcAAQgggBgRAYIgQAAQAIgSASgJQARgKAWAAQAhAAAVAWQAVAYAAAhQAAAhgTAXQgUAYghAAQggAAgTgOgAgigdQADADABAEQACAFgCAFIAbAAQgCgHgFgFQgGgGgJAAQgFAAgEABg");
	this.shape_53.setTransform(701.6,375.7);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("EgmqAD/Qg2AAgmgmQgmgmAAg2IAAj5QAAg2AmgmQAmgmA2AAMBNVAAAQA1AAAnAmQAmAmAAA2IAAD5QAAA2gmAmQgnAmg1AAg");
	this.shape_54.setTransform(900.5,375.6);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#0097A7").ss(3).p("AG4C5ItvAAQg2AAgmgmQgmgnAAg1IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA1gmAnQgmAmg2AAg");
	this.shape_55.setTransform(1091,313.1);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#05686D").s().p("Am3C5Qg2AAgmgmQgmgnAAg1IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA1gmAnQgmAmg2AAg");
	this.shape_56.setTransform(1091,313.1);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#000000").s().p("AgPAOIAAgbIAfAAIAAAbg");
	this.shape_57.setTransform(678.9,318.6);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#000000").s().p("AgmBEQgQgLABgVIAAgBIAfAAQAAAKAGAGQAGAFAKAAQAKAAAGgGQAHgHAAgJQAAgMgGgHQgHgFgLgBIgRAAIAAgWIARAAQALAAAFgGQAFgGAAgKQAAgJgFgFQgGgHgJAAQgJABgGAFQgFAFAAAIIgfAAIAAgBQgBgSAPgLQAPgNAWAAQAXAAAOAMQAPALAAAVQAAALgGAKQgHAIgKAFQAMAEAHAKQAGAJAAANQAAAVgPANQgQAMgXAAQgVAAgRgMg");
	this.shape_58.setTransform(669.5,312.2);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#000000").s().p("AAAAyQgKANgRAAQgYAAgMgSQgLgQABgcQAAgeAUgSQAOgNASgBQAIgBAGACIAAAOQgJgDgMAEQgMAEgIALQgHAMAAAOQAAAlAaAAQAJAAAGgFQAHgGAAgJIAAgNIAOAAIAAAMQAAAKAHAGQAGAFAKAAQALAAAIgIQAHgHAAgMQAAgKgIgIQgHgHgMAAIgkAAQAAgWAJgLQAJgKASABQAhAAAAAaIgBAGIgMAAQAMAFAHALQAHALAAAMQAAAXgMAPQgMAPgUAAQgWAAgJgNgAAKggIAWAAIAJAAIAAgEQAAgLgOAAQgPAAgCAPg");
	this.shape_59.setTransform(828.2,315);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#000000").s().p("Ag6BJQgQgGgGAAQgGAAgHAJIgLgHIAWghQgKgMAAgTQAAgHACgGQAFgUASgKQANgIASAAIAJAAIAVgfIAQAAIgVAgIALAFIAJAHQADgFADgDQAHgFAJAAQALAAAHAIQAJgJAMABQAhABACA5QAAAbgJASQgMAVgXAAQgQAAgIgIQgJgJAAgOIABgHIAOAAIAAAEQAAASAQAAQAKAAAEgQQACgJABgVQAAgQgCgMQgDgQgHAAQgKAAAAAOIAAAeIgPAAIAAgeQAAgOgJgBQgEgBgDAEQgDACAAAEQAMAOgBAXQgBAcgQAQQgQAQgdABIgBAAQgLAAgPgFgAg9A5QAMAFALAAIALgBQAPgDAKgLQAIgMAAgQQAAgPgJgKQgKgMgPgBIgOATIAIAAQAJAAAGAEQAGAGABAIQABALgFAHQgFAIgKADQgHADgJAAQgOAAgKgGIgIALIAGAAQAGAAAGACgAg+AfQACADAFABQAKACADgCQgIgCgDgFQgCgFABgFgAgpAUQAAAHAHAAQAHAAAAgHQAAgHgHAAQgHAAAAAHgAhEgFQgFAGAAAKQAAAGABAEIAbgnQgQACgHALg");
	this.shape_60.setTransform(809.7,313.5);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#000000").s().p("AgQAeIAAgZIgLAAIAAgLIALAAIAAgQIgLAAIAAgLIAgAAQAKAAAGAEQAHAGAAAKQAAAJgIAFQgJAFgMgBIAAAdgAgBgWIAAAQQALAAAAgHQAAgJgJAAIgCAAg");
	this.shape_61.setTransform(796.7,306.6);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#000000").s().p("AgcA6IAAgOQAEADAIAAQAIAAAFgOQAFgOAAgRQAAgRgFgOQgFgQgHAAQgIgBgFAEIAAgPQAHgFAMAAQARAAALAWQAKATAAAXQAAAXgKASQgMAUgQAAQgLAAgIgFg");
	this.shape_62.setTransform(796.3,315.1);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#000000").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_63.setTransform(785.1,315.1);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#000000").s().p("AgnAtQgTgSABgaQgBgcATgRQARgSAaAAQAdAAANAMQAJAJABAMIgOAAQgBgJgMgFQgKgEgNAAQgRAAgOALQgNANAAARQAAARAMALQAMAKARAAQAOAAALgHQALgIAAgOQAAgRgPgEQAFAFAAAMQABAHgIAGQgIAHgIAAQgLAAgIgHQgIgHABgLQgBgMAKgIQAKgGAMAAQATAAAOALQAPALAAASQgBAYgQAPQgQAQgXAAQgZAAgRgSgAgCgOQgEADAAAEQAAAFAEACQACADAEAAQAEAAADgDQAEgCAAgEQAAgFgEgDQgCgDgEAAQgFAAgCADg");
	this.shape_64.setTransform(770.5,315.1);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#000000").s().p("AgcA/QgVgBgOgOQgOgOgBgUQgBgSAGgNIAPAAQgBAFACACQABACAFAAQAHAAAGgEQALgHADgJIABgFQAAgKgHgDQgGgCgFACQAJADAAALQAAAGgFAEQgEAEgHAAIgGgBQgLgDAAgOIACgJQAGgRAXAAQAJAAAHAFQAHAEACAHQAMgRAaABQAbACAOAZQAJARAAATQgBAqgeAPQgKAFgLAAIgOgCIAAgKQgKAHgIADQgHACgLAAIgFAAgAAOgPQAOALAAATQAAATgMANIAKABQALAAAGgEQAPgMAAgYQAAgQgLgMQgKgMgQgBQgTgBgJAMIACAAQALAAAIAHgAg+AJQAAAKAGAGQAHAKASAAIAKAAQAMgCAIgIQAGgIAAgKQAAgGgDgDQgDgHgKgBQgFAAgIAFIgNAHQgGADgIAAQgGAAgEgDIgBAHg");
	this.shape_65.setTransform(748.4,315.1);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#000000").s().p("AhIAkQgEgMADgJIgLAAIAAgMIAJgJIALgMQgIAAgEgFQgEgFAAgIQAAgLAJgHQAIgHAMgBQATgBAJAOQARgNAaAAIAJAAQAZACAQATQAQAUgBAYQgCAZgQARQgQASgZAAIgJgBIAAgMQgQAMgYABIgDAAQgmAAgIgbgAgHgkQAHABAHAFQAIAEAFAFQAMANAAAQQABAYgNAPIAFABQALAAAIgFQAQgLACgVQABgNgHgNQgGgMgNgGQgLgFgQAAQgKAAgHACgAgrgQQgJAHgOANIAKAAQgEAJACAJQAGARAagBQAOgBALgJQAJgJABgNQABgLgJgJQgHgJgNAAIgCAAQgMAAgKAHgAg0gvIgGADQAGABACADQACAFgBAFIAHgGIAGgFQgDgEgGgCIgDAAIgEAAg");
	this.shape_66.setTransform(731.5,315.1);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#000000").s().p("AgDBUQgbgBgPgQQgRgRAAgaQAAgRAGgMIACgEIAAgjIgMAAIAAgMIAMAAIAAgRIgMAAIAAgLIAhAAQAKAAAHAFQAHAGAAAKQAAAJgJAGQgIAFgOgBIAAASQgJAIgFAJQAGgKANgLIAbgaQAJgLADgHIAZAAQgFAJgGAJQgIAIgLAIQARgBANAFQAmAOAAAsQAAAbgUASQgTASgbAAIgEgBgAghgMQgLAMABAPQgBAPAKAKQAOAOAVAAQASAAAMgJQAQgNAAgTQAAgIgDgIQgFgLgLgHQgLgGgOAAQgXAAgNAPgAgohIIAAAQQANAAAAgIQAAgIgLAAIgCAAg");
	this.shape_67.setTransform(715.8,312.9);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#000000").s().p("AgzBCQgVgPABgbQABgXATgJQgKgCAAgKQAAgOAOgGQAKgFAOAAQARABAKAOQAKANAAARIgrAAQgJAAgHAFQgHAGABAJQABAWAdAGQAJACAMAAQAYAAAPgQQAOgPABgXQAAgYgOgSQgPgTgcAAQgggBgRAYIgQAAQAIgSASgKQARgJAWAAQAhAAAVAXQAVAXAAAhQAAAhgTAWQgUAZghAAQggAAgTgOgAgigcQADACABAEQACAFgCAFIAbAAQgCgHgFgGQgGgFgJAAQgFAAgEACg");
	this.shape_68.setTransform(701.6,313.4);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("EgmqAD/Qg2AAgmgmQgmgnAAg1IAAj5QAAg2AmgmQAmgmA2AAMBNVAAAQA1AAAnAmQAmAmAAA2IAAD5QAAA1gmAnQgnAmg1AAg");
	this.shape_69.setTransform(900.5,313.3);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#0097A7").ss(3).p("AG4C5ItvAAQg2AAgmgmQgmgmAAg2IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA2gmAmQgmAmg2AAg");
	this.shape_70.setTransform(1091,250.7);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#05686D").s().p("Am3C5Qg2AAgmgmQgmgmAAg2IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA2gmAmQgmAmg2AAg");
	this.shape_71.setTransform(1091,250.7);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#000000").s().p("AgPAOIAAgbIAfAAIAAAbg");
	this.shape_72.setTransform(678.9,256.3);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#000000").s().p("AgyBPIAAgUIAxg3QAKgMAEgJQAFgJAAgHQAAgKgFgFQgFgGgJAAQgKAAgFAHQgGAIAAALIgeAAIgBgBQAAgVAOgOQAOgOAYAAQAXAAAOAMQANAMAAAVQAAAOgHAMQgIAKgSAVIgZAfIAAAAIA/AAIAAAYg");
	this.shape_73.setTransform(669.5,249.8);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#000000").s().p("AAAAyQgKANgRAAQgYAAgMgSQgLgQABgcQAAgeAUgSQAOgNASgBQAIgBAGACIAAAOQgJgDgMAEQgMAEgIALQgHAMAAAOQAAAlAaAAQAJAAAGgFQAHgGAAgJIAAgNIAOAAIAAAMQAAAKAHAGQAGAFAKAAQALAAAIgIQAHgHAAgMQAAgKgIgIQgHgHgMAAIgkAAQAAgWAJgLQAJgKASABQAhAAAAAaIgBAGIgMAAQAMAFAHALQAHALAAAMQAAAXgMAPQgMAPgUAAQgWAAgJgNgAAKggIAWAAIAJAAIAAgEQAAgLgOAAQgPAAgCAPg");
	this.shape_74.setTransform(815,252.7);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#000000").s().p("Ag6BJQgQgGgGAAQgGAAgHAJIgLgHIAWghQgKgMAAgTQAAgHACgGQAFgUASgKQANgIASAAIAJAAIAVgfIAQAAIgVAgIALAFIAJAHQADgFADgDQAHgFAJAAQALAAAHAIQAJgJAMABQAhABACA5QAAAbgJASQgMAVgXAAQgQAAgIgIQgJgJAAgOIABgHIAOAAIAAAEQAAASAQAAQAKAAAEgQQACgJABgVQAAgQgCgMQgDgQgHAAQgKAAAAAOIAAAeIgPAAIAAgeQAAgOgJgBQgEgBgDAEQgDACAAAEQAMAOgBAXQgBAcgQAQQgQAQgdABIgBAAQgLAAgPgFgAg9A5QAMAFALAAIALgBQAPgDAKgLQAIgMAAgQQAAgPgJgKQgKgMgPgBIgOATIAIAAQAJAAAGAEQAGAGABAIQABALgFAHQgFAIgKADQgHADgJAAQgOAAgKgGIgIALIAGAAQAGAAAGACgAg+AfQACADAFABQAKACADgCQgIgCgDgFQgCgFABgFgAgpAUQAAAHAHAAQAHAAAAgHQAAgHgHAAQgHAAAAAHgAhEgFQgFAGAAAKQAAAGABAEIAbgnQgQACgHALg");
	this.shape_75.setTransform(796.5,251.2);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#000000").s().p("AgQAeIAAgZIgLAAIAAgLIALAAIAAgQIgLAAIAAgLIAgAAQAKAAAGAEQAHAHAAAKQAAAJgIADQgJAFgMAAIAAAdgAgBgVIAAAPQALAAAAgHQAAgJgJAAIgCABg");
	this.shape_76.setTransform(783.5,244.3);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#000000").s().p("AgcA6IAAgOQAEADAIAAQAIAAAFgOQAFgOAAgRQAAgRgFgOQgFgQgHAAQgIgBgFAEIAAgPQAHgFAMAAQARAAALAWQAKATAAAXQAAAXgKASQgMAUgQAAQgLAAgIgFg");
	this.shape_77.setTransform(783.1,252.8);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#000000").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_78.setTransform(771.9,252.8);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#000000").s().p("AgnAtQgSgSAAgaQAAgcASgRQARgSAaAAQAdAAANAMQAJAJABAMIgPAAQAAgJgLgFQgLgEgNAAQgSAAgMALQgPANAAARQAAARANALQAMAKASAAQANAAALgHQAMgIAAgOQgBgRgQgEQAGAFABAMQAAAHgIAGQgHAHgKAAQgKAAgIgHQgHgHgBgLQAAgMAKgIQAJgGANAAQATAAAOALQAOALABASQAAAYgRAPQgQAQgXAAQgZAAgRgSgAgDgOQgCADAAAEQAAAFACACQADADAEAAQAEAAAEgDQADgCAAgEQAAgFgDgDQgEgDgDAAQgFAAgDADg");
	this.shape_79.setTransform(757.3,252.8);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#000000").s().p("AgcA/QgVgBgOgOQgOgOgBgUQgBgSAGgNIAPAAQgBAFACACQABACAFAAQAHAAAGgEQALgHADgJIABgFQAAgKgHgDQgGgCgFACQAJADAAALQAAAGgFAEQgEAEgHAAIgGgBQgLgDAAgOIACgJQAGgRAXAAQAJAAAHAFQAHAEACAHQAMgRAaABQAbACAOAZQAJARAAATQgBAqgeAPQgKAFgLAAIgOgCIAAgKQgKAHgIADQgHACgLAAIgFAAgAAOgPQAOALAAATQAAATgMANIAKABQALAAAGgEQAPgMAAgYQAAgQgLgMQgKgMgQgBQgTgBgJAMIACAAQALAAAIAHgAg+AJQAAAKAGAGQAHAKASAAIAKAAQAMgCAIgIQAGgIAAgKQAAgGgDgDQgDgHgKgBQgFAAgIAFIgNAHQgGADgIAAQgGAAgEgDIgBAHg");
	this.shape_80.setTransform(735.2,252.8);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#000000").s().p("AhIAkQgEgMADgJIgLAAIAAgMIAJgJIALgMQgIAAgEgFQgEgFAAgIQAAgLAJgHQAIgHAMgBQATgBAJAOQARgNAaAAIAJAAQAZACAQATQAQAUgBAYQgCAZgQARQgQASgZAAIgJgBIAAgMQgQAMgYABIgDAAQgmAAgIgbgAgHgkQAHABAHAFQAIAEAFAFQAMANAAAQQABAYgNAPIAFABQALAAAIgFQAQgLACgVQABgNgHgNQgGgMgNgGQgLgFgQAAQgKAAgHACgAgrgQQgJAHgOANIAKAAQgEAJACAJQAGARAagBQAOgBALgJQAJgJABgNQABgLgJgJQgHgJgNAAIgCAAQgMAAgKAHgAg0gvIgGADQAGABACADQACAFgBAFIAHgGIAGgFQgDgEgGgCIgDAAIgEAAg");
	this.shape_81.setTransform(718.3,252.8);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#000000").s().p("Ag3AtQgRgPAAgZQAAgJAEgKQAFgJAHgFQgHgEAAgIIABgHQAGgPAbAAQAVAAAIANQAJgNAXAAQALAAAIAFQAKAFABAKQABAIgEAGQAGAFAEAJQAEAJAAAKQAAAagSAPQgVARgiAAQgjAAgUgSgAgygNQgGAGABAKQABAQATAHQAOAGAVAAQAUAAAPgGQATgHAAgQQABgKgGgGQgGgGgJgBIhEAAQgKABgGAGgAAQgsQgHAEgBAHIAjAAIAAgEQAAgEgEgEQgFgDgFAAIgBAAQgHAAgFAEgAgngvQAEACABAFQAAAEgCADIAaAAQgBgMgPgDIgGAAQgEAAgDABg");
	this.shape_82.setTransform(701.8,252.8);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("EgmqAD/Qg2AAgmgmQgmgmAAg2IAAj5QAAg2AmgmQAmgmA2AAMBNVAAAQA1AAAnAmQAmAmAAA2IAAD5QAAA2gmAmQgnAmg1AAg");
	this.shape_83.setTransform(900.5,251);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f().s("#0097A7").ss(3).p("AG4C5ItvAAQg2AAgmgmQgmgnAAg1IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA1gmAnQgmAmg2AAg");
	this.shape_84.setTransform(1091,188.4);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#05686D").s().p("Am3C5Qg2AAgmgmQgmgnAAg1IAAhtQAAg2AmgmQAmgmA2AAINvAAQA2AAAmAmQAmAmAAA2IAABtQAAA1gmAnQgmAmg2AAg");
	this.shape_85.setTransform(1091,188.4);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#000000").s().p("AgPAOIAAgbIAfAAIAAAbg");
	this.shape_86.setTransform(678.9,194);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#000000").s().p("AAABPIAAiAIgfAAIAAgYIA/gFIAACdg");
	this.shape_87.setTransform(668.5,187.5);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#000000").s().p("AAAAyQgKANgRAAQgYAAgMgSQgLgQABgcQAAgeAUgSQAOgNASgBQAIgBAGACIAAAOQgJgDgMAEQgMAEgIALQgHAMAAAOQAAAlAaAAQAJAAAGgFQAHgGAAgJIAAgNIAOAAIAAAMQAAAKAHAGQAGAFAKAAQALAAAIgIQAHgHAAgMQAAgKgIgIQgHgHgMAAIgkAAQAAgWAJgLQAJgKASABQAhAAAAAaIgBAGIgMAAQAMAFAHALQAHALAAAMQAAAXgMAPQgMAPgUAAQgWAAgJgNgAAKggIAWAAIAJAAIAAgEQAAgLgOAAQgPAAgCAPg");
	this.shape_88.setTransform(824.9,190.4);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#000000").s().p("Ag6BJQgQgGgGAAQgGAAgHAJIgLgHIAWghQgKgMAAgTQAAgHACgGQAFgUASgKQANgIASAAIAJAAIAVgfIAQAAIgVAgIALAFIAJAHQADgFADgDQAHgFAJAAQALAAAHAIQAJgJAMABQAhABACA5QAAAbgJASQgMAVgXAAQgQAAgIgIQgJgJAAgOIABgHIAOAAIAAAEQAAASAQAAQAKAAAEgQQACgJABgVQAAgQgCgMQgDgQgHAAQgKAAAAAOIAAAeIgPAAIAAgeQAAgOgJgBQgEgBgDAEQgDACAAAEQAMAOgBAXQgBAcgQAQQgQAQgdABIgBAAQgLAAgPgFgAg9A5QAMAFALAAIALgBQAPgDAKgLQAIgMAAgQQAAgPgJgKQgKgMgPgBIgOATIAIAAQAJAAAGAEQAGAGABAIQABALgFAHQgFAIgKADQgHADgJAAQgOAAgKgGIgIALIAGAAQAGAAAGACgAg+AfQACADAFABQAKACADgCQgIgCgDgFQgCgFABgFgAgpAUQAAAHAHAAQAHAAAAgHQAAgHgHAAQgHAAAAAHgAhEgFQgFAGAAAKQAAAGABAEIAbgnQgQACgHALg");
	this.shape_89.setTransform(806.5,188.9);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#000000").s().p("AgEBPQgbgBgRgQQgQgRAAgaQgBgRAHgMQAGgMAOgNQAKgIASgSQAJgLADgHIAZAAQgEAJgIAJQgHAIgLAIQARgBANAFQAmAOAAAsQAAAbgUASQgTASgaAAIgEgBgAgjgRQgKAMAAAPQAAAPAKAKQANAOAWAAQARAAAMgJQAQgNAAgTQAAgIgDgIQgFgLgLgHQgLgGgOAAQgXAAgNAPg");
	this.shape_90.setTransform(789.6,188.8);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#000000").s().p("AAoAdQALgHAAgKIgBgDQgCgHgPgEQgOgDgTAAQgRAAgOAEQgQADgDAHIAAAEQAAAKAJAGIgNAAQgMgEABgRQAAgGACgDQAHgQATgGQAOgFAXAAQA1AAALAbQACADAAAGQAAAQgLAFg");
	this.shape_91.setTransform(774.9,182.5);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#000000").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_92.setTransform(774.1,190.4);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#000000").s().p("AhIAkQgEgMADgJIgLAAIAAgMIAJgJIALgMQgIAAgEgFQgEgFAAgIQAAgLAJgHQAIgHAMgBQATgBAJAOQARgNAaAAIAJAAQAZACAQATQAQAUgBAYQgCAZgQARQgQASgZAAIgJgBIAAgMQgQAMgYABIgDAAQgmAAgIgbgAgHgkQAHABAHAFQAIAEAFAFQAMANAAAQQABAYgNAPIAFABQALAAAIgFQAQgLACgVQABgNgHgNQgGgMgNgGQgLgFgQAAQgKAAgHACgAgrgQQgJAHgOANIAKAAQgEAJACAJQAGARAagBQAOgBALgJQAJgJABgNQABgLgJgJQgHgJgNAAIgCAAQgMAAgKAHgAg0gvIgGADQAGABACADQACAFgBAFIAHgGIAGgFQgDgEgGgCIgDAAIgEAAg");
	this.shape_93.setTransform(749.2,190.4);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#000000").s().p("AgbA+QgZAAgNgSQgNgRAAgZQAAgpAdgRQAJgGAQAAIAIABIAAAOQARgPAXAAQAaABAOARQAOAQAAAbQABAagPATQgQASgZAAIgJAAIAAgMQgKAHgGADQgIADgMAAIgFgBgAAJgUQAWAGgBAaQAAAUgNAPQANACAKgGQAQgLgBgaQgBgPgKgMQgKgMgPgBQgcgCgEATQAGgEAJAAIAHABgAgygjQgJAMAAAPQAAARAJALQAKAMARAAQANABAKgGQAJgGABgLQABgGgEgEQgEgGgGABQgFAAgDADQgDADAAAFIABAGIgPAAQgDgPACgOQABgOALgPIgJgBQgPAAgJAMg");
	this.shape_94.setTransform(732.3,190.4);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#000000").s().p("AgCBUQgbgBgQgQQgRgRAAgaQAAgRAGgMIACgEQAGgKANgLIAbgaQAJgLADgHIAZAAQgFAJgGAJQgIAIgLAIQARgBANAFQAmAOAAAsQAAAbgUASQgTASgbAAIgDgBgAghgMQgLAMAAAPQAAAPAKAKQAOAOAVAAQARAAANgJQAQgNAAgTQAAgIgDgIQgFgLgLgHQgMgGgNAAQgXAAgNAPgAg2gsIgLAAIAAgMIALAAIAAgRIgMAAIAAgLIAhAAQAKAAAGAFQAIAGAAAKQAAAJgJAGQgIAFgOgBIAAASQgJAIgFAJgAgohIIAAAQQANAAAAgIQAAgIgLAAIgCAAg");
	this.shape_95.setTransform(717.3,188.3);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#000000").s().p("AApAdQAKgHAAgKIgBgDQgCgHgPgEQgPgDgSAAQgSAAgNAEQgQADgCAHIgBAEQAAAKAKAGIgOAAQgLgEAAgRQAAgGABgDQAIgQASgGQAOgFAYAAQA2AAAKAbQADADAAAGQAAAQgMAFg");
	this.shape_96.setTransform(702.7,182.5);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#000000").s().p("AgcA/QgVgBgOgOQgOgOgBgUQgBgSAGgNIAPAAQgBAFACACQABACAFAAQAHAAAGgEQALgHADgJIABgFQAAgKgHgDQgGgCgFACQAJADAAALQAAAGgFAEQgEAEgHAAIgGgBQgLgDAAgOIACgJQAGgRAXAAQAJAAAHAFQAHAEACAHQAMgRAaABQAbACAOAZQAJARAAATQgBAqgeAPQgKAFgLAAIgOgCIAAgKQgKAHgIADQgHACgLAAIgFAAgAAOgPQAOALAAATQAAATgMANIAKABQALAAAGgEQAPgMAAgYQAAgQgLgMQgKgMgQgBQgTgBgJAMIACAAQALAAAIAHgAg+AJQAAAKAGAGQAHAKASAAIAKAAQAMgCAIgIQAGgIAAgKQAAgGgDgDQgDgHgKgBQgFAAgIAFIgNAHQgGADgIAAQgGAAgEgDIgBAHg");
	this.shape_97.setTransform(702.3,190.4);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#FFFFFF").s().p("EgmqAD/Qg2AAgmgmQgmgmAAg2IAAj5QAAg2AmgmQAmgmA2AAMBNVAAAQA1AAAnAmQAmAmAAA2IAAD5QAAA2gmAmQgnAmg1AAg");
	this.shape_98.setTransform(900.5,188.7);

	this.instance_1 = new lib.Path_4();
	this.instance_1.parent = this;
	this.instance_1.setTransform(364,329.2,1,1,0,0,0,13.8,28.1);
	this.instance_1.alpha = 0.898;

	this.instance_2 = new lib.Path_1_2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(145.4,502.5,1,1,0,0,0,17.4,28.3);
	this.instance_2.alpha = 0.898;

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#000000").s().p("AAGA5IAAgZIgtAAIgBgOIAuhKIAWAAIAABGIAOAAIAAASIgOAAIAAAZgAADgVIgXAjIAaAAIAAgoIAAAAg");
	this.shape_99.setTransform(290.1,412.1);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#FFB300").s().p("AhYBZQglglAAg0QAAgzAlglQAlglAzABQA0gBAlAlQAlAlAAAzQAAA0glAlQglAlg0gBQgzABglglg");
	this.shape_100.setTransform(289.9,413.1);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#FF6F00").s().p("AhnBoQgsgrABg9QgBg8AsgrQArgrA8gBQA9ABArArQArArABA8QgBA9grArQgrArg9ABQg8gBgrgrg");
	this.shape_101.setTransform(290,413.1);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#000000").s().p("AgbAyQgMgIABgQIAAAAIAWAAQAAAHAFAEQAEAEAHAAQAHAAAFgFQAFgEAAgHQAAgJgFgEQgEgFgIAAIgNAAIAAgQIANAAQAHAAAEgEQAEgFAAgHQAAgGgEgFQgEgEgHAAQgGAAgEAEQgEAEAAAGIgWAAIgBgBQAAgNALgIQAKgKAQABQARAAAKAIQALAJAAAPQAAAHgFAHQgEAHgIADQAJADAFAGQAFAIAAAJQAAAQgMAJQgLAIgRABQgPAAgMgJg");
	this.shape_102.setTransform(239.5,456.1);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#FFB300").s().p("AhYBZQglglAAg0QAAgzAlglQAlglAzAAQA0AAAlAlQAlAlAAAzQAAA0glAlQglAlg0AAQgzAAglglg");
	this.shape_103.setTransform(239.3,457.2);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#FF6F00").s().p("AhnBoQgsgrAAg9QAAg8AsgrQArgrA8gBQA9ABArArQAsArAAA8QAAA9gsArQgrAsg9AAQg8AAgrgsg");
	this.shape_104.setTransform(239.3,457.2);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f().s("#000000").ss(2).p("AgWAAIAtAA");
	this.shape_105.setTransform(405.3,356.3);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#000000").s().p("AJYAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBQAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAG4AKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBQAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAEYAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBQAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAB4AKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBQAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAgnAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBPAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAjHAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBQAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAlnAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBQAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAoHAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBQAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAqnAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBQAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAg");
	this.shape_106.setTransform(328.9,356.3);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f().s("#000000").ss(2).p("AgWAAIAtAA");
	this.shape_107.setTransform(249.8,356.3);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f().s("#000000").ss(2).p("AgWAAIAtAA");
	this.shape_108.setTransform(283.5,474.6);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#000000").s().p("AMcAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIAcAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAJoAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBaAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAG0AKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBaAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAEAAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBaAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgABMAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBaAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAhnAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBaAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAkbAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBaAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAnPAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBaAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAqDAKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBaAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAgAs3AKQgEAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAIBaAAQAEAAADADQADADAAADQAAAEgDADQgDADgEAAg");
	this.shape_109.setTransform(194.1,474.6);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f().s("#000000").ss(2).p("AgWAAIAtAA");
	this.shape_110.setTransform(100,474.6);

	this.instance_3 = new lib.Path_3();
	this.instance_3.parent = this;
	this.instance_3.setTransform(252.3,420.8,1,1,0,0,0,13,14.1);
	this.instance_3.alpha = 0.898;

	this.instance_4 = new lib.Path_1_1();
	this.instance_4.parent = this;
	this.instance_4.setTransform(358.6,280.3,1,1,0,0,0,14,14.5);
	this.instance_4.alpha = 0.898;

	this.instance_5 = new lib.Path_2_0();
	this.instance_5.parent = this;
	this.instance_5.setTransform(135.2,571.1,1,1,0,0,0,14,14.5);
	this.instance_5.alpha = 0.898;

	this.instance_6 = new lib.Path_0();
	this.instance_6.parent = this;
	this.instance_6.setTransform(255.1,417.1,1,1,0,0,0,67.1,62.4);
	this.instance_6.alpha = 0.898;

	this.instance_7 = new lib.Path_1_0();
	this.instance_7.parent = this;
	this.instance_7.setTransform(151.5,548.5,1,1,0,0,0,40.5,72.6);
	this.instance_7.alpha = 0.898;

	this.instance_8 = new lib.Path_2();
	this.instance_8.parent = this;
	this.instance_8.setTransform(359.5,285.5,1,1,0,0,0,40.4,72.6);
	this.instance_8.alpha = 0.898;

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#558B2F").s().p("AqaanMAAAg1NIU0AAMAAAA1Ng");
	this.shape_111.setTransform(255.9,424.1);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#000000").s().p("AgPA5IAAgOQAAgYAJgTQAIgTAQgTIg5AAIAAgSIBPAAIAAASQgTAWgHARQgHASAAAYIAAAOg");
	this.shape_112.setTransform(74.5,473.5);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#FFB300").s().p("AhYBZQglglAAg0QAAgzAlglQAlglAzAAQA0AAAlAlQAlAlAAAzQAAA0glAlQglAlg0AAQgzAAglglg");
	this.shape_113.setTransform(74.4,474.6);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#FF6F00").s().p("AhnBoQgrgrAAg9QAAg8ArgrQArgsA8AAQA9AAAsAsQArArAAA8QAAA9grArQgsAsg9AAQg8AAgrgsg");
	this.shape_114.setTransform(74.4,474.6);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#000000").s().p("AgPA5IAAgOQAAgYAJgTQAIgTAQgTIg5AAIAAgSIBPAAIAAASQgTAWgHARQgHASAAAYIAAAOg");
	this.shape_115.setTransform(430.6,355.2);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#FFB300").s().p("AhYBZQglglAAg0QAAgzAlglQAlglAzAAQA0AAAlAlQAlAlAAAzQAAA0glAlQglAlg0AAQgzAAglglg");
	this.shape_116.setTransform(430.5,356.2);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#FF6F00").s().p("AhoBoQgrgrAAg9QAAg8ArgsQAsgrA8AAQA9AAArArQArAsAAA8QAAA9grArQgrAsg9gBQg8ABgsgsg");
	this.shape_117.setTransform(430.5,356.3);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#000000").s().p("AgbAvQgLgMAAgWIAAgXQAAgVAMgOQANgNATABQAGgBAGACIALADIgEARIgKgDIgJAAQgJAAgGAHQgHAIAAANIAAAEQAFgEAGgCQAGgDAHAAQAPAAAIAKQAIAKAAAQQAAARgKALQgLALgRAAQgRgBgLgLgAgJACQgEACgDADIAAAHQAAANAFAHQAFAHAHAAQAHAAAEgGQAEgGAAgJQAAgJgEgGQgEgFgHAAQgFAAgFACg");
	this.shape_118.setTransform(414.9,229.9);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#FFB300").s().p("AhYBZQglglAAg0QAAgzAlglQAlglAzAAQA0AAAlAlQAlAlAAAzQAAA0glAlQglAlg0AAQgzAAglglg");
	this.shape_119.setTransform(414.7,230.9);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#FF6F00").s().p("AhoBoQgrgrAAg9QAAg8ArgrQAsgrA8gBQA9ABArArQArArAAA8QAAA9grArQgrArg9AAQg8AAgsgrg");
	this.shape_120.setTransform(414.8,231);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#000000").s().p("AgaAyQgLgJAAgPIAAAAIAWgBQAAAHAEAEQAFAEAGAAQAIAAAEgFQADgFAAgLQAAgJgEgGQgEgFgHAAQgGAAgEABQgDADgCAEIgUgBIAHg+IA+AAIAAASIgsAAIgDAaIAIgEQAFgBAFgBQAQAAAIAKQAJAKAAARQAAARgJALQgKALgTgBQgPAAgLgHg");
	this.shape_121.setTransform(355.7,334.4);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#FFB300").s().p("AhYBZQglglAAg0QAAgzAlglQAlgkAzAAQA0AAAlAkQAlAlAAAzQAAA0glAlQglAlg0AAQgzAAglglg");
	this.shape_122.setTransform(355.5,335.4);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#FF6F00").s().p("AhnBoQgrgrgBg9QABg8ArgsQArgqA8gBQA9ABArAqQAsAsAAA8QAAA9gsArQgrArg9ABQg8gBgrgrg");
	this.shape_123.setTransform(355.5,335.4);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#000000").s().p("AgkA6IAAgPIAjgoQAHgIADgHQAEgGAAgGQAAgHgEgEQgDgEgGAAQgIAAgEAFQgEAGAAAIIgWAAIAAAAQgBgQALgKQAKgLASAAQAQAAAKAJQAJAJAAAPQAAALgFAIQgFAHgOAPIgRAXIAAAAIAtAAIAAASg");
	this.shape_124.setTransform(157.8,493.9);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#FFB300").s().p("AhYBZQgkglAAg0QAAgzAkglQAlglAzAAQA0AAAlAlQAlAlAAAzQAAA0glAlQglAlg0AAQgzAAglglg");
	this.shape_125.setTransform(157.7,495.1);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#FF6F00").s().p("AhoBoQgrgrAAg9QAAg8ArgrQAsgsA8AAQA9AAArAsQAsArgBA8QABA9gsArQgrAsg9AAQg8AAgsgsg");
	this.shape_126.setTransform(157.7,495.1);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#000000").s().p("AAAA6IAAhdIgWAAIAAgSIAtgEIAABzg");
	this.shape_127.setTransform(101.2,589.9);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("#FFB300").s().p("AhYBZQglglAAg0QAAgzAlglQAlglAzAAQA0AAAlAlQAlAlAAAzQAAA0glAlQglAlg0AAQgzAAglglg");
	this.shape_128.setTransform(101.7,591);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("#FF6F00").s().p("AhoBoQgqgrAAg9QAAg8AqgrQAsgsA8AAQA9AAArAsQArArAAA8QAAA9grArQgrAsg9AAQg8AAgsgsg");
	this.shape_129.setTransform(101.8,591);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#FFFFFF").s().p("AgKAOQgHgFAAgJQAAgFAEgFQAFgHAIAAQAGAAAFAEQAHAFAAAIQAAAGgEAFQgFAHgJAAQgFAAgFgEg");
	this.shape_130.setTransform(1123,91);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f("#FFFFFF").s().p("AgcA/QgVgBgOgOQgOgOgBgUQgBgSAGgNIAPAAQgBAFACACQABACAFAAQAHAAAGgEQALgHADgJIABgFQAAgKgHgDQgGgCgFACQAJADAAALQAAAGgFAEQgEAEgHAAIgGgBQgLgDAAgOIACgJQAGgRAXAAQAJAAAHAFQAHAEACAHQAMgRAaABQAbACAOAZQAJARAAATQgBAqgeAPQgKAFgLAAIgOgCIAAgKQgKAHgIADQgHACgLAAIgFAAgAAOgPQAOALAAATQAAATgMANIAKABQALAAAGgEQAPgMAAgYQAAgQgLgMQgKgMgQgBQgTgBgJAMIACAAQALAAAIAHgAg+AJQAAAKAGAGQAHAKASAAIAKAAQAMgCAIgIQAGgIAAgKQAAgGgDgDQgDgHgKgBQgFAAgIAFIgNAHQgGADgIAAQgGAAgEgDIgBAHg");
	this.shape_131.setTransform(1111,86.3);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f("#FFFFFF").s().p("AgQAeIAAgZIgLAAIAAgLIALAAIAAgQIgLAAIAAgLIAgAAQAKAAAGAEQAHAHAAAKQAAAJgIADQgJAFgMAAIAAAdgAgBgVIAAAPQALAAAAgHQAAgJgJAAIgCABg");
	this.shape_132.setTransform(1099.9,77.8);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f("#FFFFFF").s().p("AgcA/QgVgBgOgOQgOgOgBgUQgBgSAGgNIAPAAQgBAFACACQABACAFAAQAHAAAGgEQALgHADgJIABgFQAAgKgHgDQgGgCgFACQAJADAAALQAAAGgFAEQgEAEgHAAIgGgBQgLgDAAgOIACgJQAGgRAXAAQAJAAAHAFQAHAEACAHQAMgRAaABQAbACAOAZQAJARAAATQgBAqgeAPQgKAFgLAAIgOgCIAAgKQgKAHgIADQgHACgLAAIgFAAgAAOgPQAOALAAATQAAATgMANIAKABQALAAAGgEQAPgMAAgYQAAgQgLgMQgKgMgQgBQgTgBgJAMIACAAQALAAAIAHgAg+AJQAAAKAGAGQAHAKASAAIAKAAQAMgCAIgIQAGgIAAgKQAAgGgDgDQgDgHgKgBQgFAAgIAFIgNAHQgGADgIAAQgGAAgEgDIgBAHg");
	this.shape_133.setTransform(1094.5,86.3);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f("#FFFFFF").s().p("AADBNQgLgGAAgMQgBgJAHgFQgaABgSgPQgSgOACgWQABgbAWgMQgDgBgDgEQgDgEAAgFIABgFQAFgKAMgEQAIgDAQAAQAdAAANAPQAJAMABARIgtAAQgNAAgJAIQgJAJgBAMQgBAPAMALQAMAKAQABQAZABASgKIAAAPIgRAGQgQAEAAAGQgBAFAFADQAEADAGAAQAHAAAGgEQAFgDAAgHIgBgGIAOAAIABAHQAAANgLAJQgKAHgPABIgGABQgKAAgJgEgAgTg/IADACQABAAAAABQAAAAABABQAAAAAAABQAAAAABABQAAAFgEACIAHAAIAkAAQgFgKgLgDQgGgCgJAAQgJAAgFACg");
	this.shape_134.setTransform(1080.7,88.1);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f("#FFFFFF").s().p("AgcA6IAAgOQAEADAIAAQAIAAAFgOQAFgOAAgRQAAgRgFgOQgFgQgHAAQgIgBgFAEIAAgPQAHgFAMAAQARAAALAWQAKATAAAXQAAAXgKASQgMAUgQAAQgLAAgIgFg");
	this.shape_135.setTransform(1070.9,86.3);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f("#FFFFFF").s().p("AAAAyQgKANgRAAQgYAAgMgSQgLgQABgcQAAgeAUgSQAOgNASgBQAIgBAGACIAAAOQgJgDgMAEQgMAEgIALQgHAMAAAOQAAAlAaAAQAJAAAGgFQAHgGAAgJIAAgNIAOAAIAAAMQAAAKAHAGQAGAFAKAAQALAAAIgIQAHgHAAgMQAAgKgIgIQgHgHgMAAIgkAAQAAgWAJgLQAJgKASABQAhAAAAAaIgBAGIgMAAQAMAFAHALQAHALAAAMQAAAXgMAPQgMAPgUAAQgWAAgJgNgAAKggIAWAAIAJAAIAAgEQAAgLgOAAQgPAAgCAPg");
	this.shape_136.setTransform(1060.5,86.2);

	this.shape_137 = new cjs.Shape();
	this.shape_137.graphics.f("#FFFFFF").s().p("AgnAtQgSgSAAgaQAAgcASgRQARgSAaAAQAdAAANAMQAJAJABAMIgOAAQgBgJgLgFQgLgEgNAAQgSAAgMALQgOANAAARQgBARANALQAMAKASAAQANAAALgHQAMgIAAgOQgBgRgQgEQAGAFABAMQAAAHgIAGQgHAHgJAAQgLAAgIgHQgHgHgBgLQAAgMAKgIQAJgGANAAQAUAAANALQAOALABASQAAAYgRAPQgQAQgXAAQgZAAgRgSgAgCgOQgDADAAAEQAAAFADACQACADAEAAQAEAAAEgDQACgCAAgEQAAgFgCgDQgEgDgDAAQgFAAgCADg");
	this.shape_137.setTransform(1046.6,86.3);

	this.shape_138 = new cjs.Shape();
	this.shape_138.graphics.f("#FFFFFF").s().p("AAoAdQAKgHAAgKIAAgDQgCgHgQgEQgNgDgTAAQgRAAgOAEQgQADgDAHIAAAEQAAAKAJAGIgNAAQgMgEAAgRQAAgGADgDQAHgQASgGQAOgFAYAAQA1AAAMAbQABADAAAGQAAAQgLAFg");
	this.shape_138.setTransform(1024.9,78.4);

	this.shape_139 = new cjs.Shape();
	this.shape_139.graphics.f("#FFFFFF").s().p("AASAwQANABAKgGQAKgGAEgMQAFgMgBgMQgBgOgKgKQgKgLgOAAQgXACgFAUQAEgDAFAAQAHAAAEADQAOAJgBAWQAAAUgSAMQgPAMgXAAQgWAAgQgOQgPgOAAgVQAAgXARgLQgFgBgDgFQgDgFAAgGQAAgLAKgHQAJgGAMAAQALAAAIAHQAIAGACALQAOgXAcgBQAXgBAPAUQAPATAAAYQAAAbgSARQgRASgcAAgAg2gHQgHAGAAAJQAAAMAMAHQALAGAPAAQANAAAKgFQAKgGABgKQAAgFgCgEQgDgDgEAAQgDAAgCACQgDADAAADIgOAAIAAgLIABgKIgSAAQgKAAgHAGgAgygtQADACADAGQABAFgBAFIAPAAQgBgKgFgEQgEgEgHAAIgEAAg");
	this.shape_139.setTransform(1024.3,86.2);

	this.shape_140 = new cjs.Shape();
	this.shape_140.graphics.f("#FFFFFF").s().p("AgzBCQgVgPABgbQABgXATgJQgKgCAAgKQAAgOAOgGQAKgFAOABQARAAAKAOQAKAOAAAQIgrAAQgJAAgHAFQgHAFABAKQABAWAdAGQAJACAMgBQAYAAAPgPQAOgPABgXQAAgYgOgSQgPgSgcgBQgggBgRAYIgQAAQAIgSASgKQARgJAWAAQAhAAAVAXQAVAXAAAhQAAAhgTAWQgUAZghAAQggAAgTgOgAgigcQADACABADQACAGgCAFIAbAAQgCgIgFgFQgGgFgJAAQgFAAgEACg");
	this.shape_140.setTransform(1008.4,84.6);

	this.shape_141 = new cjs.Shape();
	this.shape_141.graphics.f("#FFFFFF").s().p("AgnAtQgTgSABgaQgBgcATgRQARgSAaAAQAdAAANAMQAJAJABAMIgOAAQgBgJgLgFQgLgEgNAAQgSAAgNALQgOANABARQAAARAMALQAMAKASAAQAOAAAKgHQAMgIAAgOQAAgRgQgEQAFAFABAMQAAAHgIAGQgHAHgJAAQgLAAgIgHQgHgHAAgLQAAgMAJgIQAJgGANAAQATAAAOALQAPALAAASQAAAYgRAPQgQAQgXAAQgZAAgRgSgAgCgOQgDADAAAEQAAAFADACQACADAEAAQAFAAADgDQADgCAAgEQAAgFgDgDQgEgDgDAAQgFAAgCADg");
	this.shape_141.setTransform(994.8,86.3);

	this.shape_142 = new cjs.Shape();
	this.shape_142.graphics.f("#FFFFFF").s().p("Ag+ArIAAgYIBvAAIAAgnIAHgKQAFgGACgGIAABVg");
	this.shape_142.setTransform(981,92.4);

	this.shape_143 = new cjs.Shape();
	this.shape_143.graphics.f("#FFFFFF").s().p("Ag0A9QgTgTABgfQAAgVAQgQQAPgPAXAAIAQAAIAAAPIgOAAQgRAAgKAKQgKAKgBAOQAAATAQAMQAQALAUAAQAXAAAPgRQAPgPAAgXQAAgXgQgSQgPgSgWgBQgQAAgOAHQgOAHgGANIgOAAQAHgUARgLQAQgKAYAAQAgAAAUAXQAUAXAAAiQAAAigUAWQgVAXggAAQggAAgTgTg");
	this.shape_143.setTransform(981,84.5);

	this.shape_144 = new cjs.Shape();
	this.shape_144.graphics.f("#FFFFFF").s().p("AgcA6IAAgOQAEADAIAAQAIAAAFgOQAFgOAAgRQAAgRgFgOQgFgQgHAAQgIgBgFAEIAAgPQAHgFAMAAQARAAALAWQAKATAAAXQAAAXgKASQgMAUgQAAQgLAAgIgFg");
	this.shape_144.setTransform(970.5,86.3);

	this.shape_145 = new cjs.Shape();
	this.shape_145.graphics.f("#FFFFFF").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_145.setTransform(959.3,86.3);

	this.shape_146 = new cjs.Shape();
	this.shape_146.graphics.f("#FFFFFF").s().p("AgnAtQgTgSABgaQgBgcATgRQARgSAaAAQAdAAANAMQAJAJABAMIgOAAQgBgJgMgFQgKgEgNAAQgRAAgOALQgNANAAARQAAARAMALQAMAKARAAQAOAAALgHQAMgIgBgOQAAgRgPgEQAFAFAAAMQABAHgIAGQgIAHgIAAQgLAAgIgHQgIgHABgLQgBgMAKgIQAKgGAMAAQATAAAOALQAPALAAASQgBAYgQAPQgQAQgXAAQgZAAgRgSgAgCgOQgEADAAAEQAAAFAEACQACADAEAAQAEAAADgDQAEgCAAgEQAAgFgEgDQgDgDgDAAQgFAAgCADg");
	this.shape_146.setTransform(944.6,86.3);

	this.shape_147 = new cjs.Shape();
	this.shape_147.graphics.f("#FFFFFF").s().p("AApAdQAKgHgBgKIAAgDQgCgHgQgEQgOgDgSAAQgSAAgNAEQgQADgCAHIgBAEQAAAKAKAGIgOAAQgMgEABgRQAAgGABgDQAIgQASgGQAOgFAYAAQA2AAALAbQACADAAAGQAAAQgMAFg");
	this.shape_147.setTransform(922.9,78.4);

	this.shape_148 = new cjs.Shape();
	this.shape_148.graphics.f("#FFFFFF").s().p("AhIAkQgEgMADgJIgLAAIAAgMIAJgJIALgMQgIAAgEgFQgEgFAAgIQAAgLAJgHQAIgHAMgBQATgBAJAOQARgNAaAAIAJAAQAZACAQATQAQAUgBAYQgCAZgQARQgQASgZAAIgJgBIAAgMQgQAMgYABIgDAAQgmAAgIgbgAgHgkQAHABAHAFQAIAEAFAFQAMANAAAQQABAYgNAPIAFABQALAAAIgFQAQgLACgVQABgNgHgNQgGgMgNgGQgLgFgQAAQgKAAgHACgAgrgQQgJAHgOANIAKAAQgEAJACAJQAGARAagBQAOgBALgJQAJgJABgNQABgLgJgJQgHgJgNAAIgCAAQgMAAgKAHgAg0gvIgGADQAGABACADQACAFgBAFIAHgGIAGgFQgDgEgGgCIgDAAIgEAAg");
	this.shape_148.setTransform(921.9,86.3);

	this.shape_149 = new cjs.Shape();
	this.shape_149.graphics.f("#FFFFFF").s().p("AgbAzQgLgJAAgSQAAgNAFgKQADgFAJgKQAJgLAZgVIgwAAIAAgNIBHAAIAAANIgQANQgIAGgHAGQgJAMgDAFQgHAJABALQAAAHAGAEQAEADAGAAQANABAEgLIABgHQAAgFgDgFIAPAAQAHALgBANQgBAOgMAKQgLAJgPAAQgQAAgLgJg");
	this.shape_149.setTransform(909,86.5);

	this.shape_150 = new cjs.Shape();
	this.shape_150.graphics.f("#FFFFFF").s().p("AAVBMIAAgcQgSACgOgBQgcgBgRgMQgTgOAAgZQAAgMAHgKQAHgKAJgFQgFgBgCgEQgCgDAAgEQAAgIAHgGQAMgJAWAAQAiAAAJAUQACgJAHgGQAIgFAJAAIAOAAIAAAOIgCAAQgFAAgBAEQAAACAFAEQAHAFADAEQAGAGAAAJQABAPgOAHQgLAGgPgBIAAAfIANgDIAJgDIAAANIgJAFIgNADIAAAegAgigWQgIAJAAANQABAaAeAIIAPABQALAAAGgBIAAhCIgiAAQgNAAgIAKgAAlglIAAAYIADAAQAGAAAEgDQAEgEAAgGQAAgHgGgIQgGgJAAgEIAAgDQgFACAAASgAAJgtQgCgJgLgFQgNgEgMAEQADAEAAADQAAAFgDACIADAAIADAAIAgAAIAAAAg");
	this.shape_150.setTransform(896.9,87.6);

	this.shape_151 = new cjs.Shape();
	this.shape_151.graphics.f("#FFFFFF").s().p("AgQAeIAAgZIgLAAIAAgLIALAAIAAgQIgLAAIAAgLIAgAAQAKAAAGAEQAHAHAAAKQAAAJgIADQgJAFgMAAIAAAdgAgBgVIAAAPQALAAAAgHQAAgJgJAAIgCABg");
	this.shape_151.setTransform(878.5,77.8);

	this.shape_152 = new cjs.Shape();
	this.shape_152.graphics.f("#FFFFFF").s().p("AAAAyQgKANgRAAQgYAAgMgSQgLgQABgcQAAgeAUgSQAOgNASgBQAIgBAGACIAAAOQgJgDgMAEQgMAEgIALQgHAMAAAOQAAAlAaAAQAJAAAGgFQAHgGAAgJIAAgNIAOAAIAAAMQAAAKAHAGQAGAFAKAAQALAAAIgIQAHgHAAgMQAAgKgIgIQgHgHgMAAIgkAAQAAgWAJgLQAJgKASABQAhAAAAAaIgBAGIgMAAQAMAFAHALQAHALAAAMQAAAXgMAPQgMAPgUAAQgWAAgJgNgAAKggIAWAAIAJAAIAAgEQAAgLgOAAQgPAAgCAPg");
	this.shape_152.setTransform(873.5,86.2);

	this.shape_153 = new cjs.Shape();
	this.shape_153.graphics.f("#FFFFFF").s().p("AgoAtQgRgSgBgaQABgcARgRQASgSAaAAQAdAAANAMQAJAJABAMIgPAAQAAgJgLgFQgLgEgNAAQgSAAgMALQgOANgBARQAAARANALQANAKARAAQANAAALgHQALgIAAgOQAAgRgQgEQAHAFAAAMQgBAHgHAGQgIAHgJAAQgLAAgHgHQgIgHAAgLQAAgMAKgIQAJgGANAAQATAAAPALQAOALgBASQAAAYgQAPQgQAQgXAAQgZAAgSgSgAgDgOQgCADAAAEQAAAFACACQADADAEAAQAFAAADgDQACgCAAgEQAAgFgCgDQgEgDgEAAQgEAAgDADg");
	this.shape_153.setTransform(859.6,86.3);

	this.shape_154 = new cjs.Shape();
	this.shape_154.graphics.f("#FFFFFF").s().p("AgDBTQgbgBgQgRQgQgRgBgZQgBgXAOgRQgDgBgFgFQgEgFgCgFQgCgHAAgFQAAgMALgMQALgNATAAQAGAAAFACQANADABAJIAGgKIAaAAQgFAKgJAKQgJAKgLAHQARgBAOAEQAlAOABAsQgBAcgTASQgTARgcAAIgDAAgAghgOQgLANAAAPQAAAPAKAKQANANAWAAQARAAAMgJQAQgMABgTQgBgJgDgHQgEgMgMgGQgLgGgNAAQgXAAgNAOgAgqg0QgFAIAAAFQAAAJAGADIAFgEIAdgaQgDgGgHAAIgEgBQgOAAgHAMg");
	this.shape_154.setTransform(846.6,84.3);

	this.shape_155 = new cjs.Shape();
	this.shape_155.graphics.f("#FFFFFF").s().p("AADBcQgLgGAAgLQgBgKAHgFQgaACgSgPQgSgPACgXQABgZAWgMQgDgBgDgEQgDgEAAgFIABgFIACgEQgJgEAAgNQAAgGACgFQAKgQAoAAQAkAAAJAPQADAGAAAGQAAAOgMADQALAMABASIgtAAQgNAAgJAJQgJAJgBALQgBAPAMALQAMAKAQABQAZABASgJIAAAPIgRAFQgQAEAAAHQgBAFAFACQAEADAGAAQAHAAAGgDQAFgEAAgHIgBgFIAOAAIABAGQAAAOgLAIQgKAIgPABIgGAAQgKAAgJgEgAgTgwQAEACABAFQAAAEgDACIAHAAIAkAAQgEgJgMgEQgGgCgJAAQgJABgFABgAASg9IAHADQADgDAAgDQAAgGgLgDQgJgEgKAAQgNAAgJADQgNADAAAGQAAADACACIAGgCQAIgCAPAAQAPAAAJADg");
	this.shape_155.setTransform(834.2,86.5);

	this.shape_156 = new cjs.Shape();
	this.shape_156.graphics.f("#FFFFFF").s().p("AglBLQgRgJACgRQABgIAIgFQAHgEAKAAIAHABIAAAMIgDAAQgKAAAAAGQgBAJAhAAQAKgBAIgCQAKgDgBgFQAAgGgRABIgPABIAAgNQAagCAOgKQARgMAAgTIgCgKQgDgPgPgIQgOgHgSgBQgVAAgOAIQgPAJgBARQgBAPAKAIQAJAJAOAAQAIAAAHgEQAIgDABgHQgFAEgGAAQgKAAgGgFQgIgGABgJQgBgMAHgGQAHgGAMgBQAOAAAIAJQAIAJAAAOQAAAQgNAKQgMAKgRABQgYACgPgOQgPgOAAgWQAAgdAWgQQATgQAgAAQAhABASARQARARAAAcQAAAkgiASQAMAGAAALQgBANgOAHQgOAIgUABIgEAAQgUAAgNgHgAgLgZQgCADAAAFQAAAEADADQADADAEAAQADAAAEgDQACgDAAgEQAAgFgCgDQgEgCgDAAQgFAAgDACg");
	this.shape_156.setTransform(820.3,88.2);

	this.shape_157 = new cjs.Shape();
	this.shape_157.graphics.f("#FFFFFF").s().p("AAAAyQgKANgRAAQgYAAgMgSQgLgQABgcQAAgeAUgSQAOgNASgBQAIgBAGACIAAAOQgJgDgMAEQgMAEgIALQgHAMAAAOQAAAlAaAAQAJAAAGgFQAHgGAAgJIAAgNIAOAAIAAAMQAAAKAHAGQAGAFAKAAQALAAAIgIQAHgHAAgMQAAgKgIgIQgHgHgMAAIgkAAQAAgWAJgLQAJgKASABQAhAAAAAaIgBAGIgMAAQAMAFAHALQAHALAAAMQAAAXgMAPQgMAPgUAAQgWAAgJgNgAAKggIAWAAIAJAAIAAgEQAAgLgOAAQgPAAgCAPg");
	this.shape_157.setTransform(797.4,86.2);

	this.shape_158 = new cjs.Shape();
	this.shape_158.graphics.f("#FFFFFF").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_158.setTransform(781.2,86.3);

	this.shape_159 = new cjs.Shape();
	this.shape_159.graphics.f("#FFFFFF").s().p("AgfAhQgOgNAAgUQAAgSAOgOQANgNASAAQATAAANANQAOANAAATQAAASgNAOQgOAOgTAAQgRAAgOgNgAgVgVQgFAHAAAJQAAANALAJQAHAFAJAAQANAAAIgLQAGgHAAgJQAAgNgLgJQgIgFgIAAQgOAAgIALg");
	this.shape_159.setTransform(767.7,87.8);

	this.shape_160 = new cjs.Shape();
	this.shape_160.graphics.f("#FFFFFF").s().p("AAVBMIAAgcQgSACgOgBQgcgBgRgMQgTgOAAgZQAAgMAHgKQAHgKAJgFQgFgBgCgEQgCgDAAgEQAAgIAHgGQAMgJAWAAQAiAAAJAUQACgJAHgGQAIgFAJAAIAOAAIAAAOIgCAAQgFAAgBAEQAAACAFAEQAHAFADAEQAGAGAAAJQABAPgOAHQgLAGgPgBIAAAfIANgDIAJgDIAAANIgJAFIgNADIAAAegAgigWQgIAJAAANQABAaAeAIIAPABQALAAAGgBIAAhCIgiAAQgNAAgIAKgAAlglIAAAYIADAAQAGAAAEgDQAEgEAAgGQAAgHgGgIQgGgJAAgEIAAgDQgFACAAASgAAJgtQgCgJgLgFQgNgEgMAEQADAEAAADQAAAFgDACIADAAIADAAIAgAAIAAAAg");
	this.shape_160.setTransform(755,87.6);

	this.shape_161 = new cjs.Shape();
	this.shape_161.graphics.f("#FFFFFF").s().p("AgvA4QgWgRAAgcQAAgaATgPQgBgNAFgJQALgVAWACQALABAHAIQAIgKAQABQAPAAAHAKQAJALAAAYIg8AAIgaABQAAAQAMALQAMALAPgBQAMgBAJgHQAJgGACgKIAPAAQgEARgOALQgOAMgTAAQgkAAgPgaIgBAKQAAASAPAMQAOAMASAAQAcgBAOgRQAJgKgBgNIAPAAQABARgLAPQgTAcgnAAQgcAAgUgRgAgXg0QgDAFABAIIALgBIAMAAQAAgQgLAAQgGAAgEAEgAANgoIAZAAQgBgQgLAAQgNAAAAAQg");
	this.shape_161.setTransform(732.3,87.2);

	this.shape_162 = new cjs.Shape();
	this.shape_162.graphics.f("#FFFFFF").s().p("AgcA6IAAgOQAEADAIAAQAIAAAFgOQAFgOAAgRQAAgRgFgOQgFgQgHAAQgIgBgFAEIAAgPQAHgFAMAAQARAAALAWQAKATAAAXQAAAXgKASQgMAUgQAAQgLAAgIgFg");
	this.shape_162.setTransform(721.9,86.3);

	this.shape_163 = new cjs.Shape();
	this.shape_163.graphics.f("#FFFFFF").s().p("AADBNQgLgGAAgMQgBgJAHgFQgaABgSgPQgSgOACgWQABgbAWgMQgDgBgDgEQgDgEAAgFIABgFQAFgKAMgEQAIgDAQAAQAdAAANAPQAJAMABARIgtAAQgNAAgJAIQgJAJgBAMQgBAPAMALQAMAKAQABQAZABASgKIAAAPIgRAGQgQAEAAAGQgBAFAFADQAEADAGAAQAHAAAGgEQAFgDAAgHIgBgGIAOAAIABAHQAAANgLAJQgKAHgPABIgGABQgKAAgJgEgAgTg/IADACQABAAAAABQAAAAABABQAAAAAAABQAAAAABABQAAAFgEACIAHAAIAkAAQgFgKgLgDQgGgCgJAAQgJAAgFACg");
	this.shape_163.setTransform(713.5,88.1);

	this.shape_164 = new cjs.Shape();
	this.shape_164.graphics.f("#FFFFFF").s().p("AAVBMIAAgcQgSACgOgBQgcgBgRgMQgTgOAAgZQAAgMAHgKQAHgKAJgFQgFgBgCgEQgCgDAAgEQAAgIAHgGQAMgJAWAAQAiAAAJAUQACgJAHgGQAIgFAJAAIAOAAIAAAOIgCAAQgFAAgBAEQAAACAFAEQAHAFADAEQAGAGAAAJQABAPgOAHQgLAGgPgBIAAAfIANgDIAJgDIAAANIgJAFIgNADIAAAegAgigWQgIAJAAANQABAaAeAIIAPABQALAAAGgBIAAhCIgiAAQgNAAgIAKgAAlglIAAAYIADAAQAGAAAEgDQAEgEAAgGQAAgHgGgIQgGgJAAgEIAAgDQgFACAAASgAAJgtQgCgJgLgFQgNgEgMAEQADAEAAADQAAAFgDACIADAAIADAAIAgAAIAAAAg");
	this.shape_164.setTransform(699.1,87.6);

	this.shape_165 = new cjs.Shape();
	this.shape_165.graphics.f("#FFFFFF").s().p("Ag0A9QgTgTABgfQAAgVAQgQQAPgPAXAAIAQAAIAAAPIgOAAQgRAAgKAKQgKAKgBAOQAAATAQAMQAQALAUAAQAXAAAPgRQAPgPAAgXQAAgXgQgSQgPgSgWgBQgQAAgOAHQgOAHgGANIgOAAQAHgUARgLQAQgKAYAAQAgAAAUAXQAUAXAAAiQAAAigUAWQgVAXggAAQggAAgTgTg");
	this.shape_165.setTransform(676.1,84.5);

	this.shape_166 = new cjs.Shape();
	this.shape_166.graphics.f("#FFFFFF").s().p("AgiBJQgPgHgJgNQgNgTAAgbQAAgTALgQQAKgQAPABQAOABAEAKQAHgMAPAAQAOAAAJAKQAKALABAOQABAQgNAMQgMANgQAAQgQAAgKgKQgLgKABgOQAAgFACgFIAEgIQAAgDgCgCQgCgCgDAAQgHABgEAKQgEAIAAAKQgBAVAQAOQAQANAVAAQAWAAAQgQQAQgQgBgWQAAgbgNgRQgPgRgaAAQgiAAgNAYIgQAAQAHgTASgKQAQgKAWAAQAhAAASATQAWAWABAkQACAjgVAYQgUAZgiAAQgSAAgPgIgAgUgGQAAAHAGAEQAHAFAHAAQAJAAAHgGQAIgFAAgIIgBgEQgBAFgGAEQgGADgHAAQgRAAgDgPIgDAKgAgDgXQAAAEADACQABADAEAAQAEAAACgDQADgCAAgEQAAgDgDgDQgCgDgEAAQgIAAAAAJg");
	this.shape_166.setTransform(660.9,84.5);

	this.shape_167 = new cjs.Shape();
	this.shape_167.graphics.f("#FFFFFF").s().p("AgcA/QgVgBgOgOQgOgOgBgUQgBgSAGgNIAPAAQgBAFACACQABACAFAAQAHAAAGgEQALgHADgJIABgFQAAgKgHgDQgGgCgFACQAJADAAALQAAAGgFAEQgEAEgHAAIgGgBQgLgDAAgOIACgJQAGgRAXAAQAJAAAHAFQAHAEACAHQAMgRAaABQAbACAOAZQAJARAAATQgBAqgeAPQgKAFgLAAIgOgCIAAgKQgKAHgIADQgHACgLAAIgFAAgAAOgPQAOALAAATQAAATgMANIAKABQALAAAGgEQAPgMAAgYQAAgQgLgMQgKgMgQgBQgTgBgJAMIACAAQALAAAIAHgAg+AJQAAAKAGAGQAHAKASAAIAKAAQAMgCAIgIQAGgIAAgKQAAgGgDgDQgDgHgKgBQgFAAgIAFIgNAHQgGADgIAAQgGAAgEgDIgBAHg");
	this.shape_167.setTransform(645.2,86.3);

	this.shape_168 = new cjs.Shape();
	this.shape_168.graphics.f("#FFFFFF").s().p("Ag+ArIAAgYIBvAAIAAgnIAHgKQAFgGACgGIAABVg");
	this.shape_168.setTransform(621.7,92.4);

	this.shape_169 = new cjs.Shape();
	this.shape_169.graphics.f("#FFFFFF").s().p("AAjA/QgWAAgKgPQgJAPgYAAQgSAAgMgOQgMgOAAgSQAAgUAOgLIgTAAIAAgOIATAAQgEgCAAgHIABgHQAHgSAaAAQAXAAAHAPQAKgPAVAAQANAAAIAFQAKAHABALQABAKgEAFIgDAAQATAQAAAWQAAAUgMAOQgMAPgSAAIgBAAgAAKAEIAAANQAAAIAIAEQAHADAJAAQAKAAAIgHQAIgIgBgKQAAgJgHgGQgHgGgKAAIg/AAQgKAAgIAHQgHAGAAAJQgBALAHAHQAIAGALAAQAJAAAHgDQAIgEAAgHIAAgOgAAegvQgIAAgFAGQgFAFgBAIIAhAAQADgDgBgFQgBgLgNAAIgCAAgAgkgtQAEADABAFQABAFgDAEIAbAAQgBgKgIgGQgFgDgHAAQgFAAgEACg");
	this.shape_169.setTransform(621.2,86.3);

	this.shape_170 = new cjs.Shape();
	this.shape_170.graphics.f("#FFFFFF").s().p("AgxBTIAAgXIBaAAIAAgVQgSAFgbgCQgYgBgPgNQgPgOAAgUQAAgaAWgMQgDgBgDgEQgCgEAAgFIAAgGQAEgLANgEQAJgDARAAQAbAAAOAQQAJALABATIgsAAQgNAAgJAIQgKAHgBAMQgBAOAMALQANALAPAAQAcAAATgKIAABCgAgQhBQAEACABAFQABAIgFACIAIgBIAlAAQgCgKgNgFQgFgCgKgBQgLAAgFACg");
	this.shape_170.setTransform(607.4,88.3);

	this.shape_171 = new cjs.Shape();
	this.shape_171.graphics.f("#FFFFFF").s().p("Ag+ArIAAgYIBvAAIAAgnIAHgKQAFgGACgGIAABVg");
	this.shape_171.setTransform(593.7,92.4);

	this.shape_172 = new cjs.Shape();
	this.shape_172.graphics.f("#FFFFFF").s().p("AAjA/QgWAAgKgPQgJAPgYAAQgSAAgMgOQgMgOAAgSQAAgUAOgLIgTAAIAAgOIATAAQgEgCAAgHIABgHQAHgSAaAAQAXAAAHAPQAKgPAVAAQANAAAIAFQAKAHABALQABAKgEAFIgDAAQATAQAAAWQAAAUgMAOQgMAPgSAAIgBAAgAAKAEIAAANQAAAIAIAEQAHADAJAAQAKAAAIgHQAIgIgBgKQAAgJgHgGQgHgGgKAAIg/AAQgKAAgIAHQgHAGAAAJQgBALAHAHQAIAGALAAQAJAAAHgDQAIgEAAgHIAAgOgAAegvQgIAAgFAGQgFAFgBAIIAhAAQADgDgBgFQgBgLgNAAIgCAAgAgkgtQAEADABAFQABAFgDAEIAbAAQgBgKgIgGQgFgDgHAAQgFAAgEACg");
	this.shape_172.setTransform(593.1,86.3);

	this.shape_173 = new cjs.Shape();
	this.shape_173.graphics.f("#FFFFFF").s().p("Ag0A9QgTgTABgfQAAgVAQgQQAPgPAXAAIAQAAIAAAPIgOAAQgRAAgKAKQgKAKgBAOQAAATAQAMQAQALAUAAQAXAAAPgRQAPgPAAgXQAAgXgQgSQgPgSgWgBQgQAAgOAHQgOAHgGANIgOAAQAHgUARgLQAQgKAYAAQAgAAAUAXQAUAXAAAiQAAAigUAWQgVAXggAAQggAAgTgTg");
	this.shape_173.setTransform(570.3,84.5);

	this.shape_174 = new cjs.Shape();
	this.shape_174.graphics.f("#FFFFFF").s().p("AgiBJQgPgHgJgNQgNgTAAgbQAAgTALgQQAKgQAPABQAOABAEAKQAHgMAPAAQAOAAAJAKQAKALABAOQABAQgNAMQgMANgQAAQgQAAgKgKQgLgKABgOQAAgFACgFIAEgIQAAgDgCgCQgCgCgDAAQgHABgEAKQgEAIAAAKQgBAVAQAOQAQANAVAAQAWAAAQgQQAQgQgBgWQAAgbgNgRQgPgRgaAAQgiAAgNAYIgQAAQAHgTASgKQAQgKAWAAQAhAAASATQAWAWABAkQACAjgVAYQgUAZgiAAQgSAAgPgIgAgUgGQAAAHAGAEQAHAFAHAAQAJAAAHgGQAIgFAAgIIgBgEQgBAFgGAEQgGADgHAAQgRAAgDgPIgDAKgAgDgXQAAAEADACQABADAEAAQAEAAACgDQADgCAAgEQAAgDgDgDQgCgDgEAAQgIAAAAAJg");
	this.shape_174.setTransform(555.2,84.5);

	this.shape_175 = new cjs.Shape();
	this.shape_175.graphics.f("#FFFFFF").s().p("AgDBTQgbgBgQgRQgRgRAAgZQAAgXANgRQgDgBgFgFQgEgFgBgFQgCgHgBgFQAAgMALgMQALgNATAAQAGAAAFACQAMADACAJIAHgKIAZAAQgFAKgJAKQgKAKgKAHQARgBAOAEQAlAOAAAsQAAAcgUASQgSARgcAAIgDAAgAgigOQgKANAAAPQAAAPAKAKQANANAWAAQARAAAMgJQAQgMAAgTQAAgJgDgHQgEgMgMgGQgLgGgOAAQgXAAgNAOgAgrg0QgEAIAAAFQAAAJAGADIAFgEIAMgLQgGAAgFgEQgFgEAAgFIgDADgAgbg8QAAAIAIAAQAHAAAAgIQAAgIgIAAQgIAAABAIg");
	this.shape_175.setTransform(540.8,84.3);

	this.shape_176 = new cjs.Shape();
	this.shape_176.graphics.f("#FFFFFF").s().p("AAoAdQAKgHAAgKIAAgDQgCgHgQgEQgOgDgSAAQgRAAgOAEQgQADgDAHIAAAEQAAAKAKAGIgOAAQgMgEAAgRQAAgGACgDQAIgQASgGQAOgFAYAAQA1AAAMAbQACADgBAGQAAAQgLAFg");
	this.shape_176.setTransform(526.2,78.4);

	this.shape_177 = new cjs.Shape();
	this.shape_177.graphics.f("#FFFFFF").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_177.setTransform(525.4,86.3);

	this.shape_178 = new cjs.Shape();
	this.shape_178.graphics.f("#FFFFFF").s().p("AgxBDQgUgTABgdQAAgUAIgOQAKgQAQAAQAQAAADANQAFgPAQAAQAMAAAKALQAIAKAAANQAAAQgKALQgMALgOAAQgNAAgKgIQgKgHAAgMIACgTQACgIgJAAQgFAAgEAJQgEAHAAAJQAAAVAQANQAPALAUAAQAWAAAOgOQAPgPAAgWQAAgQgHgMQgIgNgQAAIhTAAIAAgNQAVABAAgCQABgBgGgCQgEgCAAgFQAAgQATgFQAJgDAXAAQASAAALAEQAQAEAKAMQAIAJAAAOIgNAAIgBgBQgCgMgPgIQgMgGgTAAQgNAAgGACQgJADAAAHQAAADADACQACABADAAIAnAAQANAAAIAEIAIAEIAAABIAAAAIABAAQATAQAAAlQAAAggTAWQgUAVgeAAQgdAAgUgTgAgRADQAAAGAFAFQAGAGAHAAQAHAAAGgFQAHgGAAgIQgHAJgKAAQgMABgIgNIgBAFgAAAgRQgCACAAAEQAAADACACQACACADAAQACAAADgCQADgCgBgDQABgEgDgCQgCgCgDAAQgDAAgCACgAAxgqIAAAAIAAgBIABABgAAxgrIAAAAgAAxgrIAAAAgAAxgrIAAAAgAApgvQgIgEgNAAIgnAAQgDAAgCgBQgDgCAAgDQAAgHAJgDQAGgCANAAQATAAAMAGQAPAIACAMIgIgEg");
	this.shape_178.setTransform(502,84);

	this.shape_179 = new cjs.Shape();
	this.shape_179.graphics.f("#FFFFFF").s().p("AgcA/QgVgBgOgOQgOgOgBgUQgBgSAGgNIAPAAQgBAFACACQABACAFAAQAHAAAGgEQALgHADgJIABgFQAAgKgHgDQgGgCgFACQAJADAAALQAAAGgFAEQgEAEgHAAIgGgBQgLgDAAgOIACgJQAGgRAXAAQAJAAAHAFQAHAEACAHQAMgRAaABQAbACAOAZQAJARAAATQgBAqgeAPQgKAFgLAAIgOgCIAAgKQgKAHgIADQgHACgLAAIgFAAgAAOgPQAOALAAATQAAATgMANIAKABQALAAAGgEQAPgMAAgYQAAgQgLgMQgKgMgQgBQgTgBgJAMIACAAQALAAAIAHgAg+AJQAAAKAGAGQAHAKASAAIAKAAQAMgCAIgIQAGgIAAgKQAAgGgDgDQgDgHgKgBQgFAAgIAFIgNAHQgGADgIAAQgGAAgEgDIgBAHg");
	this.shape_179.setTransform(486.5,86.3);

	this.shape_180 = new cjs.Shape();
	this.shape_180.graphics.f("#FFFFFF").s().p("AAjA/QgWAAgKgPQgJAPgYAAQgSAAgMgOQgMgOAAgSQAAgUAOgLIgTAAIAAgOIATAAQgEgCAAgHIABgHQAHgSAaAAQAXAAAHAPQAKgPAVAAQANAAAIAFQAKAHABALQABAKgEAFIgDAAQATAQAAAWQAAAUgMAOQgMAPgSAAIgBAAgAAKAEIAAANQAAAIAIAEQAHADAJAAQAKAAAIgHQAIgIgBgKQAAgJgHgGQgHgGgKAAIg/AAQgKAAgIAHQgHAGAAAJQgBALAHAHQAIAGALAAQAJAAAHgDQAIgEAAgHIAAgOgAAegvQgIAAgFAGQgFAFgBAIIAhAAQADgDgBgFQgBgLgNAAIgCAAgAgkgtQAEADABAFQABAFgDAEIAbAAQgBgKgIgGQgFgDgHAAQgFAAgEACg");
	this.shape_180.setTransform(462.6,86.3);

	this.shape_181 = new cjs.Shape();
	this.shape_181.graphics.f("#FFFFFF").s().p("Ag0A9QgTgTABgfQAAgVAQgQQAPgPAXAAIAQAAIAAAPIgOAAQgRAAgKAKQgKAKgBAOQAAATAQAMQAQALAUAAQAXAAAPgRQAPgPAAgXQAAgXgQgSQgPgSgWgBQgQAAgOAHQgOAHgGANIgOAAQAHgUARgLQAQgKAYAAQAgAAAUAXQAUAXAAAiQAAAigUAWQgVAXggAAQggAAgTgTg");
	this.shape_181.setTransform(447.5,84.5);

	this.shape_182 = new cjs.Shape();
	this.shape_182.graphics.f("#FFFFFF").s().p("AgcA6IAAgOQAEADAIAAQAIAAAFgOQAFgOAAgRQAAgRgFgOQgFgQgHAAQgIgBgFAEIAAgPQAHgFAMAAQARAAALAWQAKATAAAXQAAAXgKASQgMAUgQAAQgLAAgIgFg");
	this.shape_182.setTransform(436.9,86.3);

	this.shape_183 = new cjs.Shape();
	this.shape_183.graphics.f("#FFFFFF").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_183.setTransform(425.7,86.3);

	this.shape_184 = new cjs.Shape();
	this.shape_184.graphics.f("#FFFFFF").s().p("AgoAtQgRgSgBgaQABgcARgRQASgSAaAAQAdAAANAMQAJAJABAMIgPAAQAAgJgLgFQgLgEgNAAQgSAAgMALQgPANAAARQAAARANALQANAKAQAAQAPAAAKgHQALgIAAgOQABgRgRgEQAHAFAAAMQgBAHgHAGQgIAHgJAAQgLAAgHgHQgIgHAAgLQAAgMAKgIQAKgGAMAAQAUAAAOALQAOALgBASQAAAYgQAPQgQAQgXAAQgZAAgSgSgAgDgOQgCADgBAEQABAFACACQADADAEAAQAFAAACgDQADgCAAgEQAAgFgDgDQgCgDgFAAQgEAAgDADg");
	this.shape_184.setTransform(411.1,86.3);

	this.shape_185 = new cjs.Shape();
	this.shape_185.graphics.f("#FFFFFF").s().p("AgQAeIAAgZIgLAAIAAgLIALAAIAAgQIgLAAIAAgLIAgAAQAKAAAGAEQAHAHAAAKQAAAJgIADQgJAFgMAAIAAAdgAgBgVIAAAPQALAAAAgHQAAgJgJAAIgCABg");
	this.shape_185.setTransform(394.3,77.8);

	this.shape_186 = new cjs.Shape();
	this.shape_186.graphics.f("#FFFFFF").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_186.setTransform(388.6,86.3);

	this.shape_187 = new cjs.Shape();
	this.shape_187.graphics.f("#FFFFFF").s().p("AgzBGQgUgNAAgYQAAgWAOgJIgNAAIAAgLIARAAQgEgDAAgGQAAgIAHgFQALgHASABQAWACAJANQAFAIABAQIgjAAQgeABgCASQgCAQARAIQAPAHAUgCQAUgBAOgMQANgLAEgTIABgKQAAgRgJgPQgKgPgOgHIgGAHQgFAFgDABQgKAFgKAAQgQgBgFgGQgFgGADgKQACgFgJAAIgFABIAAgNIAZgBQAQAAALACQAdAGATAUQAWAXAAAkQABAfgVAVQgVAWghAAQgdAAgTgLgAgegWQADABABAEQAAAEgDADIAbAAQgBgHgMgFIgIgBIgHABgAgYg7QABAFAJABQAMAAAFgJQgJgEgSAAIAAAHg");
	this.shape_187.setTransform(372.6,84.5);

	this.shape_188 = new cjs.Shape();
	this.shape_188.graphics.f("#FFFFFF").s().p("AgQAeIAAgZIgLAAIAAgLIALAAIAAgQIgLAAIAAgLIAgAAQAKAAAGAEQAHAHAAAKQAAAJgIADQgJAFgMAAIAAAdgAgBgVIAAAPQALAAAAgHQAAgJgJAAIgCABg");
	this.shape_188.setTransform(354.6,77.8);

	this.shape_189 = new cjs.Shape();
	this.shape_189.graphics.f("#FFFFFF").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_189.setTransform(348.9,86.3);

	this.shape_190 = new cjs.Shape();
	this.shape_190.graphics.f("#FFFFFF").s().p("AgzBGQgUgNAAgYQAAgWAOgJIgNAAIAAgLIARAAQgEgDAAgGQAAgIAHgFQALgHASABQAWACAJANQAFAIABAQIgjAAQgeABgCASQgCAQARAIQAPAHAUgCQAUgBAOgMQANgLAEgTIABgKQAAgRgJgPQgKgPgOgHIgGAHQgFAFgDABQgKAFgKAAQgQgBgFgGQgFgGADgKQACgFgJAAIgFABIAAgNIAZgBQAQAAALACQAdAGATAUQAWAXAAAkQABAfgVAVQgVAWghAAQgdAAgTgLgAgegWQADABABAEQAAAEgDADIAbAAQgBgHgMgFIgIgBIgHABgAgYg7QABAFAJABQAMAAAFgJQgKgEgRAAIAAAHg");
	this.shape_190.setTransform(332.9,84.5);

	this.shape_191 = new cjs.Shape();
	this.shape_191.graphics.f("#FFFFFF").s().p("AAoAdQAKgHAAgKIAAgDQgCgHgQgEQgNgDgTAAQgRAAgOAEQgQADgDAHIAAAEQAAAKAJAGIgNAAQgMgEAAgRQABgGACgDQAHgQATgGQAOgFAXAAQA1AAAMAbQABADAAAGQAAAQgLAFg");
	this.shape_191.setTransform(309.9,78.4);

	this.shape_192 = new cjs.Shape();
	this.shape_192.graphics.f("#FFFFFF").s().p("AASAwQANABAKgGQAKgGAEgMQAFgMgBgMQgBgOgKgKQgKgLgOAAQgXACgFAUQAEgDAFAAQAHAAAEADQAOAJgBAWQAAAUgSAMQgPAMgXAAQgWAAgQgOQgPgOAAgVQAAgXARgLQgFgBgDgFQgDgFAAgGQAAgLAKgHQAJgGAMAAQALAAAIAHQAIAGACALQAOgXAcgBQAXgBAPAUQAPATAAAYQAAAbgSARQgRASgcAAgAg2gHQgHAGAAAJQAAAMAMAHQALAGAPAAQANAAAKgFQAKgGABgKQAAgFgCgEQgDgDgEAAQgDAAgCACQgDADAAADIgOAAIAAgLIABgKIgSAAQgKAAgHAGgAgygtQADACADAGQABAFgBAFIAPAAQgBgKgFgEQgEgEgHAAIgEAAg");
	this.shape_192.setTransform(309.3,86.2);

	this.shape_193 = new cjs.Shape();
	this.shape_193.graphics.f("#FFFFFF").s().p("AgiBJQgPgHgJgNQgNgTAAgbQAAgTALgQQAKgQAPABQAOABAEAKQAHgMAPAAQAOAAAJAKQAKALABAOQABAQgNAMQgMANgQAAQgQAAgKgKQgLgKABgOQAAgFACgFIAEgIQAAgDgCgCQgCgCgDAAQgHABgEAKQgEAIAAAKQgBAVAQAOQAQANAVAAQAWAAAQgQQAQgQgBgWQAAgbgNgRQgPgRgaAAQgiAAgNAYIgQAAQAHgTASgKQAQgKAWAAQAhAAASATQAWAWABAkQACAjgVAYQgUAZgiAAQgSAAgPgIgAgUgGQAAAHAGAEQAHAFAHAAQAJAAAHgGQAIgFAAgIIgBgEQgBAFgGAEQgGADgHAAQgRAAgDgPIgDAKgAgDgXQAAAEADACQABADAEAAQAEAAACgDQADgCAAgEQAAgDgDgDQgCgDgEAAQgIAAAAAJg");
	this.shape_193.setTransform(293.2,84.5);

	this.shape_194 = new cjs.Shape();
	this.shape_194.graphics.f("#FFFFFF").s().p("AgnAtQgTgSABgaQgBgcATgRQARgSAaAAQAdAAANAMQAJAJABAMIgOAAQgBgJgMgFQgKgEgNAAQgRAAgOALQgNANAAARQAAARAMALQAMAKARAAQAOAAALgHQALgIAAgOQAAgRgPgEQAFAFAAAMQABAHgIAGQgIAHgIAAQgLAAgIgHQgIgHABgLQgBgMAKgIQAKgGAMAAQATAAAOALQAPALAAASQgBAYgQAPQgQAQgXAAQgZAAgRgSgAgCgOQgEADAAAEQAAAFAEACQACADAEAAQAEAAADgDQAEgCAAgEQAAgFgEgDQgDgDgDAAQgFAAgCADg");
	this.shape_194.setTransform(279.5,86.3);

	this.shape_195 = new cjs.Shape();
	this.shape_195.graphics.f("#FFFFFF").s().p("AgKAOQgHgFAAgJQAAgFAEgFQAFgHAIAAQAGAAAFAEQAHAFAAAIQAAAGgEAFQgFAHgJAAQgFAAgFgEg");
	this.shape_195.setTransform(262.5,91);

	this.shape_196 = new cjs.Shape();
	this.shape_196.graphics.f("#FFFFFF").s().p("AhIAkQgEgMADgJIgLAAIAAgMIAJgJIALgMQgIAAgEgFQgEgFAAgIQAAgLAJgHQAIgHAMgBQATgBAJAOQARgNAaAAIAJAAQAZACAQATQAQAUgBAYQgCAZgQARQgQASgZAAIgJgBIAAgMQgQAMgYABIgDAAQgmAAgIgbgAgHgkQAHABAHAFQAIAEAFAFQAMANAAAQQABAYgNAPIAFABQALAAAIgFQAQgLACgVQABgNgHgNQgGgMgNgGQgLgFgQAAQgKAAgHACgAgrgQQgJAHgOANIAKAAQgEAJACAJQAGARAagBQAOgBALgJQAJgJABgNQABgLgJgJQgHgJgNAAIgCAAQgMAAgKAHgAg0gvIgGADQAGABACADQACAFgBAFIAHgGIAGgFQgDgEgGgCIgDAAIgEAAg");
	this.shape_196.setTransform(249.9,86.3);

	this.shape_197 = new cjs.Shape();
	this.shape_197.graphics.f("#FFFFFF").s().p("AgbAzQgLgJAAgSQAAgNAFgKQADgFAJgKQAJgLAZgVIgwAAIAAgNIBHAAIAAANIgQANQgIAGgHAGQgJAMgDAFQgHAJABALQAAAHAGAEQAEADAGAAQANABAEgLIABgHQAAgFgDgFIAPAAQAHALgBANQgBAOgMAKQgLAJgPAAQgQAAgLgJg");
	this.shape_197.setTransform(237,86.5);

	this.shape_198 = new cjs.Shape();
	this.shape_198.graphics.f("#FFFFFF").s().p("AAVBMIAAgcQgSACgOgBQgcgBgRgMQgTgOAAgZQAAgMAHgKQAHgKAJgFQgFgBgCgEQgCgDAAgEQAAgIAHgGQAMgJAWAAQAiAAAJAUQACgJAHgGQAIgFAJAAIAOAAIAAAOIgCAAQgFAAgBAEQAAACAFAEQAHAFADAEQAGAGAAAJQABAPgOAHQgLAGgPgBIAAAfIANgDIAJgDIAAANIgJAFIgNADIAAAegAgigWQgIAJAAANQABAaAeAIIAPABQALAAAGgBIAAhCIgiAAQgNAAgIAKgAAlglIAAAYIADAAQAGAAAEgDQAEgEAAgGQAAgHgGgIQgGgJAAgEIAAgDQgFACAAASgAAJgtQgCgJgLgFQgNgEgMAEQADAEAAADQAAAFgDACIADAAIADAAIAgAAIAAAAg");
	this.shape_198.setTransform(224.9,87.6);

	this.shape_199 = new cjs.Shape();
	this.shape_199.graphics.f("#FFFFFF").s().p("AgcA6IAAgOQAEADAIAAQAIAAAFgOQAFgOAAgRQAAgRgFgOQgFgQgHAAQgIgBgFAEIAAgPQAHgFAMAAQARAAALAWQAKATAAAXQAAAXgKASQgMAUgQAAQgLAAgIgFg");
	this.shape_199.setTransform(1157.7,56.2);

	this.shape_200 = new cjs.Shape();
	this.shape_200.graphics.f("#FFFFFF").s().p("AgzBCQgVgPABgbQABgXATgJQgKgCAAgKQAAgOAOgGQAKgFAOABQARAAAKAOQAKANAAARIgrAAQgJAAgHAFQgHAGABAJQABAWAdAGQAJACAMAAQAYAAAPgQQAOgPABgXQAAgYgOgSQgPgTgcAAQgggBgRAYIgQAAQAIgSASgKQARgJAWAAQAhAAAVAXQAVAXAAAhQAAAhgTAWQgUAZghAAQggAAgTgOgAgigcQADACABADQACAGgCAFIAbAAQgCgHgFgGQgGgFgJAAQgFAAgEACg");
	this.shape_200.setTransform(1147.5,54.5);

	this.shape_201 = new cjs.Shape();
	this.shape_201.graphics.f("#FFFFFF").s().p("AgQAeIAAgZIgLAAIAAgLIALAAIAAgQIgLAAIAAgLIAgAAQAKAAAGAEQAHAHAAAJQAAAJgIAFQgJAFgMgBIAAAdgAgBgVIAAAPQALAAAAgHQAAgJgJAAIgCABg");
	this.shape_201.setTransform(1137.1,47.7);

	this.shape_202 = new cjs.Shape();
	this.shape_202.graphics.f("#FFFFFF").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_202.setTransform(1131.4,56.2);

	this.shape_203 = new cjs.Shape();
	this.shape_203.graphics.f("#FFFFFF").s().p("AADBNQgLgGAAgMQgBgJAHgFQgaABgSgPQgSgOACgWQABgbAWgMQgDgBgDgEQgDgEAAgFIABgFQAFgKAMgEQAIgDAQAAQAdAAANAPQAJAMABARIgtAAQgNAAgJAIQgJAJgBAMQgBAPAMALQAMAKAQABQAZABASgKIAAAPIgRAGQgQAEAAAGQgBAFAFADQAEADAGAAQAHAAAGgEQAFgDAAgHIgBgGIAOAAIABAHQAAANgLAJQgKAHgPABIgGABQgKAAgJgEgAgTg/IADACQABAAAAABQAAAAABABQAAAAAAABQAAAAABABQAAAFgEACIAHAAIAkAAQgFgKgLgDQgGgCgJAAQgJAAgFACg");
	this.shape_203.setTransform(1117.2,58);

	this.shape_204 = new cjs.Shape();
	this.shape_204.graphics.f("#FFFFFF").s().p("AApAdQAJgHABgKIgBgDQgCgHgPgEQgOgDgTAAQgSAAgNAEQgQADgDAHIAAAEQAAAKAJAGIgNAAQgLgEAAgRQAAgGABgDQAIgQATgGQAOgFAXAAQA1AAALAbQACADABAGQAAAQgMAFg");
	this.shape_204.setTransform(1095,48.3);

	this.shape_205 = new cjs.Shape();
	this.shape_205.graphics.f("#FFFFFF").s().p("AASAwQANABAKgGQAKgGAEgMQAFgMgBgMQgBgOgKgKQgKgLgOAAQgXACgFAUQAEgDAFAAQAHAAAEADQAOAJgBAWQAAAUgSAMQgPAMgXAAQgWAAgQgOQgPgOAAgVQAAgXARgLQgFgCgDgEQgDgFAAgGQAAgLAKgHQAJgGAMAAQALAAAIAHQAIAGACALQAOgXAcgBQAXgBAPAUQAPATAAAYQAAAbgSARQgRASgcAAgAg2gHQgHAGAAAJQAAAMAMAHQALAGAPAAQANAAAKgFQAKgGABgKQAAgFgCgEQgDgDgEAAQgDAAgCACQgDADAAADIgOAAIAAgLIABgKIgSAAQgKAAgHAGgAgygtQADACADAGQABAFgBAFIAPAAQgBgKgFgEQgEgEgHAAIgEAAg");
	this.shape_205.setTransform(1094.4,56.2);

	this.shape_206 = new cjs.Shape();
	this.shape_206.graphics.f("#FFFFFF").s().p("AgiBJQgPgHgJgNQgNgTAAgbQAAgTALgQQAKgQAPABQAOABAEAKQAHgMAPAAQAOAAAJAKQAKALABAOQABAQgNAMQgMANgQAAQgQAAgKgKQgLgKABgOQAAgFACgFIAEgIQAAgDgCgCQgCgCgDAAQgHABgEAKQgEAIAAAKQgBAVAQAOQAQANAVAAQAWAAAQgQQAQgQgBgWQAAgbgNgRQgPgRgaAAQgiAAgNAYIgQAAQAHgTASgKQAQgKAWAAQAhAAASATQAWAWABAkQACAjgVAYQgUAZgiAAQgSAAgPgIgAgUgGQAAAHAGAEQAHAFAHAAQAJAAAHgGQAIgFAAgIIgBgEQgBAFgGAEQgGADgHAAQgRAAgDgPIgDAKgAgDgXQAAAEADACQABADAEAAQAEAAACgDQADgCAAgEQAAgDgDgDQgCgDgEAAQgIAAAAAJg");
	this.shape_206.setTransform(1078.3,54.4);

	this.shape_207 = new cjs.Shape();
	this.shape_207.graphics.f("#FFFFFF").s().p("AgnAtQgTgSABgaQgBgcATgRQARgSAaAAQAdAAANAMQAJAJABAMIgOAAQgBgJgLgFQgLgEgNAAQgSAAgMALQgOANAAARQgBARANALQAMAKASAAQANAAALgHQAMgIAAgOQgBgRgQgEQAGAFABAMQAAAHgIAGQgHAHgJAAQgLAAgIgHQgHgHgBgLQAAgMAKgIQAJgGANAAQAUAAANALQAOALABASQAAAYgRAPQgQAQgXAAQgZAAgRgSgAgCgOQgDADAAAEQAAAFADACQACADAEAAQAEAAAEgDQACgCAAgEQAAgFgCgDQgEgDgDAAQgFAAgCADg");
	this.shape_207.setTransform(1064.6,56.2);

	this.shape_208 = new cjs.Shape();
	this.shape_208.graphics.f("#FFFFFF").s().p("AgQAeIAAgZIgLAAIAAgLIALAAIAAgQIgLAAIAAgLIAgAAQAKAAAGAEQAHAHAAAJQAAAJgIAFQgJAFgMgBIAAAdgAgBgVIAAAPQALAAAAgHQAAgJgJAAIgCABg");
	this.shape_208.setTransform(1047.9,47.7);

	this.shape_209 = new cjs.Shape();
	this.shape_209.graphics.f("#FFFFFF").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_209.setTransform(1042.1,56.2);

	this.shape_210 = new cjs.Shape();
	this.shape_210.graphics.f("#FFFFFF").s().p("AgcA/QgVgBgOgOQgOgOgBgUQgBgSAGgNIAPAAQgBAFACACQABACAFAAQAHAAAGgEQALgHADgJIABgFQAAgKgHgDQgGgCgFACQAJADAAALQAAAGgFAEQgEAEgHAAIgGgBQgLgDAAgOIACgJQAGgRAXAAQAJAAAHAFQAHAEACAHQAMgRAaABQAbACAOAZQAJARAAATQgBAqgeAPQgKAFgLAAIgOgCIAAgKQgKAHgIADQgHACgLAAIgFAAgAAOgPQAOALAAATQAAATgMANIAKABQALAAAGgEQAPgMAAgYQAAgQgLgMQgKgMgQgBQgTgBgJAMIACAAQALAAAIAHgAg+AJQAAAKAGAGQAHAKASAAIAKAAQAMgCAIgIQAGgIAAgKQAAgGgDgDQgDgHgKgBQgFAAgIAFIgNAHQgGADgIAAQgGAAgEgDIgBAHg");
	this.shape_210.setTransform(1025.4,56.2);

	this.shape_211 = new cjs.Shape();
	this.shape_211.graphics.f("#FFFFFF").s().p("AASAwQANABAKgGQAKgGAEgMQAFgMgBgMQgBgOgKgKQgKgLgOAAQgXACgFAUQAEgDAFAAQAHAAAEADQAOAJgBAWQAAAUgSAMQgPAMgXAAQgWAAgQgOQgPgOAAgVQAAgXARgLQgFgCgDgEQgDgFAAgGQAAgLAKgHQAJgGAMAAQALAAAIAHQAIAGACALQAOgXAcgBQAXgBAPAUQAPATAAAYQAAAbgSARQgRASgcAAgAg2gHQgHAGAAAJQAAAMAMAHQALAGAPAAQANAAAKgFQAKgGABgKQAAgFgCgEQgDgDgEAAQgDAAgCACQgDADAAADIgOAAIAAgLIABgKIgSAAQgKAAgHAGgAgygtQADACADAGQABAFgBAFIAPAAQgBgKgFgEQgEgEgHAAIgEAAg");
	this.shape_211.setTransform(1008.9,56.2);

	this.shape_212 = new cjs.Shape();
	this.shape_212.graphics.f("#FFFFFF").s().p("Ag0A9QgTgTABgfQAAgVAQgQQAPgPAXAAIAQAAIAAAPIgOAAQgRAAgKAKQgKAKgBAOQAAATAQAMQAQALAUAAQAXAAAPgRQAPgPAAgXQAAgXgQgSQgPgSgWgBQgQAAgOAHQgOAHgGANIgOAAQAHgUARgLQAQgKAYAAQAgAAAUAXQAUAXAAAiQAAAigUAWQgVAXggAAQggAAgTgTg");
	this.shape_212.setTransform(993,54.5);

	this.shape_213 = new cjs.Shape();
	this.shape_213.graphics.f("#FFFFFF").s().p("AAjA/QgWAAgKgPQgJAPgYAAQgSAAgMgOQgMgOAAgSQAAgUAOgLIgTAAIAAgOIATAAQgEgCAAgHIABgHQAHgSAaAAQAXAAAHAPQAKgPAVAAQANAAAIAFQAKAHABALQABAKgEAFIgDAAQATAQAAAWQAAAUgMAOQgMAPgSAAIgBAAgAAKAEIAAANQAAAIAIAEQAHADAJAAQAKAAAIgHQAIgIgBgKQAAgJgHgGQgHgGgKAAIg/AAQgKAAgIAHQgHAGAAAJQgBALAHAHQAIAGALAAQAJAAAHgDQAIgEAAgHIAAgOgAAegvQgIAAgFAGQgFAFgBAIIAhAAQADgDgBgFQgBgLgNAAIgCAAgAgkgtQAEADABAFQABAFgDAEIAbAAQgBgKgIgGQgFgDgHAAQgFAAgEACg");
	this.shape_213.setTransform(977.6,56.2);

	this.shape_214 = new cjs.Shape();
	this.shape_214.graphics.f("#FFFFFF").s().p("Ag6BJQgQgGgGAAQgGAAgHAJIgLgHIAWghQgKgMAAgTQAAgHACgGQAFgUASgKQANgIASAAIAJAAIAVgfIAQAAIgVAgIALAFIAJAHQADgFADgDQAHgFAJAAQALAAAHAIQAJgJAMABQAhABACA5QAAAbgJASQgMAVgXAAQgQAAgIgIQgJgJAAgOIABgHIAOAAIAAAEQAAASAQAAQAKAAAEgQQACgJABgVQAAgQgCgMQgDgQgHAAQgKAAAAAOIAAAeIgPAAIAAgeQAAgOgJgBQgEgBgDAEQgDACAAAEQAMAOgBAXQgBAcgQAQQgQAQgdABIgBAAQgLAAgPgFgAg9A5QAMAFALAAIALgBQAPgDAKgLQAIgMAAgQQAAgPgJgKQgKgMgPgBIgOATIAIAAQAJAAAGAEQAGAGABAIQABALgFAHQgFAIgKADQgHADgJAAQgOAAgKgGIgIALIAGAAQAGAAAGACgAg+AfQACADAFABQAKACADgCQgIgCgDgFQgCgFABgFgAgpAUQAAAHAHAAQAHAAAAgHQAAgHgHAAQgHAAAAAHgAhEgFQgFAGAAAKQAAAGABAEIAbgnQgQACgHALg");
	this.shape_214.setTransform(951.5,54.7);

	this.shape_215 = new cjs.Shape();
	this.shape_215.graphics.f("#FFFFFF").s().p("AgEBPQgbgBgRgQQgQgRAAgaQgBgRAHgMQAGgMAOgNQAKgIASgSQAJgLADgHIAZAAQgEAJgIAJQgHAIgLAIQARgBANAFQAmAOAAAsQAAAbgUASQgTASgaAAIgEgBgAgjgRQgKAMAAAPQAAAPAKAKQANAOAWAAQARAAAMgJQAQgNAAgTQAAgIgDgIQgFgLgLgHQgLgGgOAAQgXAAgNAPg");
	this.shape_215.setTransform(934.6,54.6);

	this.shape_216 = new cjs.Shape();
	this.shape_216.graphics.f("#FFFFFF").s().p("AAoAdQALgHgBgKIAAgDQgCgHgQgEQgOgDgSAAQgRAAgOAEQgQADgDAHIAAAEQAAAKAKAGIgOAAQgMgEAAgRQAAgGACgDQAIgQASgGQAOgFAYAAQA1AAAMAbQABADAAAGQAAAQgLAFg");
	this.shape_216.setTransform(919.9,48.3);

	this.shape_217 = new cjs.Shape();
	this.shape_217.graphics.f("#FFFFFF").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_217.setTransform(919.2,56.2);

	this.shape_218 = new cjs.Shape();
	this.shape_218.graphics.f("#FFFFFF").s().p("AgcA/QgVgBgOgOQgOgOgBgUQgBgSAGgNIAPAAQgBAFACACQABACAFAAQAHAAAGgEQALgHADgJIABgFQAAgKgHgDQgGgCgFACQAJADAAALQAAAGgFAEQgEAEgHAAIgGgBQgLgDAAgOIACgJQAGgRAXAAQAJAAAHAFQAHAEACAHQAMgRAaABQAbACAOAZQAJARAAATQgBAqgeAPQgKAFgLAAIgOgCIAAgKQgKAHgIADQgHACgLAAIgFAAgAAOgPQAOALAAATQAAATgMANIAKABQALAAAGgEQAPgMAAgYQAAgQgLgMQgKgMgQgBQgTgBgJAMIACAAQALAAAIAHgAg+AJQAAAKAGAGQAHAKASAAIAKAAQAMgCAIgIQAGgIAAgKQAAgGgDgDQgDgHgKgBQgFAAgIAFIgNAHQgGADgIAAQgGAAgEgDIgBAHg");
	this.shape_218.setTransform(894.8,56.2);

	this.shape_219 = new cjs.Shape();
	this.shape_219.graphics.f("#FFFFFF").s().p("AgzBCQgVgPABgbQABgXATgJQgKgCAAgKQAAgOAOgGQAKgFAOABQARAAAKAOQAKANAAARIgrAAQgJAAgHAFQgHAGABAJQABAWAdAGQAJACAMAAQAYAAAPgQQAOgPABgXQAAgYgOgSQgPgTgcAAQgggBgRAYIgQAAQAIgSASgKQARgJAWAAQAhAAAVAXQAVAXAAAhQAAAhgTAWQgUAZghAAQggAAgTgOgAgigcQADACABADQACAGgCAFIAbAAQgCgHgFgGQgGgFgJAAQgFAAgEACg");
	this.shape_219.setTransform(879.1,54.5);

	this.shape_220 = new cjs.Shape();
	this.shape_220.graphics.f("#FFFFFF").s().p("AgQAeIAAgZIgLAAIAAgLIALAAIAAgQIgLAAIAAgLIAgAAQAKAAAGAEQAHAHAAAJQAAAJgIAFQgJAFgMgBIAAAdgAgBgVIAAAPQALAAAAgHQAAgJgJAAIgCABg");
	this.shape_220.setTransform(868.7,47.7);

	this.shape_221 = new cjs.Shape();
	this.shape_221.graphics.f("#FFFFFF").s().p("AgcA/QgVgBgOgOQgOgOgBgUQgBgSAGgNIAPAAQgBAFACACQABACAFAAQAHAAAGgEQALgHADgJIABgFQAAgKgHgDQgGgCgFACQAJADAAALQAAAGgFAEQgEAEgHAAIgGgBQgLgDAAgOIACgJQAGgRAXAAQAJAAAHAFQAHAEACAHQAMgRAaABQAbACAOAZQAJARAAATQgBAqgeAPQgKAFgLAAIgOgCIAAgKQgKAHgIADQgHACgLAAIgFAAgAAOgPQAOALAAATQAAATgMANIAKABQALAAAGgEQAPgMAAgYQAAgQgLgMQgKgMgQgBQgTgBgJAMIACAAQALAAAIAHgAg+AJQAAAKAGAGQAHAKASAAIAKAAQAMgCAIgIQAGgIAAgKQAAgGgDgDQgDgHgKgBQgFAAgIAFIgNAHQgGADgIAAQgGAAgEgDIgBAHg");
	this.shape_221.setTransform(863.3,56.2);

	this.shape_222 = new cjs.Shape();
	this.shape_222.graphics.f("#FFFFFF").s().p("Ag3AtQgRgPAAgZQAAgJAEgKQAFgJAHgFQgHgDAAgJIABgHQAGgPAbAAQAVAAAIANQAJgNAXAAQALAAAIAFQAKAGABAJQABAIgEAGQAGAFAEAJQAEAKAAAJQAAAagSAPQgVARgiAAQgjAAgUgSgAgygNQgGAGABAKQABAQATAHQAOAGAVAAQAUAAAPgGQATgHAAgQQABgKgGgGQgGgGgJgBIhEAAQgKABgGAGgAAQgsQgHAEgBAHIAjAAIAAgEQAAgEgEgEQgFgDgFAAIgBAAQgHAAgFAEgAgngvQAEADABAEQAAAEgCADIAaAAQgBgMgPgDIgGAAQgEAAgDABg");
	this.shape_222.setTransform(847.4,56.3);

	this.shape_223 = new cjs.Shape();
	this.shape_223.graphics.f("#FFFFFF").s().p("AgoAtQgRgSgBgaQABgcARgRQASgSAaAAQAdAAANAMQAJAJABAMIgPAAQAAgJgMgFQgKgEgNAAQgRAAgNALQgPANAAARQAAARANALQANAKAQAAQAPAAAKgHQALgIAAgOQAAgRgQgEQAHAFgBAMQAAAHgHAGQgIAHgJAAQgLAAgHgHQgIgHAAgLQAAgMAKgIQAKgGAMAAQAUAAAOALQANALAAASQAAAYgQAPQgQAQgXAAQgZAAgSgSgAgDgOQgDADABAEQgBAFADACQADADAEAAQAFAAACgDQADgCAAgEQAAgFgDgDQgDgDgEAAQgEAAgDADg");
	this.shape_223.setTransform(833.6,56.2);

	this.shape_224 = new cjs.Shape();
	this.shape_224.graphics.f("#FFFFFF").s().p("AgQAeIAAgZIgLAAIAAgLIALAAIAAgQIgLAAIAAgLIAgAAQAKAAAGAEQAHAHAAAJQAAAJgIAFQgJAFgMgBIAAAdgAgBgVIAAAPQALAAAAgHQAAgJgJAAIgCABg");
	this.shape_224.setTransform(816.9,47.7);

	this.shape_225 = new cjs.Shape();
	this.shape_225.graphics.f("#FFFFFF").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_225.setTransform(811.1,56.2);

	this.shape_226 = new cjs.Shape();
	this.shape_226.graphics.f("#FFFFFF").s().p("AgzBCQgVgPABgbQABgXATgJQgKgCAAgKQAAgOAOgGQAKgFAOABQARAAAKAOQAKANAAARIgrAAQgJAAgHAFQgHAGABAJQABAWAdAGQAJACAMAAQAYAAAPgQQAOgPABgXQAAgYgOgSQgPgTgcAAQgggBgRAYIgQAAQAIgSASgKQARgJAWAAQAhAAAVAXQAVAXAAAhQAAAhgTAWQgUAZghAAQggAAgTgOgAgigcQADACABADQACAGgCAFIAbAAQgCgHgFgGQgGgFgJAAQgFAAgEACg");
	this.shape_226.setTransform(795.1,54.5);

	this.shape_227 = new cjs.Shape();
	this.shape_227.graphics.f("#FFFFFF").s().p("AgcA6IAAgOQAEADAIAAQAIAAAFgOQAFgOAAgRQAAgRgFgOQgFgQgHAAQgIgBgFAEIAAgPQAHgFAMAAQARAAALAWQAKATAAAXQAAAXgKASQgMAUgQAAQgLAAgIgFg");
	this.shape_227.setTransform(784.4,56.2);

	this.shape_228 = new cjs.Shape();
	this.shape_228.graphics.f("#FFFFFF").s().p("AgzBCQgUgTACggQABgVAPgNQAPgNAWAAIAWAAIAAAOIgTAAQgQAAgKAIQgMAJAAAPQAAARAPALQAQAKAUgBQAYgBAMgPQANgOAAgWIAAgJQgDgcgUgQQgNAPgWABQgWAAgIgMQgBgDAAgFQAAgGACgDIgJAAIAAgPIAQAAQAkAAAVAKQAWALALAXQAMAYgBAbQgCAegTATQgUAVgfgBQgfAAgSgQgAgWhAQAAAEAEADQAEADAFAAQAOABAGgJQgHgCgJgBIgQgCIgBADg");
	this.shape_228.setTransform(774.5,54.3);

	this.shape_229 = new cjs.Shape();
	this.shape_229.graphics.f("#FFFFFF").s().p("AgQAeIAAgZIgLAAIAAgLIALAAIAAgQIgLAAIAAgLIAgAAQAKAAAGAEQAHAHAAAJQAAAJgIAFQgJAFgMgBIAAAdgAgBgVIAAAPQALAAAAgHQAAgJgJAAIgCABg");
	this.shape_229.setTransform(764.4,47.7);

	this.shape_230 = new cjs.Shape();
	this.shape_230.graphics.f("#FFFFFF").s().p("AAjA/QgWAAgKgPQgJAPgYAAQgSAAgMgOQgMgOAAgSQAAgUAOgLIgTAAIAAgOIATAAQgEgCAAgHIABgHQAHgSAaAAQAXAAAHAPQAKgPAVAAQANAAAIAFQAKAHABALQABAKgEAFIgDAAQATAQAAAWQAAAUgMAOQgMAPgSAAIgBAAgAAKAEIAAANQAAAIAIAEQAHADAJAAQAKAAAIgHQAIgIgBgKQAAgJgHgGQgHgGgKAAIg/AAQgKAAgIAHQgHAGAAAJQgBALAHAHQAIAGALAAQAJAAAHgDQAIgEAAgHIAAgOgAAegvQgIAAgFAGQgFAFgBAIIAhAAQADgDgBgFQgBgLgNAAIgCAAgAgkgtQAEADABAFQABAFgDAEIAbAAQgBgKgIgGQgFgDgHAAQgFAAgEACg");
	this.shape_230.setTransform(759.1,56.2);

	this.shape_231 = new cjs.Shape();
	this.shape_231.graphics.f("#FFFFFF").s().p("AgzBCQgVgPABgbQABgXATgJQgKgCAAgKQAAgOAOgGQAKgFAOABQARAAAKAOQAKANAAARIgrAAQgJAAgHAFQgHAGABAJQABAWAdAGQAJACAMAAQAYAAAPgQQAOgPABgXQAAgYgOgSQgPgTgcAAQgggBgRAYIgQAAQAIgSASgKQARgJAWAAQAhAAAVAXQAVAXAAAhQAAAhgTAWQgUAZghAAQggAAgTgOgAgigcQADACABADQACAGgCAFIAbAAQgCgHgFgGQgGgFgJAAQgFAAgEACg");
	this.shape_231.setTransform(743.9,54.5);

	this.shape_232 = new cjs.Shape();
	this.shape_232.graphics.f("#FFFFFF").s().p("AAVBMIAAgcQgSACgOgBQgcgBgRgMQgTgOAAgZQAAgMAHgLQAHgJAJgFQgFgBgCgEQgCgDAAgEQAAgIAHgFQAMgKAWAAQAiAAAJAUQACgJAHgGQAIgFAJAAIAOAAIAAAOIgCgBQgFABgBAEQAAACAFAEQAHAFADAEQAGAGAAAJQABAPgOAHQgLAGgPgBIAAAfIANgDIAJgDIAAANIgJAFIgNACIAAAfgAgigWQgIAKAAAMQABAaAeAIIAPABQALAAAGgBIAAhCIgiAAQgNAAgIAKgAAlglIAAAYIADAAQAGAAAEgEQAEgDAAgGQAAgHgGgIQgGgJAAgEIAAgCQgFABAAASgAAJgtQgCgJgLgFQgNgDgMADQADAEAAADQAAAEgDADIADAAIADAAIAgAAIAAAAg");
	this.shape_232.setTransform(728.6,57.6);

	this.shape_233 = new cjs.Shape();
	this.shape_233.graphics.f("#FFFFFF").s().p("AgcA/QgVgBgOgOQgOgOgBgUQgBgSAGgNIAPAAQgBAFACACQABACAFAAQAHAAAGgEQALgHADgJIABgFQAAgKgHgDQgGgCgFACQAJADAAALQAAAGgFAEQgEAEgHAAIgGgBQgLgDAAgOIACgJQAGgRAXAAQAJAAAHAFQAHAEACAHQAMgRAaABQAbACAOAZQAJARAAATQgBAqgeAPQgKAFgLAAIgOgCIAAgKQgKAHgIADQgHACgLAAIgFAAgAAOgPQAOALAAATQAAATgMANIAKABQALAAAGgEQAPgMAAgYQAAgQgLgMQgKgMgQgBQgTgBgJAMIACAAQALAAAIAHgAg+AJQAAAKAGAGQAHAKASAAIAKAAQAMgCAIgIQAGgIAAgKQAAgGgDgDQgDgHgKgBQgFAAgIAFIgNAHQgGADgIAAQgGAAgEgDIgBAHg");
	this.shape_233.setTransform(704.9,56.2);

	this.shape_234 = new cjs.Shape();
	this.shape_234.graphics.f("#FFFFFF").s().p("AgEBPQgbgBgRgQQgQgRAAgaQgBgRAHgMQAGgMAOgNQAKgIASgSQAJgLADgHIAZAAQgEAJgIAJQgHAIgLAIQARgBANAFQAmAOAAAsQAAAbgUASQgTASgaAAIgEgBgAgjgRQgKAMAAAPQAAAPAKAKQANAOAWAAQARAAAMgJQAQgNAAgTQAAgIgDgIQgFgLgLgHQgLgGgOAAQgXAAgNAPg");
	this.shape_234.setTransform(689.9,54.6);

	this.shape_235 = new cjs.Shape();
	this.shape_235.graphics.f("#FFFFFF").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_235.setTransform(674.6,56.2);

	this.shape_236 = new cjs.Shape();
	this.shape_236.graphics.f("#FFFFFF").s().p("AgQAeIAAgZIgLAAIAAgLIALAAIAAgQIgLAAIAAgLIAgAAQAKAAAGAEQAHAHAAAJQAAAJgIAFQgJAFgMgBIAAAdgAgBgVIAAAPQALAAAAgHQAAgJgJAAIgCABg");
	this.shape_236.setTransform(655.5,47.7);

	this.shape_237 = new cjs.Shape();
	this.shape_237.graphics.f("#FFFFFF").s().p("AgcA/QgVgBgOgOQgOgOgBgUQgBgSAGgNIAPAAQgBAFACACQABACAFAAQAHAAAGgEQALgHADgJIABgFQAAgKgHgDQgGgCgFACQAJADAAALQAAAGgFAEQgEAEgHAAIgGgBQgLgDAAgOIACgJQAGgRAXAAQAJAAAHAFQAHAEACAHQAMgRAaABQAbACAOAZQAJARAAATQgBAqgeAPQgKAFgLAAIgOgCIAAgKQgKAHgIADQgHACgLAAIgFAAgAAOgPQAOALAAATQAAATgMANIAKABQALAAAGgEQAPgMAAgYQAAgQgLgMQgKgMgQgBQgTgBgJAMIACAAQALAAAIAHgAg+AJQAAAKAGAGQAHAKASAAIAKAAQAMgCAIgIQAGgIAAgKQAAgGgDgDQgDgHgKgBQgFAAgIAFIgNAHQgGADgIAAQgGAAgEgDIgBAHg");
	this.shape_237.setTransform(650.1,56.2);

	this.shape_238 = new cjs.Shape();
	this.shape_238.graphics.f("#FFFFFF").s().p("AgiBJQgPgHgJgNQgNgTAAgbQAAgTALgQQAKgQAPABQAOABAEAKQAHgMAPAAQAOAAAJAKQAKALABAOQABAQgNAMQgMANgQAAQgQAAgKgKQgLgKABgOQAAgFACgFIAEgIQAAgDgCgCQgCgCgDAAQgHABgEAKQgEAIAAAKQgBAVAQAOQAQANAVAAQAWAAAQgQQAQgQgBgWQAAgbgNgRQgPgRgaAAQgiAAgNAYIgQAAQAHgTASgKQAQgKAWAAQAhAAASATQAWAWABAkQACAjgVAYQgUAZgiAAQgSAAgPgIgAgUgGQAAAHAGAEQAHAFAHAAQAJAAAHgGQAIgFAAgIIgBgEQgBAFgGAEQgGADgHAAQgRAAgDgPIgDAKgAgDgXQAAAEADACQABADAEAAQAEAAACgDQADgCAAgEQAAgDgDgDQgCgDgEAAQgIAAAAAJg");
	this.shape_238.setTransform(634.1,54.4);

	this.shape_239 = new cjs.Shape();
	this.shape_239.graphics.f("#FFFFFF").s().p("AgbA+QgZAAgNgSQgNgRAAgZQAAgpAdgRQAJgGAQAAIAIABIAAAOQARgPAXAAQAaABAOARQAOAQAAAbQABAagPATQgQASgZAAIgJAAIAAgMQgKAHgGADQgIADgMAAIgFgBgAAJgUQAWAGgBAaQAAAUgNAPQANACAKgGQAQgLgBgaQgBgPgKgMQgKgMgPgBQgcgCgEATQAGgEAJAAIAHABgAgygjQgJAMAAAPQAAARAJALQAKAMARAAQANABAKgGQAJgGABgLQABgGgEgEQgEgGgGABQgFAAgDADQgDADAAAFIABAGIgPAAQgDgPACgOQABgOALgPIgJgBQgPAAgJAMg");
	this.shape_239.setTransform(618.5,56.2);

	this.shape_240 = new cjs.Shape();
	this.shape_240.graphics.f("#FFFFFF").s().p("AgtBHQgUgMgCgWQgCgYAUgKQgGgBgDgDQgEgEAAgGIABgGQAHgQAgABQATABAJAOQAJAMgBASIgqAAQgVAAABAPQAAAMAPAGQANAFATAAQAVAAAOgLQAOgLABgRIABgJQAAgRgLgMQgJgLgRAAIgyAAQgXAAAAgQQAAgbA8AAQAkAAANAKQAKAHAAAKQAAAKgJAGQATAVgBAhQAAAdgWASQgUARgcAAQgaAAgRgKgAghgRQADADAAAFQABAFgEAEIAeAAQgCgJgGgEQgGgFgKAAIgGABgAAcg8QgDACAAAEQAAADADADQACACAEAAQADAAADgCQADgDAAgDQAAgEgDgCQgDgDgDAAQgEAAgCADgAgig5QgBAGALAAIAiAAQgBgHADgHIgOgBQgeAAgCAJg");
	this.shape_240.setTransform(595.6,54.4);

	this.shape_241 = new cjs.Shape();
	this.shape_241.graphics.f("#FFFFFF").s().p("AAAAyQgKANgRAAQgYAAgMgSQgLgQABgcQAAgeAUgSQAOgNASgBQAIgBAGACIAAAOQgJgDgMAEQgMAEgIALQgHAMAAAOQAAAlAaAAQAJAAAGgFQAHgGAAgJIAAgNIAOAAIAAAMQAAAKAHAGQAGAFAKAAQALAAAIgIQAHgHAAgMQAAgKgIgIQgHgHgMAAIgkAAQAAgWAJgLQAJgKASABQAhAAAAAaIgBAGIgMAAQAMAFAHALQAHALAAAMQAAAXgMAPQgMAPgUAAQgWAAgJgNgAAKggIAWAAIAJAAIAAgEQAAgLgOAAQgPAAgCAPg");
	this.shape_241.setTransform(573,56.2);

	this.shape_242 = new cjs.Shape();
	this.shape_242.graphics.f("#FFFFFF").s().p("AgcA/QgVgBgOgOQgOgOgBgUQgBgSAGgNIAPAAQgBAFACACQABACAFAAQAHAAAGgEQALgHADgJIABgFQAAgKgHgDQgGgCgFACQAJADAAALQAAAGgFAEQgEAEgHAAIgGgBQgLgDAAgOIACgJQAGgRAXAAQAJAAAHAFQAHAEACAHQAMgRAaABQAbACAOAZQAJARAAATQgBAqgeAPQgKAFgLAAIgOgCIAAgKQgKAHgIADQgHACgLAAIgFAAgAAOgPQAOALAAATQAAATgMANIAKABQALAAAGgEQAPgMAAgYQAAgQgLgMQgKgMgQgBQgTgBgJAMIACAAQALAAAIAHgAg+AJQAAAKAGAGQAHAKASAAIAKAAQAMgCAIgIQAGgIAAgKQAAgGgDgDQgDgHgKgBQgFAAgIAFIgNAHQgGADgIAAQgGAAgEgDIgBAHg");
	this.shape_242.setTransform(557.1,56.2);

	this.shape_243 = new cjs.Shape();
	this.shape_243.graphics.f("#FFFFFF").s().p("AhIAkQgEgMADgJIgLAAIAAgMIAJgJIALgMQgIAAgEgFQgEgFAAgIQAAgLAJgHQAIgHAMgBQATgBAJAOQARgNAaAAIAJAAQAZACAQATQAQAUgBAYQgCAZgQARQgQASgZAAIgJgBIAAgMQgQAMgYABIgDAAQgmAAgIgbgAgHgkQAHABAHAFQAIAEAFAFQAMANAAAQQABAYgNAPIAFABQALAAAIgFQAQgLACgVQABgNgHgNQgGgMgNgGQgLgFgQAAQgKAAgHACgAgrgQQgJAHgOANIAKAAQgEAJACAJQAGARAagBQAOgBALgJQAJgJABgNQABgLgJgJQgHgJgNAAIgCAAQgMAAgKAHgAg0gvIgGADQAGABACADQACAFgBAFIAHgGIAGgFQgDgEgGgCIgDAAIgEAAg");
	this.shape_243.setTransform(540.2,56.2);

	this.shape_244 = new cjs.Shape();
	this.shape_244.graphics.f("#FFFFFF").s().p("AgCBUQgcgBgQgQQgQgRgBgaQABgRAGgMIACgEIAAgjIgLAAIAAgMIALAAIAAgRIgMAAIAAgLIAhAAQAKAAAGAFQAIAGAAAKQAAAJgJAGQgJAFgNgBIAAASQgJAIgFAJQAGgKAMgLIAcgaQAIgLAEgHIAaAAQgGAJgHAJQgHAIgMAIQASgBAOAFQAlAOAAAsQAAAbgUASQgTASgbAAIgDgBgAgggMQgMAMAAAPQABAPAJAKQAOAOAVAAQARAAANgJQAQgNAAgTQAAgIgEgIQgEgLgMgHQgLgGgNAAQgXAAgMAPgAgohIIAAAQQAMAAAAgIQAAgIgKAAIgCAAg");
	this.shape_244.setTransform(524.5,54.1);

	this.shape_245 = new cjs.Shape();
	this.shape_245.graphics.f("#FFFFFF").s().p("AgzBCQgVgPABgbQABgXATgJQgKgCAAgKQAAgOAOgGQAKgFAOABQARAAAKAOQAKANAAARIgrAAQgJAAgHAFQgHAGABAJQABAWAdAGQAJACAMAAQAYAAAPgQQAOgPABgXQAAgYgOgSQgPgTgcAAQgggBgRAYIgQAAQAIgSASgKQARgJAWAAQAhAAAVAXQAVAXAAAhQAAAhgTAWQgUAZghAAQggAAgTgOgAgigcQADACABADQACAGgCAFIAbAAQgCgHgFgGQgGgFgJAAQgFAAgEACg");
	this.shape_245.setTransform(510.3,54.5);

	this.shape_246 = new cjs.Shape();
	this.shape_246.graphics.f("#FFFFFF").s().p("AgcA6IAAgOQAEADAIAAQAIAAAFgOQAFgOAAgRQAAgRgFgOQgFgQgHAAQgIgBgFAEIAAgPQAHgFAMAAQARAAALAWQAKATAAAXQAAAXgKASQgMAUgQAAQgLAAgIgFg");
	this.shape_246.setTransform(492,56.2);

	this.shape_247 = new cjs.Shape();
	this.shape_247.graphics.f("#FFFFFF").s().p("AASAwQANABAKgGQAKgGAEgMQAFgMgBgMQgBgOgKgKQgKgLgOAAQgXACgFAUQAEgDAFAAQAHAAAEADQAOAJgBAWQAAAUgSAMQgPAMgXAAQgWAAgQgOQgPgOAAgVQAAgXARgLQgFgCgDgEQgDgFAAgGQAAgLAKgHQAJgGAMAAQALAAAIAHQAIAGACALQAOgXAcgBQAXgBAPAUQAPATAAAYQAAAbgSARQgRASgcAAgAg2gHQgHAGAAAJQAAAMAMAHQALAGAPAAQANAAAKgFQAKgGABgKQAAgFgCgEQgDgDgEAAQgDAAgCACQgDADAAADIgOAAIAAgLIABgKIgSAAQgKAAgHAGgAgygtQADACADAGQABAFgBAFIAPAAQgBgKgFgEQgEgEgHAAIgEAAg");
	this.shape_247.setTransform(480.9,56.2);

	this.shape_248 = new cjs.Shape();
	this.shape_248.graphics.f("#FFFFFF").s().p("AgEBPQgbgBgRgQQgQgRAAgaQgBgRAHgMQAGgMAOgNQAKgIASgSQAJgLADgHIAZAAQgEAJgIAJQgHAIgLAIQARgBANAFQAmAOAAAsQAAAbgUASQgTASgaAAIgEgBgAgjgRQgKAMAAAPQAAAPAKAKQANAOAWAAQARAAAMgJQAQgNAAgTQAAgIgDgIQgFgLgLgHQgLgGgOAAQgXAAgNAPg");
	this.shape_248.setTransform(465.8,54.6);

	this.shape_249 = new cjs.Shape();
	this.shape_249.graphics.f("#FFFFFF").s().p("AASAwQANABAKgGQAKgGAEgMQAFgMgBgMQgBgOgKgKQgKgLgOAAQgXACgFAUQAEgDAFAAQAHAAAEADQAOAJgBAWQAAAUgSAMQgPAMgXAAQgWAAgQgOQgPgOAAgVQAAgXARgLQgFgCgDgEQgDgFAAgGQAAgLAKgHQAJgGAMAAQALAAAIAHQAIAGACALQAOgXAcgBQAXgBAPAUQAPATAAAYQAAAbgSARQgRASgcAAgAg2gHQgHAGAAAJQAAAMAMAHQALAGAPAAQANAAAKgFQAKgGABgKQAAgFgCgEQgDgDgEAAQgDAAgCACQgDADAAADIgOAAIAAgLIABgKIgSAAQgKAAgHAGgAgygtQADACADAGQABAFgBAFIAPAAQgBgKgFgEQgEgEgHAAIgEAAg");
	this.shape_249.setTransform(450.6,56.2);

	this.shape_250 = new cjs.Shape();
	this.shape_250.graphics.f("#FFFFFF").s().p("AgQAeIAAgZIgLAAIAAgLIALAAIAAgQIgLAAIAAgLIAgAAQAKAAAGAEQAHAHAAAJQAAAJgIAFQgJAFgMgBIAAAdgAgBgVIAAAPQALAAAAgHQAAgJgJAAIgCABg");
	this.shape_250.setTransform(431.6,47.7);

	this.shape_251 = new cjs.Shape();
	this.shape_251.graphics.f("#FFFFFF").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_251.setTransform(425.9,56.2);

	this.shape_252 = new cjs.Shape();
	this.shape_252.graphics.f("#FFFFFF").s().p("AgzBCQgVgPABgbQABgXATgJQgKgCAAgKQAAgOAOgGQAKgFAOABQARAAAKAOQAKANAAARIgrAAQgJAAgHAFQgHAGABAJQABAWAdAGQAJACAMAAQAYAAAPgQQAOgPABgXQAAgYgOgSQgPgTgcAAQgggBgRAYIgQAAQAIgSASgKQARgJAWAAQAhAAAVAXQAVAXAAAhQAAAhgTAWQgUAZghAAQggAAgTgOgAgigcQADACABADQACAGgCAFIAbAAQgCgHgFgGQgGgFgJAAQgFAAgEACg");
	this.shape_252.setTransform(409.8,54.5);

	this.shape_253 = new cjs.Shape();
	this.shape_253.graphics.f("#FFFFFF").s().p("AgbAzQgLgJAAgSQAAgNAFgKQADgFAJgLQAJgLAZgTIgwAAIAAgPIBHAAIAAAOIgQANQgIAFgHAHQgJAMgDAFQgHAJABALQAAAHAGAEQAEADAGAAQANABAEgLIABgHQAAgFgDgFIAPAAQAHALgBANQgBAOgMAKQgLAKgPgBQgQAAgLgJg");
	this.shape_253.setTransform(398.2,56.5);

	this.shape_254 = new cjs.Shape();
	this.shape_254.graphics.f("#FFFFFF").s().p("AgEBPQgbgBgRgQQgQgRAAgaQgBgRAHgMQAGgMAOgNQAKgIASgSQAJgLADgHIAZAAQgEAJgIAJQgHAIgLAIQARgBANAFQAmAOAAAsQAAAbgUASQgTASgaAAIgEgBgAgjgRQgKAMAAAPQAAAPAKAKQANAOAWAAQARAAAMgJQAQgNAAgTQAAgIgDgIQgFgLgLgHQgLgGgOAAQgXAAgNAPg");
	this.shape_254.setTransform(387.2,54.6);

	this.shape_255 = new cjs.Shape();
	this.shape_255.graphics.f("#FFFFFF").s().p("AgxBTIAAgXIBaAAIAAgVQgSAFgbgCQgYgBgPgNQgPgOAAgUQAAgaAWgMQgDgBgDgEQgCgEAAgFIAAgGQAEgLANgEQAJgDARAAQAbAAAOAQQAJALABATIgsAAQgNAAgJAIQgKAHgBAMQgBAOAMALQANALAPAAQAcAAATgKIAABCgAgQhBQAEACABAFQABAIgFACIAIgBIAlAAQgCgKgNgFQgFgCgKgBQgLAAgFACg");
	this.shape_255.setTransform(374.2,58.3);

	this.shape_256 = new cjs.Shape();
	this.shape_256.graphics.f("#FFFFFF").s().p("AgtBHQgUgMgCgWQgCgYAUgKQgGgBgDgDQgEgEAAgGIABgGQAHgQAgABQATABAJAOQAJAMgBASIgqAAQgVAAABAPQAAAMAPAGQANAFATAAQAVAAAOgLQAOgLABgRIABgJQAAgRgLgMQgJgLgRAAIgyAAQgXAAAAgQQAAgbA8AAQAkAAANAKQAKAHAAAKQAAAKgJAGQATAVgBAhQAAAdgWASQgUARgcAAQgaAAgRgKgAghgRQADADAAAFQABAFgEAEIAeAAQgCgJgGgEQgGgFgKAAIgGABgAAcg8QgDACAAAEQAAADADADQACACAEAAQADAAADgCQADgDAAgDQAAgEgDgCQgDgDgDAAQgEAAgCADgAgig5QgBAGALAAIAiAAQgBgHADgHIgOgBQgeAAgCAJg");
	this.shape_256.setTransform(361,54.4);

	this.shape_257 = new cjs.Shape();
	this.shape_257.graphics.f("#FFFFFF").s().p("AgQAeIAAgZIgLAAIAAgLIALAAIAAgQIgLAAIAAgLIAgAAQAKAAAGAEQAHAHAAAJQAAAJgIAFQgJAFgMgBIAAAdgAgBgVIAAAPQALAAAAgHQAAgJgJAAIgCABg");
	this.shape_257.setTransform(343.3,47.7);

	this.shape_258 = new cjs.Shape();
	this.shape_258.graphics.f("#FFFFFF").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_258.setTransform(337.6,56.2);

	this.shape_259 = new cjs.Shape();
	this.shape_259.graphics.f("#FFFFFF").s().p("AAAAyQgKANgRAAQgYAAgMgSQgLgQABgcQAAgeAUgSQAOgNASgBQAIgBAGACIAAAOQgJgDgMAEQgMAEgIALQgHAMAAAOQAAAlAaAAQAJAAAGgFQAHgGAAgJIAAgNIAOAAIAAAMQAAAKAHAGQAGAFAKAAQALAAAIgIQAHgHAAgMQAAgKgIgIQgHgHgMAAIgkAAQAAgWAJgLQAJgKASABQAhAAAAAaIgBAGIgMAAQAMAFAHALQAHALAAAMQAAAXgMAPQgMAPgUAAQgWAAgJgNgAAKggIAWAAIAJAAIAAgEQAAgLgOAAQgPAAgCAPg");
	this.shape_259.setTransform(321.3,56.2);

	this.shape_260 = new cjs.Shape();
	this.shape_260.graphics.f("#FFFFFF").s().p("Ag6BJQgQgGgGAAQgGAAgHAJIgLgHIAWghQgKgMAAgTQAAgHACgGQAFgUASgKQANgIASAAIAJAAIAVgfIAQAAIgVAgIALAFIAJAHQADgFADgDQAHgFAJAAQALAAAHAIQAJgJAMABQAhABACA5QAAAbgJASQgMAVgXAAQgQAAgIgIQgJgJAAgOIABgHIAOAAIAAAEQAAASAQAAQAKAAAEgQQACgJABgVQAAgQgCgMQgDgQgHAAQgKAAAAAOIAAAeIgPAAIAAgeQAAgOgJgBQgEgBgDAEQgDACAAAEQAMAOgBAXQgBAcgQAQQgQAQgdABIgBAAQgLAAgPgFgAg9A5QAMAFALAAIALgBQAPgDAKgLQAIgMAAgQQAAgPgJgKQgKgMgPgBIgOATIAIAAQAJAAAGAEQAGAGABAIQABALgFAHQgFAIgKADQgHADgJAAQgOAAgKgGIgIALIAGAAQAGAAAGACgAg+AfQACADAFABQAKACADgCQgIgCgDgFQgCgFABgFgAgpAUQAAAHAHAAQAHAAAAgHQAAgHgHAAQgHAAAAAHgAhEgFQgFAGAAAKQAAAGABAEIAbgnQgQACgHALg");
	this.shape_260.setTransform(302.8,54.7);

	this.shape_261 = new cjs.Shape();
	this.shape_261.graphics.f("#FFFFFF").s().p("AgEBPQgbgBgRgQQgQgRAAgaQgBgRAHgMQAGgMAOgNQAKgIASgSQAJgLADgHIAZAAQgEAJgIAJQgHAIgLAIQARgBANAFQAmAOAAAsQAAAbgUASQgTASgaAAIgEgBgAgjgRQgKAMAAAPQAAAPAKAKQANAOAWAAQARAAAMgJQAQgNAAgTQAAgIgDgIQgFgLgLgHQgLgGgOAAQgXAAgNAPg");
	this.shape_261.setTransform(285.9,54.6);

	this.shape_262 = new cjs.Shape();
	this.shape_262.graphics.f("#FFFFFF").s().p("AAoAdQAKgHAAgKIAAgDQgCgHgQgEQgNgDgTAAQgRAAgOAEQgQADgDAHIAAAEQAAAKAJAGIgNAAQgMgEAAgRQABgGACgDQAHgQATgGQAOgFAXAAQA1AAALAbQACADAAAGQAAAQgLAFg");
	this.shape_262.setTransform(271.2,48.3);

	this.shape_263 = new cjs.Shape();
	this.shape_263.graphics.f("#FFFFFF").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_263.setTransform(270.5,56.2);

	this.shape_264 = new cjs.Shape();
	this.shape_264.graphics.f("#FFFFFF").s().p("AgWA1QgIAJgUAAQgLAAgIgHQgIgHgBgMQAAgMAGgIIgKAAIAAgKIAIgJIALgMIgDAAQgHAAgEgFQgDgFAAgHQAAgLAIgJQAIgIAMgBQAVgCAJAOQAUgOAaACQAYABARASQARASAAAYQABApgeASQgHADgNAAIgMAAIAAgKQgJAKgMAAIgBAAQgMAAgJgJgAgKgmQATACAMAOQAPAQAAAUQAAAVgLAMIAGABQAKAAAJgIQAJgIACgMIAAgIQAAgZgQgOQgNgNgZAAQgIAAgJACgAgogQQgMAJgKALIAJAAQgGAJABAJQABAEAEAEQAEADAGAAQAKAAADgKQACgGAAgRIANAAQAAASACAGQACAJAJgBQAHAAAFgIQAFgHAAgJQAAgLgHgKQgHgJgJgBIgHgBQgRAAgIAIgAg5grQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_264.setTransform(245.7,56.2);

	this.shape_265 = new cjs.Shape();
	this.shape_265.graphics.f("#FFFFFF").s().p("AgQAeIAAgZIgLAAIAAgLIALAAIAAgQIgLAAIAAgLIAgAAQAKAAAGAEQAHAHAAAJQAAAJgIAFQgJAFgMgBIAAAdgAgBgVIAAAPQALAAAAgHQAAgJgJAAIgCABg");
	this.shape_265.setTransform(234.3,47.7);

	this.shape_266 = new cjs.Shape();
	this.shape_266.graphics.f("#FFFFFF").s().p("AgcA6IAAgOQAEADAIAAQAIAAAFgOQAFgOAAgRQAAgRgFgOQgFgQgHAAQgIgBgFAEIAAgPQAHgFAMAAQARAAALAWQAKATAAAXQAAAXgKASQgMAUgQAAQgLAAgIgFg");
	this.shape_266.setTransform(233.9,56.2);

	this.shape_267 = new cjs.Shape();
	this.shape_267.graphics.f("#FFFFFF").s().p("Ag0A3QgUgYABghQAAghAVgWQAVgVAiAAQAdAAATATQAUATAAAdQAAAXgNAQQgOASgWAAQgWABgMgOQgOgOACgUQACgSAOgLIgRAAIAAgOIA4AAIAAAOIgKAAQgKAAgIAHQgGAGgBAKQAAAKAHAHQAIAGALAAQAOAAAJgLQAHgKAAgOQgBgSgJgLQgNgQgbAAQgXAAgOAUQgLARAAAZQAAAXALARQANATAUADIALABQAaAAAPgVIARAAQgSAngxAAQghAAgVgYg");
	this.shape_267.setTransform(223.7,57.9);

	this.shape_268 = new cjs.Shape();
	this.shape_268.graphics.f("#FFFFFF").s().p("AgoAtQgSgSAAgaQAAgcASgRQASgSAaAAQAdAAANAMQAJAJABAMIgPAAQAAgJgMgFQgKgEgNAAQgRAAgOALQgOANAAARQAAARANALQANAKAQAAQAPAAAKgHQALgIAAgOQABgRgRgEQAHAFgBAMQAAAHgHAGQgIAHgJAAQgKAAgIgHQgIgHAAgLQAAgMAKgIQAKgGAMAAQAUAAAOALQANALAAASQAAAYgQAPQgQAQgXAAQgZAAgSgSgAgDgOQgDADAAAEQAAAFADACQADADAEAAQAEAAADgDQADgCAAgEQAAgFgDgDQgCgDgFAAQgEAAgDADg");
	this.shape_268.setTransform(210,56.2);

	this.shape_269 = new cjs.Shape();
	this.shape_269.graphics.f("#FFFFFF").s().p("AgcA6IAAgOQAEADAIAAQAIAAAFgOQAFgOAAgRQAAgRgFgOQgFgQgHAAQgIgBgFAEIAAgPQAHgFAMAAQARAAALAWQAKATAAAXQAAAXgKASQgMAUgQAAQgLAAgIgFg");
	this.shape_269.setTransform(200.7,56.2);

	this.shape_270 = new cjs.Shape();
	this.shape_270.graphics.f("#FFFFFF").s().p("AAVBMIAAgcQgSACgOgBQgcgBgRgMQgTgOAAgZQAAgMAHgLQAHgJAJgFQgFgBgCgEQgCgDAAgEQAAgIAHgFQAMgKAWAAQAiAAAJAUQACgJAHgGQAIgFAJAAIAOAAIAAAOIgCgBQgFABgBAEQAAACAFAEQAHAFADAEQAGAGAAAJQABAPgOAHQgLAGgPgBIAAAfIANgDIAJgDIAAANIgJAFIgNACIAAAfgAgigWQgIAKAAAMQABAaAeAIIAPABQALAAAGgBIAAhCIgiAAQgNAAgIAKgAAlglIAAAYIADAAQAGAAAEgEQAEgDAAgGQAAgHgGgIQgGgJAAgEIAAgCQgFABAAASgAAJgtQgCgJgLgFQgNgDgMADQADAEAAADQAAAEgDADIADAAIADAAIAgAAIAAAAg");
	this.shape_270.setTransform(190.1,57.6);

	this.shape_271 = new cjs.Shape();
	this.shape_271.graphics.f("#0097A7").s().p("EhNbAJgQhbAAhBhAQhAhBAAhbIAAvjMChvAAAIAAPjQAABbhABBQhBBAhbAAg");
	this.shape_271.setTransform(669.4,60.8);

	this.instance_9 = new lib.Path_1();
	this.instance_9.parent = this;
	this.instance_9.setTransform(676.4,64.3,1,1,0,0,0,517.6,63.3);
	this.instance_9.alpha = 0.602;

	this.shape_272 = new cjs.Shape();
	this.shape_272.graphics.f("#FFFFFF").s().p("EgoQA44QgtAAggggQgfggAAgsMAAAhuXQAAgsAfghQAggfAtAAMBEJAAAMAOEBPxMgGQAh+g");
	this.shape_272.setTransform(288.5,384);

	this.instance_10 = new lib.Group_1();
	this.instance_10.parent = this;
	this.instance_10.setTransform(298.5,384,1,1,0,0,0,278.5,364);
	this.instance_10.alpha = 0.602;

	this.shape_273 = new cjs.Shape();
	this.shape_273.graphics.f("#00ACC1").s().p("AhhBiQgpgoAAg6QAAg5ApgpQApgoA4AAQA6AAAoAoQApApAAA5QAAA5gpApQgoAqg6AAQg4AAgpgqg");
	this.shape_273.setTransform(1188,561.7);

	this.shape_274 = new cjs.Shape();
	this.shape_274.graphics.f("#80DEEA").s().p("Ah3B4QgxgyAAhGQAAhFAxgyQAygxBFgBQBGABAyAxQAxAyAABFQAABGgxAyQgyAyhGAAQhFAAgygyg");
	this.shape_274.setTransform(1188,561.7);

	this.shape_275 = new cjs.Shape();
	this.shape_275.graphics.f("#00ACC1").s().p("AhhBiQgpgoAAg6QAAg4ApgpQApgpA4AAQA6AAAoApQApApAAA4QAAA6gpAoQgoApg6AAQg4AAgpgpg");
	this.shape_275.setTransform(1188,498.9);

	this.shape_276 = new cjs.Shape();
	this.shape_276.graphics.f("#80DEEA").s().p("Ah3B4QgxgyAAhGQAAhFAxgyQAygxBFAAQBGAAAyAxQAxAyAABFQAABGgxAyQgyAxhGAAQhFAAgygxg");
	this.shape_276.setTransform(1188,498.9);

	this.shape_277 = new cjs.Shape();
	this.shape_277.graphics.f("#00ACC1").s().p("AhhBiQgpgoAAg6QAAg5ApgpQApgoA4AAQA6AAAoAoQApApAAA5QAAA5gpApQgoAqg6AAQg4AAgpgqg");
	this.shape_277.setTransform(1188,437.1);

	this.shape_278 = new cjs.Shape();
	this.shape_278.graphics.f("#80DEEA").s().p("Ah3B4QgxgyAAhGQAAhFAxgyQAygxBFgBQBGABAyAxQAxAyAABFQAABGgxAyQgyAyhGgBQhFABgygyg");
	this.shape_278.setTransform(1188,437.1);

	this.shape_279 = new cjs.Shape();
	this.shape_279.graphics.f("#00ACC1").s().p("AhhBiQgpgoAAg6QAAg5ApgpQApgpA4ABQA6gBAoApQApApAAA5QAAA5gpApQgoApg6ABQg4gBgpgpg");
	this.shape_279.setTransform(1188,374.3);

	this.shape_280 = new cjs.Shape();
	this.shape_280.graphics.f("#80DEEA").s().p("Ah3B4QgxgyAAhGQAAhFAxgyQAygxBFAAQBGAAAyAxQAxAyAABFQAABGgxAyQgyAyhGgBQhFABgygyg");
	this.shape_280.setTransform(1188,374.3);

	this.shape_281 = new cjs.Shape();
	this.shape_281.graphics.f("#00ACC1").s().p("AhhBjQgpgpAAg6QAAg4ApgpQApgpA4gBQA6ABAoApQApApAAA4QAAA5gpAqQgoApg6gBQg4ABgpgpg");
	this.shape_281.setTransform(1188,312.5);

	this.shape_282 = new cjs.Shape();
	this.shape_282.graphics.f("#80DEEA").s().p("Ah3B4QgxgyAAhGQAAhFAxgyQAygyBFABQBGgBAyAyQAxAyAABFQAABGgxAyQgyAxhGABQhFgBgygxg");
	this.shape_282.setTransform(1188,312.5);

	this.shape_283 = new cjs.Shape();
	this.shape_283.graphics.f("#00ACC1").s().p("AhhBjQgpgqAAg5QAAg4ApgpQApgpA4gBQA6ABAoApQApApAAA4QAAA5gpAqQgoApg6gBQg4ABgpgpg");
	this.shape_283.setTransform(1188,250.8);

	this.shape_284 = new cjs.Shape();
	this.shape_284.graphics.f("#80DEEA").s().p("Ah3B4QgxgyAAhGQAAhGAxgxQAygyBFABQBGgBAyAyQAxAxAABGQAABGgxAyQgyAxhGAAQhFAAgygxg");
	this.shape_284.setTransform(1188,250.8);

	this.shape_285 = new cjs.Shape();
	this.shape_285.graphics.f("#00ACC1").s().p("AhhBjQgpgpAAg6QAAg5ApgpQApgpA4AAQA6AAAoApQApApAAA5QAAA6gpApQgoAog6AAQg4AAgpgog");
	this.shape_285.setTransform(1188,189);

	this.shape_286 = new cjs.Shape();
	this.shape_286.graphics.f("#80DEEA").s().p("Ah3B4QgxgyAAhGQAAhFAxgyQAygyBFAAQBGAAAyAyQAxAyAABFQAABGgxAyQgyAxhGABQhFgBgygxg");
	this.shape_286.setTransform(1188,189);

	this.instance_11 = new lib.Path();
	this.instance_11.parent = this;
	this.instance_11.setTransform(640,384,1,1,0,0,0,640.5,384.5);
	this.instance_11.alpha = 0.602;

	this.shape_287 = new cjs.Shape();
	this.shape_287.graphics.f().s("#26C6DA").p("EBkAA8AMjH/AAAMAAAh3/MDH/AAAg");
	this.shape_287.setTransform(640,384);

	this.shape_288 = new cjs.Shape();
	this.shape_288.graphics.f("#26C6DA").s().p("Ehj/A8AMAAAh3/MDH/AAAMAAAB3/g");
	this.shape_288.setTransform(640,384);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_288},{t:this.shape_287},{t:this.instance_11},{t:this.shape_286},{t:this.shape_285},{t:this.shape_284},{t:this.shape_283},{t:this.shape_282},{t:this.shape_281},{t:this.shape_280},{t:this.shape_279},{t:this.shape_278},{t:this.shape_277},{t:this.shape_276},{t:this.shape_275},{t:this.shape_274},{t:this.shape_273},{t:this.instance_10},{t:this.shape_272},{t:this.instance_9},{t:this.shape_271},{t:this.shape_270},{t:this.shape_269},{t:this.shape_268},{t:this.shape_267},{t:this.shape_266},{t:this.shape_265},{t:this.shape_264},{t:this.shape_263},{t:this.shape_262},{t:this.shape_261},{t:this.shape_260},{t:this.shape_259},{t:this.shape_258},{t:this.shape_257},{t:this.shape_256},{t:this.shape_255},{t:this.shape_254},{t:this.shape_253},{t:this.shape_252},{t:this.shape_251},{t:this.shape_250},{t:this.shape_249},{t:this.shape_248},{t:this.shape_247},{t:this.shape_246},{t:this.shape_245},{t:this.shape_244},{t:this.shape_243},{t:this.shape_242},{t:this.shape_241},{t:this.shape_240},{t:this.shape_239},{t:this.shape_238},{t:this.shape_237},{t:this.shape_236},{t:this.shape_235},{t:this.shape_234},{t:this.shape_233},{t:this.shape_232},{t:this.shape_231},{t:this.shape_230},{t:this.shape_229},{t:this.shape_228},{t:this.shape_227},{t:this.shape_226},{t:this.shape_225},{t:this.shape_224},{t:this.shape_223},{t:this.shape_222},{t:this.shape_221},{t:this.shape_220},{t:this.shape_219},{t:this.shape_218},{t:this.shape_217},{t:this.shape_216},{t:this.shape_215},{t:this.shape_214},{t:this.shape_213},{t:this.shape_212},{t:this.shape_211},{t:this.shape_210},{t:this.shape_209},{t:this.shape_208},{t:this.shape_207},{t:this.shape_206},{t:this.shape_205},{t:this.shape_204},{t:this.shape_203},{t:this.shape_202},{t:this.shape_201},{t:this.shape_200},{t:this.shape_199},{t:this.shape_198},{t:this.shape_197},{t:this.shape_196},{t:this.shape_195},{t:this.shape_194},{t:this.shape_193},{t:this.shape_192},{t:this.shape_191},{t:this.shape_190},{t:this.shape_189},{t:this.shape_188},{t:this.shape_187},{t:this.shape_186},{t:this.shape_185},{t:this.shape_184},{t:this.shape_183},{t:this.shape_182},{t:this.shape_181},{t:this.shape_180},{t:this.shape_179},{t:this.shape_178},{t:this.shape_177},{t:this.shape_176},{t:this.shape_175},{t:this.shape_174},{t:this.shape_173},{t:this.shape_172},{t:this.shape_171},{t:this.shape_170},{t:this.shape_169},{t:this.shape_168},{t:this.shape_167},{t:this.shape_166},{t:this.shape_165},{t:this.shape_164},{t:this.shape_163},{t:this.shape_162},{t:this.shape_161},{t:this.shape_160},{t:this.shape_159},{t:this.shape_158},{t:this.shape_157},{t:this.shape_156},{t:this.shape_155},{t:this.shape_154},{t:this.shape_153},{t:this.shape_152},{t:this.shape_151},{t:this.shape_150},{t:this.shape_149},{t:this.shape_148},{t:this.shape_147},{t:this.shape_146},{t:this.shape_145},{t:this.shape_144},{t:this.shape_143},{t:this.shape_142},{t:this.shape_141},{t:this.shape_140},{t:this.shape_139},{t:this.shape_138},{t:this.shape_137},{t:this.shape_136},{t:this.shape_135},{t:this.shape_134},{t:this.shape_133},{t:this.shape_132},{t:this.shape_131},{t:this.shape_130},{t:this.shape_129},{t:this.shape_128},{t:this.shape_127},{t:this.shape_126},{t:this.shape_125},{t:this.shape_124},{t:this.shape_123},{t:this.shape_122},{t:this.shape_121},{t:this.shape_120},{t:this.shape_119},{t:this.shape_118},{t:this.shape_117},{t:this.shape_116},{t:this.shape_115},{t:this.shape_114},{t:this.shape_113},{t:this.shape_112},{t:this.shape_111},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.instance_2},{t:this.instance_1},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(470.2,383,1450.9,770);
// library properties:
lib.properties = {
	width: 1280,
	height: 768,
	fps: 25,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"sounds/audio.mp3?1513342886532", id:"audio"},
		{src:"https://code.jquery.com/jquery-2.2.4.min.js?1513342886532", id:"lib/jquery-2.2.4.min.js"},
		{src:"components/sdk/anwidget.js?1513342886532", id:"sdk/anwidget.js"},
		{src:"components/ui/src/textinput.js?1513342886532", id:"an.TextInput"},
		{src:"components/ui/src/css.js?1513342886532", id:"an.CSS"}
	],
	preloads: []
};


function _updateVisibility(evt) {
	if((this.getStage() == null || this._off || this._lastAddedFrame != this.parent.currentFrame) && this._element) {
		this._element.detach();
		stage.removeEventListener('drawstart', this._updateVisibilityCbk);
		this._updateVisibilityCbk = false;
	}
}
function _handleDrawEnd(evt) {
	var props = this.getConcatenatedDisplayProps(this._props), mat = props.matrix;
	var tx1 = mat.decompose(); var sx = tx1.scaleX; var sy = tx1.scaleY;
	var dp = window.devicePixelRatio || 1; var w = this.nominalBounds.width * sx; var h = this.nominalBounds.height * sy;
	mat.tx/=dp;mat.ty/=dp; mat.a/=(dp*sx);mat.b/=(dp*sx);mat.c/=(dp*sy);mat.d/=(dp*sy);
	this._element.setProperty('transform-origin', this.regX + 'px ' + this.regY + 'px');
	var x = (mat.tx + this.regX*mat.a + this.regY*mat.c - this.regX);
	var y = (mat.ty + this.regX*mat.b + this.regY*mat.d - this.regY);
	var tx = 'matrix(' + mat.a + ',' + mat.b + ',' + mat.c + ',' + mat.d + ',' + x + ',' + y + ')';
	this._element.setProperty('transform', tx);
	this._element.setProperty('width', w);
	this._element.setProperty('height', h);
	this._element.update();
}

function _tick(evt) {
	var stage = this.getStage();
	stage&&stage.on('drawend', this._handleDrawEnd, this, true);
	if(!this._updateVisibilityCbk) {
		this._updateVisibilityCbk = stage.on('drawstart', this._updateVisibility, this, false);
	}
}


})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;